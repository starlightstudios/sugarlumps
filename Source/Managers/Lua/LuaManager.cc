//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
//--Lua
#include "lundump.h"

///========================================== System ==============================================
LuaManager::LuaManager()
{
    ///--[Variable Initialization]
    //--System
    //--Lua State
    mLuaState = luaL_newstate();
    luaL_openlibs(mLuaState);

    //--Passing arguments
    mTotalArgs = 0;
    mArgSlot = 0;
    mScriptArguments = NULL;

    //--Public Variables
    mFiringCode = -1;
    mFiringString = NULL;
    mFailSilentlyOnce = false;

    ///--[Default Functions]
    /* LM_GetRandomNumber(iLowest, iHighest)
       Returns a random number within the specified range, inclusive. */
    lua_register(mLuaState, "LM_GetRandomNumber", &Hook_LM_GetRandomNumber);

    /* LM_FailSilentlyOnce()
       On the next case of a file not being found, suppresses the error output. */
    lua_register(mLuaState, "LM_FailSilentlyOnce", &Hook_LM_FailSilentlyOnce);

    /* LM_ExecuteScript(sPathname[], ...)
       Executes the provided script with the provided args.  Regardless of what type they are passed
       in as, the argument is always stored as a string.  The number of passed arguments can be
       checked with LM_GetNumOfArgs(). */
    lua_register(mLuaState, "LM_ExecuteScript", &Hook_LM_ExecuteScript);

    /* LM_GetNumOfArgs()
       Returns how many arguments are currently on the LM's stack. */
    lua_register(mLuaState, "LM_GetNumOfArgs", &Hook_LM_GetNumOfArgs);

    /* LM_GetScriptArgument(Slot)
       Returns the specified argument as a string.  Slots count from 0.  You can check how many args
       were passed with LM_GetNumOfArgs().  If an arg was not passed, the string will be "NULL". */
    lua_register(mLuaState, "LM_GetScriptArgument", &Hook_LM_GetScriptArgument);
}
LuaManager::~LuaManager()
{
    lua_close(mLuaState);
}
int LuaManager::xLastDumpedSize = 0;

///===================================== Property Queries =========================================
lua_State *LuaManager::GetLuaState()
{
    return mLuaState;
}
int LuaManager::GetTotalScriptArguments()
{
    return mTotalArgs;
}
char *LuaManager::GetScriptArgument(int pSlot)
{
    if(pSlot < 0 || pSlot >= mTotalArgs) return NULL;
    return (char *)mScriptArguments[pSlot];
}
bool LuaManager::DoesScriptExist(const char *pPath)
{
    if(!pPath) return false;

    FILE *fCheckFile = fopen(pPath, "r");
    if(!fCheckFile) return false;

    fclose(fCheckFile);
    return true;
}

///======================================== Manipulators ==========================================
void LuaManager::SetArgumentListSize(int pSize)
{
    ClearArgumentList();
    if(pSize < 1) return;

    mTotalArgs = pSize;
    mScriptArguments = (char **)malloc(sizeof(char *) * mTotalArgs);
    memset(mScriptArguments, 0, sizeof(char *) * mTotalArgs);
}
void LuaManager::AddArgument(float pNumber)
{
    //--Overload, uses a floating-point number.  Integers passed in will convert correctly because
    //  we allocate 32 characters.
    if(mArgSlot < 0 || mArgSlot >= mTotalArgs) return;

    char *nBuffer = (char *)malloc(sizeof(char) * 32);
    sprintf(nBuffer, "%f", pNumber);

    mScriptArguments[mArgSlot] = nBuffer;
    mArgSlot ++;
}
void LuaManager::AddArgument(const char *pString)
{
    //--String version, the string is COPIED.
    if(mArgSlot < 0 || mArgSlot >= mTotalArgs) return;

    char *nBuffer = (char *)malloc(sizeof(char) * (strlen(pString) + 1));
    strcpy(nBuffer, pString);

    mScriptArguments[mArgSlot] = nBuffer;
    mArgSlot ++;
}

///======================================== Core Methods ==========================================
void LuaManager::ClearArgumentList()
{
    for(int i = 0; i < mTotalArgs; i ++)
    {
        free(mScriptArguments[i]);
    }
    mArgSlot = 0;
    mTotalArgs = 0;
    free(mScriptArguments);
    mScriptArguments = NULL;
}
bool LuaManager::LoadLuaFile(const char *pPath)
{
    //--Attempts to load the requested file into Lua, returning true if it worked, and false if it
    //  did not.
    //--Note that the error during LOADING can be different than an error during EXECUTION.
    if(!pPath) return false;

    int tLuaErrorCode = luaL_loadfile(mLuaState, pPath);
    if(tLuaErrorCode)
    {
        if(!mFailSilentlyOnce)
        {
            fprintf(stderr, "Lua Error during Loading: %s\n", lua_tostring(mLuaState, -1));
        }
        mFailSilentlyOnce = false;
        lua_pop(mLuaState, 1);//Pop the error message off Lua's internal stack
        return false;
    }
    return true;
}
bool LuaManager::ExecuteLoadedFile()
{
    //--Attempts to execute the loaded file.  This is *deliberately* split from the loading of the
    //  file since we may want to load a file and then not execute it, for example, to save its
    //  bytecode somewhere.
    //--Returns true on success, false on error.
    int tLuaErrorCode = lua_pcall(mLuaState, 0, LUA_MULTRET, 0);
    if(tLuaErrorCode)
    {
        if(!mFailSilentlyOnce)
        {
            fprintf(stderr, "Lua Error during Loading: %s\n", lua_tostring(mLuaState, -1));
        }
        mFailSilentlyOnce = false;
        lua_pop(mLuaState, 1);
        return false;
    }
    return true;
}
void *LuaManager::DumpLoadedFile()
{
    //--Dumps the loaded file out as compiled binary data.  Do NOT deallocate the pointer returned,
    //  just let it sit (it's not very big) as it will be cleared by the next call.
    //--Returns NULL on error.
    xLastDumpedSize = 0;
    free(xDumpBuffer);
    xDumpBuffer = NULL;

    //--Dump the data
    xIsFirstWriterCall = true;
    lua_dump(mLuaState, &LuaManager::WriterFunction, NULL, false);

    //--Debug.
    //xIsFirstReaderCall = true;
    //lua_load(mLuaState, &LuaManager::ReaderFunction, xDumpBuffer, "Chunk", "bt");
    //lua_call(mLuaState, 0, 0);

    return xDumpBuffer;
}
void LuaManager::ExecuteLuaFile(const char *pPath)
{
    //--Loads a Lua file and then executes it.  Whether or not arguments have been allocated is a
    //  problem handled elsewhere.
    //--Basically a quick-access macro of the above.
    if(!pPath) return;
    if(LoadLuaFile(pPath)) ExecuteLoadedFile();
}
void LuaManager::ExecuteLuaFile(const char *pPath, int pArgs, ...)
{
    //--Variable argument list auto-executor.  This should only be used from the C-code.
    //  Args should be "paired" with their types.  The currently accepted types are:
    //  "N" - Floating point number
    //  "S" - String
    //  Ex:  ExecuteLuaFile("Somepath.lua", 2, "N", 5.5f, "S", "This is a string")
    va_list tArgList;
    va_start(tArgList, pArgs);

    //--Get each arg and push it on the argument stack.
    const char *rType;

    SetArgumentListSize(pArgs);
    for(int i = 0; i < pArgs; i ++)
    {
        rType = va_arg(tArgList, const char *);
        if(rType[0] == 'N')
        {
            float tValue = va_arg(tArgList, double);
            AddArgument(tValue);
        }
        else if(rType[0] == 'S')
        {
            const char *rString = va_arg(tArgList, const char *);
            AddArgument(rString);
        }
        else
        {
            fprintf(stderr, "LuaManager::ExecuteLuaFile:  Error, cannot resolve type %s\n", rType);
        }
    }

    va_end(tArgList);

    if(LoadLuaFile(pPath)) ExecuteLoadedFile();
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
LuaManager *LuaManager::Fetch()
{
    return Global::Shared()->gLuaManager;
}
bool LuaManager::xIsFirstWriterCall = false;
void *LuaManager::xDumpBuffer = NULL;
int LuaManager::WriterFunction(lua_State *pLuaState, const void *pSource, size_t pExpectedSize, void *pData)
{
    //--Worker function that Lua uses to dump data.  The pOutBuffer is the source of all data coming
    //  through to pDataBuffer.  The size should represent how many bytes we are adding.
    //--It is not known how many times this function is going to get called, so a concatenation
    //  is used here.

    //--Extend the length of the destination.
    uint32_t mOldSize = xLastDumpedSize;
    xLastDumpedSize = xLastDumpedSize + pExpectedSize;
    xDumpBuffer = realloc(xDumpBuffer, xLastDumpedSize);

    //--Copy all the data.
    if(xIsFirstWriterCall)
    {
        memcpy(xDumpBuffer, pSource, pExpectedSize);
    }
    else
    {
        //--Buffer decoy, prevents complaints about void pointer arithmetic.
        uint8_t *rBufferDecoy = (uint8_t *)xDumpBuffer;
        memcpy(&rBufferDecoy[mOldSize], pSource, pExpectedSize);
    }

    //--Debug
    //fprintf(stderr, "Writer was called!  Passed %i bytes to %i total.\n", pExpectedSize, xLastDumpedSize);

    return 0;
}
bool LuaManager::xIsFirstReaderCall = false;
const char *LuaManager::ReaderFunction(lua_State *pLuaState, void *pData, size_t *pExpectedSize)
{
    //--Worker function, tells Lua how to read data out.  For now, just tell it the whole size of
    //  the buffer and give it the 0 address.
    if(xIsFirstReaderCall)
    {
        //fprintf(stderr, "Called reader, told about %i bytes\n", xLastDumpedSize);
        xIsFirstReaderCall = false;
        *pExpectedSize = xLastDumpedSize;
        return (const char *)pData;
    }
    //fprintf(stderr, "Called reader, NULLing off\n");
    return NULL;
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
///--[System Functions]
int Hook_LM_GetRandomNumber(lua_State *L)
{
    //LM_GetRandomNumber(Lowest, Highest)
    if(lua_gettop(L) != 2)
    {
        LuaArgError("LM_GetRandomNumber");
        lua_pushnumber(L, 0);
        return 1;
    }

    int tLowest  = lua_tointeger(L, 1);
    int tHighest = lua_tointeger(L, 2);
    int tRange = tHighest-tLowest+1;
    int tRetval = (rand() % tRange) + tLowest;
    lua_pushnumber(L, tRetval);
    return 1;
}

///--[Executors]
int Hook_LM_FailSilentlyOnce(lua_State *L)
{
    //LM_FailSilentlyOnce()
    LuaManager::Fetch()->mFailSilentlyOnce = true;
    return 0;
}
int Hook_LM_ExecuteScript(lua_State *L)
{
    //LM_ExecuteScript(Path[], ...)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LM_ExecuteScript");

    //--Setup
    LuaManager *rLuaManager = LuaManager::Fetch();
    const char *rCallPath = lua_tostring(L, 1);
    int tNumOfArgs = tArgs - 1;

    //--Argument handling
    rLuaManager->SetArgumentListSize(tNumOfArgs);
    for(int i = 0; i < tNumOfArgs; i ++)
    {
        rLuaManager->AddArgument(lua_tostring(L, i+2));
    }

    //--Run the script
    rLuaManager->ExecuteLuaFile(rCallPath);
    return 0;
}
int Hook_LM_GetNumOfArgs(lua_State *L)
{
    //LM_GetNumOfArgs()
    lua_pushnumber(L, LuaManager::Fetch()->GetTotalScriptArguments());
    return 1;
}
int Hook_LM_GetScriptArgument(lua_State *L)
{
    //LM_GetScriptArgument(Number)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("LM_GetScriptArgument");
        lua_pushstring(L, "NULL");
        return 1;
    }

    char *rString = LuaManager::Fetch()->GetScriptArgument(lua_tointeger(L, 1));
    lua_pushstring(L, rString);

    return 1;
}
