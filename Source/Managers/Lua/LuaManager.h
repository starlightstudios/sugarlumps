///======================================== LuaManager ============================================
//--Stores the Lua state and provides an interface for using it, both in Lua scripts and in the
//  C++ program.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class LuaManager
{
    private:
    //--System
    static bool xIsFirstWriterCall;
    static bool xIsFirstReaderCall;
    static void *xDumpBuffer;

    //--Lua State
    lua_State *mLuaState;

    //--Passing arguments
    int mTotalArgs;
    int mArgSlot;
    char **mScriptArguments;

    public:
    //--Public Variables
    int mFiringCode;
    char *mFiringString;
    static int xLastDumpedSize;
    bool mFailSilentlyOnce;

    //--System
    LuaManager();
    ~LuaManager();

    //--Property Queries
    lua_State *GetLuaState();
    int GetTotalScriptArguments();
    char *GetScriptArgument(int pSlot);
    static bool DoesScriptExist(const char *pPath);

    //--Manipulators
    void SetArgumentListSize(int pSize);
    void AddArgument(float pNumber);
    void AddArgument(const char *pString);
    void SetRelativePath(const char *pPath);

    //--Core Methods
    void ClearArgumentList();
    bool LoadLuaFile(const char *pPath);
    bool ExecuteLoadedFile();
    void *DumpLoadedFile();
    void ExecuteLuaFile(const char *pPath);
    void ExecuteLuaFile(const char *pPath, int pArgs, ...);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static int WriterFunction(lua_State *pLuaState, const void *pSource, size_t pExpectedSize, void *pDest);
    static const char *ReaderFunction(lua_State *pLuaState, void *pData, size_t *pExpectedSize);
    static LuaManager *Fetch();
};

///--[System Functions]
int Hook_LM_GetRandomNumber(lua_State *L);

///--[Executors]
int Hook_LM_FailSilentlyOnce(lua_State *L);
int Hook_LM_ExecuteScript(lua_State *L);
int Hook_LM_GetNumOfArgs(lua_State *L);
int Hook_LM_GetScriptArgument(lua_State *L);

