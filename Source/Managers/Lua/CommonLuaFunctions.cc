#include "Structures.h"
int LuaArgError(const char *pFunctionName)
{
    if(!pFunctionName) return 0;
    fprintf(stderr, "%s: Failed, invalid argument list\n", pFunctionName);
    return 0;
}
int LuaArgError(const char *pFunctionName, lua_State *L)
{
    LuaArgError(pFunctionName);
    lua_pushnil(L);
    return 1;
}
int LuaTypeError(const char *pFunctionName)
{
    if(!pFunctionName) return 0;
    fprintf(stderr, "%s: Failed, rActiveObject was wrong type, or NULL.\n", pFunctionName);
    return 0;
}
int LuaTypeError(const char *pFunctionName, lua_State *L)
{
    LuaTypeError(pFunctionName);
    lua_pushnil(L);
    return 1;
}
int LuaPropertyError(const char *pFunctionName, const char *pSwitchType, int pArgs)
{
    if(!pFunctionName || !pSwitchType) return 0;
    fprintf(stderr, "%s: Failed, cannot resolve %s with %i args.\n", pFunctionName, pSwitchType, pArgs);
    return 0;
}
int LuaPushPropertyPtr(PropertyLookup *pLookup, lua_State *pLuaState)
{
    //--Pushes the requested property to the provided Lua state.  If the property isn't found then
    //  pushed NIL which will cause the Lua script to bark an error if used.
    //--Something is *always* pushed, and the number of things pushed is returned.
    return 0;
}
void LuaSetPropertyPtr(PropertyLookup *pLookup, lua_State *pLuaState, int pIndex)
{
    //--Sets a property by coercing the provided argument into a form usable by the pointer.
}
PropertyLookup *LuaDefineProperty(lua_State *pLuaState, int pIndexOfType, int pIndexOfProperty)
{
    //--Creates a new PropertyLookup that has ownership of its variable.  Use the deletion function
    //  FreePropertyLookup to deallocate correctly.
    //--Returns NULL on error.
    return NULL;
}
