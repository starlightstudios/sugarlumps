//--[GLManager]
//--Manager of the OpenGL state and wrangler of extensions.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class GLManager
{
    private:
    //--System
    ALLEGRO_DISPLAY *mDisplay;

    protected:

    public:
    //--System
    GLManager();
    ~GLManager();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    void CreateDisplay();
    void DestroyDisplay();
    void *WrangleSubroutine(const char *pFuncName);
    void WrangleExec();

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    ALLEGRO_DISPLAY *GetDisplay();

    //--Static Functions
    static void TargetDisplay();
    static void HookToLuaState(lua_State *pLuaState);

    //--Lua Hooking
};

//--Hooking Functions


