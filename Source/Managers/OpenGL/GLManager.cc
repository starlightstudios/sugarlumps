//--Base
#include "GLManager.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//=========================================== System ==============================================
GLManager::GLManager()
{
    //--System
    mDisplay = NULL;
}
GLManager::~GLManager()
{
}

//--Static GL Definitions
GLGETCOMPRESSEDTEXIMAGEARB_FUNC sglGetCompressedTexImage;
GLCOMPRESSEDTEX2D_FUNC sglCompressedTexImage2D;

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
void GLManager::CreateDisplay()
{
    al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_WINDOWED);
    al_add_new_bitmap_flag(ALLEGRO_MEMORY_BITMAP);
    al_set_new_window_position(32, 32);
    mDisplay = al_create_display(640, 480);
    al_set_window_title(mDisplay, "Sugar Lumps");
}
void GLManager::DestroyDisplay()
{
    al_destroy_display(mDisplay);
    mDisplay = NULL;
}
void *GLManager::WrangleSubroutine(const char *pFuncName)
{
    //--Worker Function
    if(!pFuncName) return NULL;

    void *rFuncPtr = al_get_opengl_proc_address(pFuncName);
    DebugManager::Print("%s %p\n", pFuncName, rFuncPtr);
    return rFuncPtr;
}
void GLManager::WrangleExec()
{
    //--Wrangles all extensions not provided under normal GL functionality.
    DebugManager::PushPrint(true, "[GL Wrangling] Begin\n");

    //--A
    /*glAttachShaderDfn = (GLATTACHSHADER_FUNC)WrangleSubroutine("glAttachShader");

    //--C
    glCompileShaderDfn = (GLCOMPILESHADER_FUNC)WrangleSubroutine("glCompileShader");
    glCreateProgramDfn = (GLCREATEPROGRAM_FUNC)WrangleSubroutine("glCreateProgram");
    glCreateShaderDfn = (GLCREATESHADER_FUNC)WrangleSubroutine("glCreateShader");*/
    sglCompressedTexImage2D = (GLCOMPRESSEDTEX2D_FUNC)WrangleSubroutine("glCompressedTexImage2D");

    //--D
    /*glDeleteProgramDfn = (GLDELETEPROGRAM_FUNC)WrangleSubroutine("glDeleteProgram");
    glDeleteShaderDfn = (GLDELETESHADER_FUNC)WrangleSubroutine("glDeleteShader");

    //--G*/
    sglGetCompressedTexImage = (GLGETCOMPRESSEDTEXIMAGEARB_FUNC)WrangleSubroutine("glGetCompressedTexImage");
    /*glGetShaderivDfn = (GLGETSHADERIV_FUNC)WrangleSubroutine("glGetShaderiv");
    glGetShaderInfoLogDfn = (GLGETSHADERINFOLOG_FUNC)WrangleSubroutine("glGetShaderInfoLog");
    glGetProgramivDfn = (GLGETSHADERIV_FUNC)WrangleSubroutine("glGetProgramiv");
    glGetProgramInfoLogDfn = (GLGETSHADERINFOLOG_FUNC)WrangleSubroutine("glGetProgramInfoLog");

    //--L
    glLinkProgramDfn = (GLLINKPROGRAM_FUNC)WrangleSubroutine("glLinkProgram");

    //--S
    glShaderSourceDfn = (GLSHADERSOURCE_FUNC)WrangleSubroutine("glShaderSource");

    //--U
    glUseProgramDfn = (GLUSEPROGRAM_FUNC)WrangleSubroutine("glUseProgram");*/

    DebugManager::PopPrint("[GL Wrangling] Done\n");
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
ALLEGRO_DISPLAY *GLManager::GetDisplay()
{
    return mDisplay;
}

//====================================== Static Functions =========================================
void GLManager::TargetDisplay()
{
    al_set_target_backbuffer(Global::Shared()->gGLManager->GetDisplay());
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
