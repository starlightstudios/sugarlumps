///======================================= DebugManager ===========================================
//--Manager that is used to "Stack" print statements together without the different levels of debugs
//  prints needing to interact with one another. Makes printing debug information smoother and easier.
//--Most of the functions here are statics.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class DebugManager
{
    private:
    //--System
    DebugManager();
    ~DebugManager();

    //--Flags
    //--Stack controller
    static bool xFullDisable;
    static StarlightLinkedList *xSuppressList;

    protected:

    public:

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    //--Update
    //--File I/O
    static void Push(bool pFlag);
    static void Pop();
    static void Print(const char *pString, ...);
    static void PushPrint(bool pFlag, const char *pString, ...);
    static void PopPrint(const char *pString, ...);
    static void ForcePrint(const char *pString, ...);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Debug_PushPrint(lua_State *L);
int Hook_Debug_PopPrint(lua_State *L);
int Hook_Debug_Print(lua_State *L);
