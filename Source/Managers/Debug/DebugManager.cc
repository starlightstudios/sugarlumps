//--Base
#include "DebugManager.h"

//--Classes
//--Definitions
#include "DeletionFunctions.h"

//--Generics
#include "StarlightLinkedList.h"

//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
DebugManager::DebugManager()
{
}
DebugManager::~DebugManager()
{
}
bool DebugManager::xFullDisable = false;
StarlightLinkedList *DebugManager::xSuppressList = new StarlightLinkedList(true);

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
void DebugManager::Push(bool pFlag)
{
    //--Increments the indent counter by 1.
    if(DebugManager::xFullDisable) return;

    bool *nFlag = (bool *)malloc(sizeof(bool));
    *nFlag = pFlag;
    DebugManager::xSuppressList->AddElementAsHead("X", nFlag, &FreeThis);
}
void DebugManager::Pop()
{
    //--Decrements the indent counter by 1.  Cannot go below 0.
    if(DebugManager::xFullDisable) return;
    DebugManager::xSuppressList->RemoveElementI(0);
}
void DebugManager::Print(const char *pString, ...)
{
    //--Prints the specified message with a number of spaces placed ahead of
    //  it.  This gives the appearance of indentation.
    //  Remember to \n the debug arg!
    if(DebugManager::xFullDisable) return;
    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(!rFlag || !(*rFlag)) return;

    for(int i = 0; i < DebugManager::xSuppressList->GetListSize(); i ++) fprintf(stderr, " ");

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
}
void DebugManager::PushPrint(bool pFlag, const char *pString, ...)
{
    //--Macro, combines Print and Push.  Note the push occurs before the print.
    if(DebugManager::xFullDisable) return;
    DebugManager::Push(pFlag);
    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(!rFlag || !(*rFlag)) return;

    for(int i = 0; i < DebugManager::xSuppressList->GetListSize()-1; i ++) fprintf(stderr, " ");

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
}
void DebugManager::PopPrint(const char *pString, ...)
{
    //--Macro, combines Print and Pop.  Note the Pop occurs after the print.
    if(DebugManager::xFullDisable) return;


    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(!rFlag || !(*rFlag))
    {
        DebugManager::Pop();
        return;
    }

    for(int i = 0; i < DebugManager::xSuppressList->GetListSize()-1; i ++) fprintf(stderr, " ");

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
    DebugManager::Pop();
}
void DebugManager::ForcePrint(const char *pString, ...)
{
    //--Regardless of the flag state, print the message with indentation.
    for(int i = 0; i < DebugManager::xSuppressList->GetListSize(); i ++) fprintf(stderr, " ");

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
}

///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void DebugManager::HookToLuaState(lua_State *pLuaState)
{
    lua_register(pLuaState, "Debug_PushPrint", &Hook_Debug_PushPrint);
    lua_register(pLuaState, "Debug_PopPrint", &Hook_Debug_PopPrint);
    lua_register(pLuaState, "Debug_Print", &Hook_Debug_Print);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Debug_PushPrint(lua_State *L)
{
    //Debug_PushPrint(xFlag, String[])
    if(lua_gettop(L) != 2) return 0;

    DebugManager::PushPrint(lua_toboolean(L, 1), lua_tostring(L, 2));

    return 0;
}
int Hook_Debug_PopPrint(lua_State *L)
{
    //Debug_PushPrint(String[])
    if(lua_gettop(L) != 1) return 0;

    DebugManager::PopPrint(lua_tostring(L, 1));

    return 0;
}
int Hook_Debug_Print(lua_State *L)
{
    //Debug_Print(String[])
    if(lua_gettop(L) != 1) return 0;

    DebugManager::Print(lua_tostring(L, 1));

    return 0;
}
