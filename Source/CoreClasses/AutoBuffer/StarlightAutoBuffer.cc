//--Base
#include "StarlightAutoBuffer.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
StarlightAutoBuffer::StarlightAutoBuffer()
{
    //--[StarlightAutoBuffer]
    //--System
    mIsDisabled = false;
    mBufferSize = SAB_START_SIZE;
    mBuffer = (uint8_t *)malloc(sizeof(uint8_t) * mBufferSize);

    //--Cursor
    mCursor = 0;
}
StarlightAutoBuffer::~StarlightAutoBuffer()
{
    free(mBuffer);
}

///===================================== Property Queries =========================================
int StarlightAutoBuffer::GetSize()
{
    if(mIsDisabled) return 0;
    return mBufferSize;
}
int StarlightAutoBuffer::GetCursor()
{
    if(mIsDisabled) return 0;
    return mCursor;
}
uint8_t *StarlightAutoBuffer::GetRawData()
{
    if(mIsDisabled) return NULL;
    return mBuffer;
}
uint8_t *StarlightAutoBuffer::LiberateData()
{
    ///--[Documentation]
    //--Once the data is liberated, this class has surrendered its data pointer and will disable itself,
    //  becoming useless. It should then be deleted, or call Reinitialize() to use it again.
    if(mIsDisabled) return NULL;

    //--Disable
    uint8_t *rOldData = mBuffer;
    mIsDisabled = true;

    //--Clear reference to the data, or it will be freed when the class is deleted!
    mBuffer = NULL;
    return rOldData;
}

///======================================== Manipulators ==========================================
void StarlightAutoBuffer::Reinitialize()
{
    ///--[Documentation]
    //--If the class has been disabled by a LiberateData() call, it can be restarted by using this
    //  function. If it hasn't, this call will do nothing.
    //--You could ostensibly create a new class to get the same effect, but it may be useful to
    //  re-use the same pointer in a while loop.
    if(!mIsDisabled) return;

    //--Allocate and reset.
    mIsDisabled = false;
    mBufferSize = SAB_START_SIZE;
    mBuffer = (uint8_t *)malloc(sizeof(uint8_t) * mBufferSize);
    mCursor = 0;
}
int StarlightAutoBuffer::AppendNull()
{
    //--Appends a single null character. Useful with AppendStringWithoutNull() to assemble a very
    //  long string.
    if(mIsDisabled) return 0;

    //--Length check.
    AllocateMoreSpace(1);

    //--Append.
    mBuffer[mCursor] = '\0';
    mCursor ++;

    //--Always return 1.
    return 1;
}
int StarlightAutoBuffer::AppendCharacter(char pCharacter)
{
    //--Appends a single character (which *can* be a null!).
    if(mIsDisabled) return 0;

    //--Length check.
    AllocateMoreSpace(1);

    //--Append.
    mBuffer[mCursor] = pCharacter;
    mCursor ++;

    //--Always return 1.
    return 1;
}
int StarlightAutoBuffer::AppendString(const char *pString)
{
    //--Simply appends the given string to the buffer, including the trailing NULL character.
    if(!pString || mIsDisabled) return 0;

    //--Length check.
    int32_t tLen = (int32_t)strlen(pString);
    int32_t tTrueLen = (sizeof(char) * (tLen+1));
    AllocateMoreSpace(tTrueLen);

    //--Append.
    for(int i = 0; i < tLen; i ++)
    {
        mBuffer[mCursor+0] = pString[i];
        mBuffer[mCursor+1] = '\0';
        mCursor ++;
    }
    mCursor ++;

    //--Done.
    return tTrueLen;
}
int StarlightAutoBuffer::AppendStringWithoutNull(const char *pString)
{
    //--Appends the given string, but doesn't add the NULL at the end.
    if(!pString || mIsDisabled) return 0;

    //--Length check.
    int32_t tLen = (int32_t)strlen(pString);
    AllocateMoreSpace(tLen);

    //--Append.
    for(int i = 0; i < tLen; i ++)
    {
        mBuffer[mCursor] = pString[i];
        mCursor ++;
    }

    //--Done.
    return tLen;
}
int StarlightAutoBuffer::AppendStringWithLen(const char *pString)
{
    ///--[Documentation]
    //--Given a string, appends the length of that string as a single byte then the string itself,
    //  not including the trailing NULL. Returns how many letters were added.
    //--Note that this is only for *very short strings*. If more than 256 characters are in the
    //  string, it will lose accuracy since the string header is a single byte. If you need a
    //  very long string that needs a pre-sized length, do it manually.
    if(!pString || mIsDisabled) return 0;

    //--Append the length as the 0th letter.
    int32_t tLen = (int32_t)strlen(pString);
    AllocateMoreSpace(sizeof(uint8_t) * 1);
    mBuffer[mCursor] = (uint8_t)tLen;
    mCursor ++;

    //--Append the string with no NULL.
    return AppendStringWithoutNull(pString) + (sizeof(uint8_t) * 1);
}
int StarlightAutoBuffer::AppendStringWithLen(const char *pString, const char *pDefault)
{
    //--Overload. If the pString is NULL, the pDefault is used instead. If pDefault is NULL, then
    //  we use a defined internal default.
    if(pString) return AppendStringWithLen(pString);
    if(pDefault) return AppendStringWithLen(pDefault);
    return AppendStringWithLen(SAB_DEFAULT_STRING);
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
void StarlightAutoBuffer::AllocateMoreSpace(int pAddingSize)
{
    //--Called at the beginning of each append function, this checks the expected addition against
    //  the current buffer size. If more size is needed, more is added. If not, nothing happens.
    uint32_t tNewNeededSize = mCursor + pAddingSize;
    if(tNewNeededSize < mBufferSize) return;

    //--More space is needed!
    while(mBufferSize <= tNewNeededSize) mBufferSize += SAB_START_SIZE;

    //--Now we know how much space we need. Set it.
    mBuffer = (uint8_t *)realloc(mBuffer, sizeof(uint8_t) * mBufferSize);
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
