///==================================== StarlightAutoBuffer =======================================
//--Basically a stream stored in RAM, the StarlightAutoBuffer stores a buffer of generic data and a cursor.
//  Data can be added to the buffer, and it can be seeked around in. The buffer has no specific size,
//  instead increasing or decreasing size as the user adds data.
//--When/if the data is 'liberated', the class is disabled and cannot be used anymore. It should be
//  deleted immediately afterwards.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define SAB_START_SIZE 256
#define SAB_DEFAULT_STRING "NOSTRING"

///========================================== Classes =============================================
class StarlightAutoBuffer
{
    private:
    //--System
    bool mIsDisabled;
    uint32_t mBufferSize;
    uint8_t *mBuffer;

    //--Cursor
    uint32_t mCursor;

    protected:

    public:
    //--System
    StarlightAutoBuffer();
    ~StarlightAutoBuffer();

    //--Public Variables
    //--Property Queries
    int GetSize();
    int GetCursor();
    uint8_t *GetRawData();
    uint8_t *LiberateData();

    //--Manipulators
    void Reinitialize();
    int AppendNull();
    int AppendCharacter(char pCharacter);
    int AppendString(const char *pString);
    int AppendStringWithoutNull(const char *pString);
    int AppendStringWithLen(const char *pString);
    int AppendStringWithLen(const char *pString, const char *pDefault);

    //--Core Methods
    private:
    //--Private Core Methods
    void AllocateMoreSpace(int pAddingSize);

    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
