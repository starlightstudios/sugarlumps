//--Base
#include "StarlightDataList.h"

//--Classes
//--CoreClasses
#include "DeletionFunctions.h"
#include "StarlightLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
StarlightDataList::StarlightDataList()
{
    //--[RootObject]
    //--System
    mType = 0;//POINTER_TYPE_DATALIST;

    //--[StarlightDataList]
    //--System
    mStrictFlag = false;
    mErrorFlag = DATALIST_ERROR_NONE;

    //--Storage
    mDataList = new StarlightLinkedList(true);
}
StarlightDataList::~StarlightDataList()
{
    delete mDataList;
    mDataList = NULL;
}

///===================================== Property Queries =========================================
int StarlightDataList::GetDataErrorCode()
{
    return mErrorFlag;
}
int StarlightDataList::GetDataListSize()
{
    return mDataList->GetListSize();
}
void *StarlightDataList::FetchDataEntry(const char *pName)
{
    return mDataList->GetElementByName(pName);
}

///======================================== Manipulators ==========================================
void StarlightDataList::SetStrictFlag(bool pFlag)
{
    mStrictFlag = pFlag;
}
void StarlightDataList::ResetErrorCode()
{
    mErrorFlag = DATALIST_ERROR_NONE;
}
void StarlightDataList::DefineEntry(const char *pName, void *pPtr, DeletionFunctionPtr pDeletionPtr)
{
    //--Allows the StarlightDataList to add new entries if the mStrictFlag is on, by flipping it off first!
    //  Since this can only be done in DefineEntry, this is effectively an initializer.
    bool tOldFlag = mStrictFlag;
    mStrictFlag = false;
    AddDataEntry(pName, pPtr, pDeletionPtr);
    mStrictFlag = tOldFlag;
}
void StarlightDataList::AddDataEntry(const char *pName, void *pPtr, DeletionFunctionPtr pDeletionPtr)
{
    //--Prevent duplicates from appearing in the list, unless the name is "X", which is a special
    //  name indicating duplication is okay (since there will not be name fetching of that entry
    //  anyway).
    if(!pName || !pPtr) return;

    bool tFoundOnListAlready = false;
    if(strcmp(pName, "X") && strncmp(pName, "X|", 2))
    {
        int mSlot = mDataList->GetSlotOfElementByName(pName);
        while(mSlot != -1)
        {
            mDataList->RemoveElementI(mSlot);
            mSlot = mDataList->GetSlotOfElementByName(pName);
            tFoundOnListAlready = true;
        }
    }

    //--The strictness flag will block this if the entry was not found and deleted beforehand.
    if(mStrictFlag && !tFoundOnListAlready)
    {
        fprintf(stderr, "StarlightDataList::AddDataEntry:  Failed, strict flag is on and entry %s does not exist\n", pName);
        return;
    }
    mDataList->AddElement(pName, pPtr, pDeletionPtr);
}
void StarlightDataList::AddDataEntry(const char *pName, void *pPtr)
{
    AddDataEntry(pName, pPtr, &FreeThis);
}
void StarlightDataList::AddDataEntry(const char *pName, void *pPtr, bool pBlocker)
{
    AddDataEntry(pName, pPtr, &DontDeleteThis);
}
void StarlightDataList::AddDataEntryN(const char *pName, float pNumber)
{
    //--Macro to quickly add an entry as a floating point.
    if(!pName) return;
    float *nNumPtr = (float *)malloc(sizeof(float));
    *nNumPtr = pNumber;
    AddDataEntry(pName, nNumPtr, &FreeThis);
}
void StarlightDataList::AddDataEntryS(const char *pName, const char *pString)
{
    //--Macro to quickly add an entry as a string.
    if(!pName || !pString) return;
    char *nString = (char *)malloc(sizeof(char) * (strlen(pString) + 1));
    strcpy(nString, pString);
    AddDataEntry(pName, nString, &FreeThis);
}

///======================================== Core Methods ==========================================
void StarlightDataList::ClearDataList()
{
    mDataList->ClearList();
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void StarlightDataList::HookToLuaState(lua_State *pLuaState)
{
    /* DataList_Define(sName, "N", fNumber)
       DataList_Define(sName, "S", sString)
       Defines the data in advance of setting it. Is otherwise identical to SetData, however, if
       the strict flag is on, then Define can create new entries while SetData cannot. */
    lua_register(pLuaState, "DataList_Define", &Hook_DataList_Define);

    /* DataList_SetData(sName, "N", fNumber)
       DataList_SetData(sName, "S", sString)
       Pushes data onto the data list with the type specified. Do NOT mix types or undefined
       behavior will result. If an entry is not found, it is created.  It may be prudent, when
       naming your variables, to prefix them with their type ala Lua (iValue, fValue, sString) */
    lua_register(pLuaState, "DataList_SetData", &Hook_DataList_SetData);

    /* DataList_GetData(sName, "N")
       DataList_GetData(sName, "S")
       Returns data from the data list with the type specified. Again, do NOT mix types!
       If an entry is not found, a default value of 0 is returned. */
    lua_register(pLuaState, "DataList_GetData", &Hook_DataList_GetData);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_DataList_Define(lua_State *L)
{
    //DataList_Define(sName, "N", fNumber)
    //DataList_Define(sName, "S", sString)
    int tArgs = lua_gettop(L);
    if(tArgs != 3) return LuaArgError("DataList_Define");

    //--Retrieval.
    StarlightDataList *rList = (StarlightDataList *)DataLibrary::Fetch()->rActiveObject;
    if(!rList) return LuaTypeError("DataList_Define");

    //--Switching type.
    const char *rTypeString = lua_tostring(L, 2);

    //--Numerical.
    if(!strcmp(rTypeString, "N"))
    {
        float *nNumPtr = (float *)malloc(sizeof(float));
        *nNumPtr = lua_tonumber(L, 3);

        rList->DefineEntry(lua_tostring(L, 1), nNumPtr, &FreeThis);
    }
    //--String.
    else if(!strcmp(rTypeString, "S"))
    {
        const char *rAddString = lua_tostring(L, 3);
        char *nString = (char *)malloc(sizeof(char) * (strlen(rAddString)+ 1));
        strcpy(nString, rAddString);
        rList->DefineEntry(lua_tostring(L, 1), nString, &FreeThis);
    }
    //--Unhandled type.
    else
    {
        fprintf(stderr, "DataList_SetData:  Failed, cannot resolve type %s.\n", rTypeString);
    }

    return 0;
}
int Hook_DataList_SetData(lua_State *L)
{
    //DataList_SetData(sName, "N", fNumber)
    //DataList_SetData(sName, "S", sString)
    int tArgs = lua_gettop(L);
    if(tArgs != 3) return LuaArgError("DataList_SetData");

    //--Retrieval.
    StarlightDataList *rList = (StarlightDataList *)DataLibrary::Fetch()->rActiveObject;
    if(!rList) return LuaTypeError("DataList_SetData");

    //--Switch type.
    const char *rTypeString = lua_tostring(L, 2);

    //--Numerical.
    if(!strcmp(rTypeString, "N"))
    {
        float *nNumPtr = (float *)malloc(sizeof(float));
        *nNumPtr = lua_tonumber(L, 3);

        rList->AddDataEntry(lua_tostring(L, 1), nNumPtr, &FreeThis);
    }
    //--String.
    else if(!strcmp(rTypeString, "S"))
    {
        const char *rAddString = lua_tostring(L, 3);
        char *nString = (char *)malloc(sizeof(char) * (strlen(rAddString)+ 1));
        strcpy(nString, rAddString);
        rList->AddDataEntry(lua_tostring(L, 1), nString, &FreeThis);
    }
    //--Unhandled type.
    else
    {
        fprintf(stderr, "DataList_SetData:  Failed, cannot resolve type %s\n", rTypeString);
    }

    return 0;
}
int Hook_DataList_GetData(lua_State *L)
{
    //DataList_GetData(sName, "N")
    //DataList_GetData(sName, "S")
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("DataList_GetData", L);

    //--Retrieval.
    StarlightDataList *rList = (StarlightDataList *)DataLibrary::Fetch()->rActiveObject;
    if(!rList) return LuaTypeError("DataList_GetData", L);

    //--Check the entry. If it doesn't exist, return 0.
    void *rCalledPtr = rList->FetchDataEntry(lua_tostring(L, 1));
    if(!rCalledPtr)
    {
        fprintf(stderr, "DataList_GetData:  Failed, entry %s not found.\n", lua_tostring(L, 1));
        lua_pushnumber(L, 0);
        return 1;
    }

    //--Switch type.
    const char *rTypeString = lua_tostring(L, 2);

    //--Numeric.
    if(!strcmp(rTypeString, "N"))
    {
        float *rFloatPtr = (float *)rCalledPtr;
        lua_pushnumber(L, *rFloatPtr);
    }
    //--String.
    else if(!strcmp(rTypeString, "S"))
    {
        char *rStringPtr = (char *)rCalledPtr;
        lua_pushstring(L, rStringPtr);
    }
    //--Unhandled type. Pushes 0.
    else
    {
        fprintf(stderr, "DataList_GetData:  Failed, %s cannot resolve type %s.\n", lua_tostring(L, 1), rTypeString);
        lua_pushnumber(L, 0);
    }

    return 1;
}
