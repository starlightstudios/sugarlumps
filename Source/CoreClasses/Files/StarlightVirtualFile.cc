//--Base
#include "StarlightVirtualFile.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
StarlightVirtualFile::StarlightVirtualFile(const char *pPath)
{
    //--Error check (if this errors out, the file won't work)
    //fprintf(stderr, "Virtual file opening %s\n", pPath);
    mIsReady = false;
    if(!pPath) return;

    //--Open the file and error check
    FILE *fInfile = fopen(pPath, "rb");
    if(!fInfile)
    {
        //fprintf(stderr, "VF file not found\n");
        return;
    }

    //--Scan the file for its length (may need to optimize)
    fseek(fInfile, 0, SEEK_END);
    mFileLength = ftell(fInfile);
    rewind(fInfile);
    //fprintf(stderr, "StarlightVirtualFile:  Length is %i\n", mFileLength);

    //--Allocate
    mData = (unsigned char *)malloc(sizeof(unsigned char) * mFileLength);

    //--Read
    fread(mData, sizeof(unsigned char) * mFileLength, 1, fInfile);

    //--File had no errors, it can now be used
    mIsReady = true;
    mFilePointer = 0;
    fclose(fInfile);
}
StarlightVirtualFile::~StarlightVirtualFile()
{
    free(mData);
}

///===================================== Property Queries =========================================
bool StarlightVirtualFile::IsReady()
{
    return mIsReady;
}
long int StarlightVirtualFile::GetLength()
{
    return mFileLength;
}
long int StarlightVirtualFile::GetCurPosition()
{
    return mFilePointer;
}
void StarlightVirtualFile::Read(void *pDest, int pSizePerItem, int pItemsTotal)
{
    if(!pDest) return;

    memcpy(pDest, &mData[mFilePointer], pSizePerItem * pItemsTotal);
    mFilePointer += pSizePerItem * pItemsTotal;
}
char *StarlightVirtualFile::ReadLenString()
{
    //--Assuming the file is in position, reads a length/string combo and returns it as a newly
    //  allocated string. The length is always assumed to be one byte long.
    uint8_t tLen = 0;
    Read(&tLen, sizeof(uint8_t), 1);

    //--Allocate name.
    char *nString = (char *)malloc(sizeof(char) * (tLen + 1));
    Read(nString, sizeof(char), tLen);
    nString[tLen] = '\0';
    return nString;
}
void StarlightVirtualFile::ReadImage(void *pDest, int pXOrig, int pYOrig, int pXNew, int pYNew, int pSizePerItem)
{
    //--Special case when reading an image which requires a specific size, but
    //  has been compressed by the EIM.  This function will "pad" the edges of
    //  the image with transparent pixels for you!
    uint8_t *rDest = (uint8_t *)pDest;
    if(!rDest) return;

    //fprintf(stderr, "Padding %i to %i\n", pXOrig, pXNew);

    int i = 0;
    for(int y = pYNew-1; y >= pYOrig; y --)
    {
        //fprintf(stderr, "Y: %i - ", y);
        for(int x = 0; x < pXNew * pSizePerItem; x ++)
        {
            rDest[i] = 0;
            i ++;
        }
        //fprintf(stderr, "%i\n", i);
    }
    for(int y = pYOrig-1; y >= 0; y --)
    {
        //fprintf(stderr, "Y: %i - ", y);
        for(int x = 0; x < pXOrig * pSizePerItem; x ++)
        {
            rDest[i] = mData[mFilePointer];
            mFilePointer ++;
            i ++;
        }
        //fprintf(stderr, "%i - ", i);
        for(int x = pXOrig * pSizePerItem; x < (pXNew * pSizePerItem); x ++)
        {
            rDest[i] = 0;
            i ++;
        }
        //fprintf(stderr, "%i\n", i);
    }
    //fprintf(stderr, "Padding completed\n");
}
int StarlightVirtualFile::Text_ReadInt(int pCharacters, bool pIsSkippingWhite)
{
    ///--[DOCUMENTATION]
    //--Reads out a number as though this were a text file.
    //  If 0 is passed for Characters, it will read until it hits a non-numeric
    //  character. Numbers cannot exceed 128 characters.
    if(!mIsReady) return 0;
    //int mOldPos = mFilePointer;

    //--Set the max
    if(pCharacters < 1) pCharacters = VF_MAXCHARS;

    //--Allocate and clear
    char mBuffer[VF_MAXCHARS];
    for(int i = 0; i < VF_MAXCHARS; i ++) mBuffer[i] = '\0';

    //--Read characters stepwise.
    int mPos = 0;
    char mCurChar = mData[mFilePointer];
    while(mCurChar >= '0' && mCurChar <= '9' && mFilePointer < mFileLength && mPos < pCharacters)
    {
        mBuffer[mPos] = mCurChar;
        mPos ++;
        mFilePointer ++;
        mCurChar = mData[mFilePointer];
    }

    //--If ordered, skip over whitespace.  Useful when using whitespace
    //  delimited text files.  Tab DOES count.
    if(pIsSkippingWhite) Text_SkipWhite();

    //--Return it
    return atoi(mBuffer);
}
char *StarlightVirtualFile::Text_ReadString(bool pIsSkippingWhite)
{
    ///--[DOCUMENTATION]
    //--Reads characters until it hits a whitepsace, then copies those into a
    //  string and returns them.

    //--First, found out how many characters are needed to be stored.
    int mStartPos = mFilePointer;
    int mLen = FindCharsToNextWhite();
    mFilePointer += mLen;

    //--Allocate a string and copy the characters into it.
    char *nLine = (char *)malloc(sizeof(char) * (mLen + 1));
    for(int i = 0; i < mLen; i ++)
    {
        nLine[i] = mData[mStartPos+i];
    }
    nLine[mLen] = '\0';

    //--If the first and last letters are quotes, remove them and shift.
    if(nLine[0] == '"' && nLine[mLen-1] == '"')
    {
        nLine[mLen-1] = '\0';
        for(int i = 0; i < mLen; i ++) nLine[i] = nLine[i+1];
    }

    //--If ordered, skip over whitespace.  Useful when using whitespace
    //  delimited text files.  Tab DOES count.
    if(pIsSkippingWhite) Text_SkipWhite();

    return nLine;
}
char *StarlightVirtualFile::GetLine()
{
    //--From the current position, copy characters into a return string until a
    //  newline is encountered.

    //--First, found out how many characters are needed to be stored.
    unsigned int mStartPos = mFilePointer;
    unsigned int mCurPos = mStartPos;

    //--Windows tends to put 13 and then 10 for newlines.
    //  Unix often does a single 10.  This needs to be dealt with.
    bool tDone = false;
    bool tDoubleCase = false;
    while(!tDone)
    {
        mCurPos ++;
        if(mData[mCurPos] == 13 && mData[mCurPos+1] == 10)
        {
            tDoubleCase = true;
            tDone = true;
        }
        else if(mData[mCurPos] == 10)
        {
            tDone = true;
        }
        else if(mCurPos >= mFileLength)
        {
            tDone = true;
        }
    }

    mFilePointer = mCurPos+1;
    if(tDoubleCase)
    {
        mFilePointer = mCurPos+2;//Add two for the 13 and 10 bytes.
    }
    int mLineLen = mCurPos - mStartPos;

    char *nLine = (char *)malloc(sizeof(char) * (mLineLen + 1));
    for(int i = 0; i < mLineLen; i ++)
    {
        nLine[i] = mData[mStartPos+i];
    }
    nLine[mLineLen] = '\0';

    return nLine;
}

///======================================== Manipulators ==========================================
void StarlightVirtualFile::Seek(int pPosition)
{
    mFilePointer = pPosition;
}
void StarlightVirtualFile::SeekRelative(int pChange)
{
    mFilePointer += pChange;
    if(mFilePointer < 0) mFilePointer = 0;
    if(mFilePointer >= mFileLength) mFilePointer = mFileLength-1;
}
void StarlightVirtualFile::Text_SkipWhite()
{
    ///--[DOCUMENTATION]
    //--Skip over white spaces until you hit something non-white.
    char mCurChar = mData[mFilePointer];
    while((mCurChar == ' ' || mCurChar == 13 || mCurChar == '\t' || mCurChar == 10) && mFilePointer < mFileLength)
    {
        mFilePointer ++;
        mCurChar = mData[mFilePointer];
    }
}
void StarlightVirtualFile::Text_SkipLine()
{
    //--Carriage returns are always a 13 followed by a 10.  If you spot this, you're done.
    //fprintf(stderr, "Skipping Line |");
    while(!(mData[mFilePointer] == 13 && mData[mFilePointer+1] == 10) && mFilePointer < mFileLength)
    {
        //fprintf(stderr, "%c", mData[mFilePointer]);
        mFilePointer ++;
    }
    mFilePointer += 2;
    //fprintf(stderr, "|done\n");
}
void StarlightVirtualFile::Text_SkipLines(int pLines)
{
    for(int i = 0; i < pLines; i ++) Text_SkipLine();
}
void StarlightVirtualFile::Text_SkipToNonComment(char pConstantCharacter)
{
    //--Skip lines until a non-whitespace character is found that is also NOT
    //  the provided comment character.
    Text_SkipWhite();

    char mReadChar = mData[mFilePointer];
    while(mReadChar == pConstantCharacter)
    {
        Text_SkipLine();
        mReadChar = mData[mFilePointer];
    }
}

///======================================== Core Methods ==========================================
int StarlightVirtualFile::FindCharsToNextWhite()
{
    ///--[DOCUMENTATION]
    //--Returns how many characters there are from the current file position to
    //  the next whitespace.  Respects "".
    bool mIsInQuotes = false;
    unsigned int mStartPos = mFilePointer;
    unsigned int mCurPos = mFilePointer;
    while(true)
    {
        if(mData[mCurPos] == '"')
        {
            mIsInQuotes = !mIsInQuotes;
        }
        else
        {
            if(!mIsInQuotes)
            {
                //--EOF
                if(mCurPos >= mFileLength-1) break;
                //--Carriage return
                if(mData[mCurPos] == 13 && mData[mCurPos+1] == 10) break;
                //--Space
                if(mData[mCurPos] == ' ') break;
                //--Tab
                if(mData[mCurPos] == '\t') break;
            }
        }
        mCurPos ++;
    }
    return mCurPos - mStartPos;
}
void StarlightVirtualFile::SkipLenString()
{
    //--Same basic functionality as ReadLenString(), except doesn't allocate or return anything. It
    //  merely skips over the string since malloc() is a fairly slow function.
    uint8_t tLen = 0;
    Read(&tLen, sizeof(uint8_t), 1);
    SeekRelative(tLen);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
void StarlightVirtualFile::Write(const char *pFileDest)
{
    FILE *fOutfile = fopen(pFileDest, "wb");
    if(!fOutfile) return;

    fwrite(mData, sizeof(unsigned char), mFileLength, fOutfile);

    fclose(fOutfile);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
unsigned char *StarlightVirtualFile::GetData()
{
    return mData;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================

