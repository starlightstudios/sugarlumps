//--Base
#include "StarlightFileSystem.h"

//--Classes
//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

#if defined _BOOST_FILESYSTEM_
#include <boost/filesystem.hpp>
using namespace boost::filesystem;
#elif defined _ALLEGRO_FILESYSTEM_

#endif

//--Libraries
//--Managers
#include "DebugManager.h"

///========================================== System ==============================================
StarlightFileSystem::StarlightFileSystem()
{
    //--System
    mTotalFiles = 0;
    mDirectoryInfo = NULL;
    mIsThisARecursion = false;

    //--Storage
    mLastParsedDirectory = NULL;

    //--Iteration
    mIterationPoint = 0;

    //--Public Variables
    mIgnoreFolders = false;
    mIgnoreZerothEntry = false;
    mIsRecursive = true;
}
StarlightFileSystem::~StarlightFileSystem()
{
    Clear();
}
void StarlightFileSystem::DeleteThis(void *pPtr)
{
    delete ((StarlightFileSystem *)pPtr);
}
void StarlightFileSystem::DeleteFileInfo(void *pPtr)
{
    FileInfo *rInfo = (FileInfo *)pPtr;
    free(rInfo->mPath);
    free(rInfo);
}

///===================================== Property Queries =========================================
int StarlightFileSystem::GetTotalEntries()
{
    return mTotalFiles;
}
FileInfo *StarlightFileSystem::GetEntry(int pSlot)
{
    if(pSlot < 0 || pSlot >= mTotalFiles) return NULL;
    return mDirectoryInfo[pSlot];
}
char *StarlightFileSystem::GetLastParsedDirectory()
{
    return mLastParsedDirectory;
}
char *StarlightFileSystem::GetIteratedPath()
{
    if(mIterationPoint < 0 || mIterationPoint >= mTotalFiles) return NULL;
    return mDirectoryInfo[mIterationPoint]->mPath;
}

///======================================== Manipulators ==========================================
void StarlightFileSystem::ScanDirectory(const char *pPath)
{
    //--Orders the given directory to be scanned into memory.  Pass NULL to clear implicitly.
    Clear();
    if(!pPath) return;

    //--Order a scan to take place.
    StarlightLinkedList *tFileInfoList = SubScanDirectory(pPath, mIsRecursive);
    if(!tFileInfoList) return;

    //--Copy and store.
    MergeFileInfo(tFileInfoList);
    delete tFileInfoList;

    //--Sort it, directories first.
    SortDirectory();

    //--<DEBUG> Print it
    DebugManager::Push(false);
    DebugManager::Print("Directory scanned: %s\n", pPath);
    DebugManager::Print("Found %i elements.\n", mTotalFiles);
    for(int i = 0; i < mTotalFiles; i ++)
    {
        DebugManager::Print(" %s\n", mDirectoryInfo[i]->mPath);
    }
    DebugManager::Print("Done.\n");
    DebugManager::Pop();
}

///======================================== Core Methods ==========================================
void StarlightFileSystem::Clear()
{
    //--Clears off the existing data and returns the object to a neutral state.
    for(int i = 0; i < mTotalFiles; i ++)
    {
        DeleteFileInfo(mDirectoryInfo[i]);
    }
    free(mDirectoryInfo);
    mDirectoryInfo = NULL;
    mTotalFiles = 0;

    free(mLastParsedDirectory);
    mLastParsedDirectory = NULL;
}
StarlightLinkedList *StarlightFileSystem::SubScanDirectory(const char *pPath, bool pScanDirectories)
{
    ///--[Documentation]
    //--Scanning subroutine. The RLL passed back will contain FileInfo's for every single entry
    //  within the folder. If pScanDirectories is true, this will recursively be called for each
    //  sub-directory within.
    //--Check for NULL on failure.
    //--Note: Unlike other Starlight projects, SugarLumps only supports Allegro. If you need a filesystem
    //  implementation, use the one in the engine project.
    if(!pPath) return NULL;

    //--RLL does not delete its members!
    StarlightLinkedList *nFileInfoList = new StarlightLinkedList(false);

    ///--[Up-One Entry]
    //--0th element is always the "../" pathname.  It means "up one directory".
    //  If a flag is set, we don't create this.
    if(!mIgnoreZerothEntry && !mIsThisARecursion)
    {
        char tBuffer[256];
        strcpy(tBuffer, pPath);
        strcat(tBuffer, "..");

        FileInfo *nEntry = (FileInfo *)malloc(sizeof(FileInfo));
        nEntry->mIsDirectory = true;
        nEntry->mIsFile = false;
        nEntry->mPath = NULL;
        ResetString(nEntry->mPath, tBuffer);
        nFileInfoList->AddElement("X", nEntry, &DontDeleteThis);
    }

    ///--[Root]
    //--Root Entry
    ALLEGRO_FS_ENTRY *tRootEntry = al_create_fs_entry(pPath);
    al_open_directory(tRootEntry);

    //--Store the absolute path of the directory.  Ignore this if this is a recursion.
    if(!mIsThisARecursion)
    {
        //--Temporary buffer.
        char tBuffer[512];
        strcpy(tBuffer, al_get_fs_entry_name(tRootEntry));

        //--Remove '\' and replace with '/'
        for(unsigned int i = 0; i < strlen(tBuffer); i ++)
        {
            if(tBuffer[i] == '\\') tBuffer[i] = '/';
        }

        //--This string needs to have a '/' added onto the end since it's guaranteed to be a
        //  directory.  Add that.
        strcat(tBuffer, "/");

        //--Save the final copy.
        ResetString(mLastParsedDirectory, tBuffer);
    }

    ///--[Scan Entries]
    //--Read all the entries, and store the filenames.
    ALLEGRO_FS_ENTRY *tEntry = al_read_directory(tRootEntry);
    while(tEntry)
    {
        //--Check flags.
        int32_t tFlags = al_get_fs_entry_mode(tEntry);

        //--If this is a directory, and we want to recurse, do that now.
        if(tFlags & ALLEGRO_FILEMODE_ISDIR && pScanDirectories)
        {
            //--Set this flag to false, otherwise we will have duplicate ../ entries.
            mIsThisARecursion = true;

            //--Build it.
            StarlightLinkedList *tSubscanList = SubScanDirectory(al_get_fs_entry_name(tEntry), pScanDirectories);

            //--Copy all the entries down onto our list.
            FileInfo *rScanInfo = (FileInfo *)tSubscanList->PushIterator();
            while(rScanInfo)
            {
                //--Name
                char *tName = tSubscanList->GetIteratorName();
                nFileInfoList->AddElement(tName, rScanInfo, &DontDeleteThis);

                rScanInfo = (FileInfo *)tSubscanList->AutoIterate();
            }

            //--Clean
            delete tSubscanList;
            mIsThisARecursion = false;
        }

        //--Is this a directory, when we are ignoring directories?  If so, don't create a heap
        //  copy, and don't store it.
        if(tFlags & ALLEGRO_FILEMODE_ISDIR && mIgnoreFolders)
        {

        }
        //--Normal behavior.
        else
        {
            //--Buffer.
            char tBuffer[512];
            strcpy(tBuffer, al_get_fs_entry_name(tEntry));

            //--Remove '\' and replace with '/'
            for(unsigned int i = 0; i < strlen(tBuffer); i ++)
            {
                if(tBuffer[i] == '\\') tBuffer[i] = '/';
            }

            //--Tack on a '/' on the end, to indicate this is a directory.
            if(tFlags & ALLEGRO_FILEMODE_ISDIR) strcat(tBuffer, "/");

            //--Create a heap copy.
            FileInfo *nEntry = (FileInfo *)malloc(sizeof(FileInfo));
            nEntry->mIsDirectory = false;
            nEntry->mIsFile = false;
            nEntry->mPath = NULL;
            ResetString(nEntry->mPath, tBuffer);
            //fprintf(stderr, "Entry recorded: %s\n", tBuffer);

            //--Store it.
            nFileInfoList->AddElement("X", nEntry, &DontDeleteThis);
            if(tFlags & ALLEGRO_FILEMODE_ISFILE) nEntry->mIsFile = true;
            if(tFlags & ALLEGRO_FILEMODE_ISDIR) nEntry->mIsFile = true;
        }

        //--Destroy the old entry.
        al_destroy_fs_entry(tEntry);

        //--Iterate up.
        tEntry = al_read_directory(tRootEntry);
    }

    ///--[Finish Up]
    //--Clean.
    al_close_directory(tRootEntry);
    al_destroy_fs_entry(tRootEntry);

    //fprintf(stderr, "Counted %i files\n", tCount);

    //--Pass back the results.
    return nFileInfoList;
}
void StarlightFileSystem::MergeFileInfo(StarlightLinkedList *pFileInfoList)
{
    //--Places the data from the given pFileInfoList into the sized array.  The RLL is left whole
    //  and the caller must delete it.
    //--Entries in the filesystem are pointer-copied, the RLL should not delete them!
    if(!pFileInfoList) return;

    //--Allocate
    mTotalFiles = pFileInfoList->GetListSize();
    mDirectoryInfo = (FileInfo **)malloc(sizeof(FileInfo *) * mTotalFiles);

    //--Copy over in iteration.
    int tSlot = 0;
    FileInfo *rEntry = (FileInfo *)pFileInfoList->PushIterator();
    while(rEntry)
    {
        mDirectoryInfo[tSlot] = rEntry;
        tSlot ++;
        rEntry = (FileInfo *)pFileInfoList->AutoIterate();
    }
}
void StarlightFileSystem::SortDirectory()
{
    //--Optional call.  Normally, filesystems will sort alphabetically, or they may not sort the
    //  data at all.  This call sorts such that directories are first, and otherwise the files are
    //  sorted alphabetically.
    if(mTotalFiles < 2 || !mDirectoryInfo) return;

    qsort(mDirectoryInfo, mTotalFiles, sizeof(FileInfo *), &StarlightFileSystem::SortFunction);
}
int StarlightFileSystem::SortFunction(const void *pInfoA, const void *pInfoB)
{
    //--Sorting function of SortDirectory() used above.
    FileInfo *rInfoA = *((FileInfo **)pInfoA);
    FileInfo *rInfoB = *((FileInfo **)pInfoB);
    if(rInfoA->mIsDirectory && !rInfoB->mIsDirectory)
    {
        return -1;
    }
    else if(!rInfoA->mIsDirectory && rInfoB->mIsDirectory)
    {
        return 1;
    }
    else
    {
        return strcmp(rInfoA->mPath, rInfoB->mPath);
    }
}
void StarlightFileSystem::ResetIterator()
{
    mIterationPoint = 0;
}
void StarlightFileSystem::Iterate()
{
    mIterationPoint ++;
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
bool StarlightFileSystem::FileExists(const char *pPattern, ...)
{
    //--Returns whether or not the named file exists. The pattern is the pathname, but can use
    //  printf-style generation to expand itself if needed.
    if(!pPattern) return false;

    //--Get the variable arg list.
    va_list tArgList;
    va_start(tArgList, pPattern);

    //--Print the args into a buffer.
    char tBuffer[256];
    vsprintf(tBuffer, pPattern, tArgList);
    va_end(tArgList);

    //--The buffer represents the pathname. Check for existence.
    FILE *fCheckFile = fopen(tBuffer, "r");
    if(!fCheckFile) return false;

    //--It exists. Clean up.
    fclose(fCheckFile);
    return true;
}
char *StarlightFileSystem::PareFileName(const char *pPath)
{
    //--Overload. Pares the filename and implicitly keeps the extension.
    return PareFileName(pPath, true);
}
char *StarlightFileSystem::PareFileName(const char *pPath, bool pKeepExtension)
{
    //--Given a filename of some sort, knocks off the rest of the path (absolute or relative) and
    //  returns the filename + extension as a heap-string.  You are responsible for deallocation.
    //--If pKeepExtension is false, the extension is also knocked off.
    //--NULL can be returned on error or an invalid path of some sort.
    if(!pPath) return NULL;
    char *nString = NULL;

    //--Parse backwards to locate the / or \\, depending on OS.
    int tSlashPos = -1;
    uint32_t tLength = strlen(pPath);
    for(int i = tLength-1; i >= 0; i --)
    {
        if(pPath[i] == '/' || pPath[i] == '\\')
        {
            tSlashPos = i;
            break;
        }
    }

    //--No slash was detected.
    if(tSlashPos == -1)
    {
        ResetString(nString, pPath);
        return nString;
    }

    //--Copy and return.
    nString = (char *)malloc(sizeof(char) * (tLength - tSlashPos));
    for(int i = tSlashPos+1; i < (int)tLength; i ++)
    {
        nString[i - tSlashPos - 1] = pPath[i];
        nString[i - tSlashPos - 0] = '\0';
    }

    //--If the extension is not desired, knock it off.
    if(!pKeepExtension)
    {
        for(int i = 0; i < (int)strlen(nString); i ++)
        {
            if(nString[i] == '.')
            {
                nString[i] = '\0';
                break;
            }
        }
    }

    //--<DEBUG>
    //fprintf(stderr, "Pared %s to %s with %i slots\n", pPath, nString, (tLength - tSlashPos));

    return nString;
}
StarlightFileSystem *StarlightFileSystem::Fetch()
{
    //--Unlike most fetch calls, this one returns the top of the Global's FileSystem stack.  It can
    //  therefore be NULL, and must be checked!
    return (StarlightFileSystem *)Global::Shared()->gFileSystemStack->GetTail();
}
bool StarlightFileSystem::IsFileExtension(const char *pPath, const char *pExtension)
{
    //--Returns true if the file extension of pPath is the same as pExtension.
    if(!pPath || !pExtension) return false;

    //--Get the extension's position.
    int tPeriod = -1;
    int tLen = (int)strlen(pPath);
    for(int i = tLen - 1; i >= 0; i --)
    {
        if(pPath[i] == '.')
        {
            tPeriod = i;
            break;
        }
    }

    //--No legal extension!
    if(tPeriod == -1) return false;

    //--Do a comparison case.
    //fprintf(stderr, "File Extension: %s vs %s\n", &pPath[tPeriod], pExtension);
    return (strcasecmp(&pPath[tPeriod], pExtension) == 0);
}

///======================================== Lua Hooking ===========================================
void StarlightFileSystem::HookToLuaState(lua_State *pLuaState)
{
    /* FS_Open(sPath)
       FS_Open(sPath, bRecurseFlag)
       Opens the specified folder on the hard drive as a Starlight FileSystem entry. Each call of Open()
       must be attached to a Close() call as well.  The program automatically stacks the calls
       together so you can iterate into subfolders if you need to. */
    lua_register(pLuaState, "FS_Open", &Hook_FS_Open);

    /* FS_Reset()
       Resets the Starlight FileSystem iterator back to the 0th entry. */
    lua_register(pLuaState, "FS_Reset", &Hook_FS_Reset);

    /* FS_Iterate()
       Iterates up to the next entry on the filesystem, and returns its path. Returns "NULL" if
       there were no more paths in the directory. */
    lua_register(pLuaState, "FS_Iterate", &Hook_FS_Iterate);

    /* FS_Close()
       Closes the currently open Starlight FileSystem entry. */
    lua_register(pLuaState, "FS_Close", &Hook_FS_Close);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_FS_Open(lua_State *L)
{
    //FS_Open(sPath)
    //FS_Open(sPath, bRecurseFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1 && pArgs != 2) return LuaArgError("FS_Open");

    //--Create a new StarlightFileSystem
    StarlightFileSystem *nSystem = new StarlightFileSystem();

    //--Switch this flag if the args call for it.
    if(pArgs == 2)
    {
        nSystem->mIsRecursive = lua_toboolean(L, 2);
    }

    //--Scan it.
    nSystem->ScanDirectory(lua_tostring(L, 1));

    //--Push it as a new entry.
    Global::Shared()->gFileSystemStack->AddElement("X", nSystem, &StarlightFileSystem::DeleteThis);

    return 0;
}
int Hook_FS_Reset(lua_State *L)
{
    //FS_Reset()
    StarlightFileSystem *rSystemHead = StarlightFileSystem::Fetch();
    if(!rSystemHead) return fprintf(stderr, "FS_Reset:  Failed, no open StarlightFileSystem.\n");

    rSystemHead->ResetIterator();

    return 0;
}
int Hook_FS_Iterate(lua_State *L)
{
    //FS_Iterate()
    StarlightFileSystem *rSystemHead = StarlightFileSystem::Fetch();
    if(!rSystemHead)
    {
        fprintf(stderr, "FS_Reset:  Failed, no open StarlightFileSystem.\n");
        lua_pushstring(L, "NULL");
        return 1;
    }

    //--Get the path, and check it agains NULL.
    char *rPath = rSystemHead->GetIteratedPath();
    if(rPath)
    {
        lua_pushstring(L, rPath);
        //free(rPath);
    }
    else
    {
        lua_pushstring(L, "NULL");
    }

    //--Iterate AFTER the path is received.
    rSystemHead->Iterate();

    return 1;
}
int Hook_FS_Close(lua_State *L)
{
    //FS_Close()
    Global::Shared()->gFileSystemStack->DeleteTail();

    return 0;
}
