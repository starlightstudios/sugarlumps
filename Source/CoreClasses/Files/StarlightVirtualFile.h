///=================================== StarlightVirtualFile =======================================
//--Emulates reading and writing with file streams. This is intended to improve loading times. It
//  is much faster to read the entire file in one operation than to read it in hundreds of smaller
//  operations, as the hard-disk has to spin up to do each job. VirtualFiles can still be operated
//  in stream mode to provide backwards compatibility.

#pragma once

///========================================== Includes ============================================
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define VF_MAXCHARS 129

///========================================== Classes =============================================
class StarlightVirtualFile
{
    private:
    //--System
    bool mIsReady;

    //--Storage
    unsigned long int mFileLength;
    unsigned char *mData;

    //--Reading
    unsigned long int mFilePointer;

    protected:

    public:
    //--System
    StarlightVirtualFile(const char *pPath);
    ~StarlightVirtualFile();

    //--Public Variables
    //--Property Queries
    bool IsReady();
    long int GetLength();
    long int GetCurPosition();
    void Read(void *pDest, int pSizePerItem, int pItemsTotal);
    char *ReadLenString();
    void ReadImage(void *pDest, int pXOrig, int pYOrig, int pXNew, int pYNew, int pSizePerItem);
    int Text_ReadInt(int pCharacters, bool pIsSkippingWhite);
    char *Text_ReadString(bool pIsSkippingWhite);
    char *GetLine();

    //--Manipulators
    void Seek(int pPosition);
    void SeekRelative(int pChange);
    void Text_SkipWhite();
    void Text_SkipLine();
    void Text_SkipLines(int pLines);
    void Text_SkipToNonComment(char pConstantCharacter);

    //--Core Methods
    int FindCharsToNextWhite();
    void SkipLenString();

    //--Update
    //--File I/O
    void Write(const char *pFileDest);

    //--Drawing
    //--Pointer Routing
    unsigned char *GetData();

    //--Lua Hooking
};

//--Hooking Functions

