///==================================== StarlightFileSystem =======================================
//--A filesystem that runs independent of OS, and also of ridiculous filesystem idiosyncracies like
//  Boost's.  Neatly abstracts all that crap away from you, so you don't need to worry about it.
//--The exact internal behavior is determined by a definition, which will be either _BOOST_FILESYSTEM_
//  or _ALLEGRO_FILESYSTEM_ depending on which library you're using.
//--The StarlightFileSystem uses no static variables can can recursively instantiate.  Alternately, you
//  can order it to recursively get directories in the iterator, and the returned pathnames will
//  have the directories appended.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
//--Represents an entry on the hard drive, either a file or directory.
typedef struct
{
    char *mPath;
    bool mIsDirectory;
    bool mIsFile;
}FileInfo;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class StarlightFileSystem
{
    private:
    //--System
    int mTotalFiles;
    FileInfo **mDirectoryInfo;
    bool mIsThisARecursion;

    //--Storage
    char *mLastParsedDirectory;

    //--Iteration
    int mIterationPoint;

    protected:

    public:
    //--System
    StarlightFileSystem();
    ~StarlightFileSystem();
    static void DeleteThis(void *pPtr);
    static void DeleteFileInfo(void *pPtr);

    //--Public Variables
    bool mIgnoreFolders;
    bool mIgnoreZerothEntry;
    bool mIsRecursive;

    //--Property Queries
    int GetTotalEntries();
    FileInfo *GetEntry(int pSlot);
    char *GetLastParsedDirectory();
    char *GetIteratedPath();

    //--Manipulators
    void ScanDirectory(const char *pPath);

    //--Core Methods
    void Clear();
    StarlightLinkedList *SubScanDirectory(const char *pPath, bool pScanDirectories);
    void MergeFileInfo(StarlightLinkedList *pFileInfoList);
    void SortDirectory();
    static int SortFunction(const void *pInfoA, const void *pInfoB);
    void ResetIterator();
    void Iterate();

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static bool FileExists(const char *pPattern, ...);
    static char *PareFileName(const char *pPath);
    static char *PareFileName(const char *pPath, bool pKeepExtension);
    static StarlightFileSystem *Fetch();
    static bool IsFileExtension(const char *pPath, const char *pExtension);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_FS_Open(lua_State *L);
int Hook_FS_Reset(lua_State *L);
int Hook_FS_Iterate(lua_State *L);
int Hook_FS_Close(lua_State *L);
