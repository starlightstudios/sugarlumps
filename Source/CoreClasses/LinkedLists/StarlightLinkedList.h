//--[StarlightLinkedList]
//--General-purpose linked list, built for the Starlight^3 engine.
//--Features:
//  Iterator handling
//  Automatic garbage handling through deletion pointers
//  Random-pointer allows for easier pruning
//  Does not rely on the STL

//--Notes:
//--The Iterator stack is not 'stable' with the Random pointer. Ideally, do not delete components
//  if there is an iterator on the stack. If you need to prune something, do it at the end of a
//  'tick' when nothing is accessing it. In addition, do not add components while the list is
//  iterating as it may or may not respect them depending on where they were added.
//--This will eventually be handled by adding a pending addition and deletion list.

#pragma once

//--Definitions.
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//--Switches
#define SLL_MERGING_DEACTIVATES_BUCKETING //If this flag is switched on, then running MergeBucket()
                                          //will turn bucketing off afterwards.
#define SLL_DUMPING_DEACTIVATES_BUCKETING //If this flag is switched on, then running DumpBucket()
                                          //will turn bucketing off.

//--If this flag is defined, an error warning is barked when an entry is deleted but has NULL
//  as its deletion function.
#define SLL_BARK_ON_NO_DELETION_POINTER

//--Deletion Function Pointer is a standardized pointer type used to delete objects or free them.
//  It should be declared elsewhere in the program surrounded by these #ifdef braces.
#ifndef _DeletionFunctionPtr_
#define _DeletionFunctionPtr_
typedef void(*DeletionFunctionPtr)(void *);
#endif

//--Sorting function, used for sorting the list. Standard type used for stuff like qsort() as well.
#ifndef _SortFunctionPtr_
#define _SortFunctionPtr_
typedef int(*SortFnPtr)(const void *, const void *);
#endif

//--Standardized function for comparing two strings. Used for sorting the names of the entries.
typedef int(*StringCompareFuncPtr)(const char *, const char *);

//--The SLL is constructed of these entries, linked to one another. The entry itself is garbage
//  collected automatically, but can be retrieved if really necessary. 99% of RLL activity does
//  not require the programmer to access these entries.
typedef struct StarlightLinkedListEntry StarlightLinkedListEntry;
struct StarlightLinkedListEntry
{
    char *mName;
    StarlightLinkedListEntry *rNext;
    StarlightLinkedListEntry *rPrev;
    void *rData;
    void (*rDeletionFunc)(void *);
};

//--These entries comprise the bucketed addition of entities. The list only goes forwards,
//  and does not have iterator support.
struct StarlightLinkedListBucketEntry
{
    bool mAppendsAtHead;
    char *mName;
    void *rData;
    void (*rDeletionFunc)(void *);
    StarlightLinkedListBucketEntry *rNext;
};

//--A component of the Iterator stack.
typedef struct SLLIteratorEntry SLLIteratorEntry;
struct SLLIteratorEntry
{
    int mSlot;
    StarlightLinkedListEntry *rCurrentEntry;
    SLLIteratorEntry *rNextStackObject;
};

//--The SLL, when in "Locked" mode, is composed of these entries. A locked list cannot be added
//  to or removed from, but it much faster since it's of fixed size.
typedef struct
{
    char *mName;
    void *rData;
    void (*rDeletionFunc)(void *);
}SLLEntryLocked;

class StarlightLinkedList
{
    private:
    //--System
    bool mIsLocked;

    //--List Handling
    StarlightLinkedListEntry *mListHead;
    StarlightLinkedListEntry *mListTail;

    //--Random Pointer Handling
    int mLockedRandomPointer;
    StarlightLinkedListEntry *mListCurrent;

    //--List Properties
    int mListSize;
    bool mHandlesDeallocation;
    StringCompareFuncPtr rStringCompareFuncPtr;

    //--Iterator Stack
    int mStackSize;
    SLLIteratorEntry *mIteratorStack;

    //--Locking
    SLLEntryLocked *mLockedList;

    //--Buckets
    int mBucketEntries;
    bool mIsBucketingActive;
    StarlightLinkedListBucketEntry *mBucketHead;
    StarlightLinkedListBucketEntry *mBucketTail;

    //--Debug
    bool mDebugFlag;

    public:
    //--System
    StarlightLinkedList(bool pHandleDeallocate);
    ~StarlightLinkedList();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    int GetListSize();
    void *GetHead();
    void *GetTail();

    //--Manipulators
    void SetDebugFlag(bool pFlag);
    void SetDeallocation(bool pFlag);
    void SetCaseSensitivity(bool pFlag);

    //--Sorting
    static int CompareEntries(const void *pEntryA, const void *pEntryB);
    static int CompareEntriesRev(const void *pEntryA, const void *pEntryB);
    void SortList(bool pReverseFlag);
    void SortListUsing(SortFnPtr pSortFunction);

    //--Core Methods
    void *DeleteHead();
    void *DeleteTail();
    void ClearList();
    void *DeleteEntry(StarlightLinkedListEntry *pEntry);

    //--Private Core Methods
    void *DeleteWholeList();
    void DecrementList();

    //--Additions
    static StarlightLinkedListEntry *AllocateEntry(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElement(const char *pName, void *pElement);
    void *AddElement(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementAsHead(const char *pName, void *pElement);
    void *AddElementAsHead(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementAsTail(const char *pName, void *pElement);
    void *AddElementAsTail(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);

    //--Buckets
    bool IsBucketing();
    int GetBucketedEntries();
    void SetBucketFlag(bool pFlag);
    void AddElementBucket(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void MergeBucket();
    void DumpBucket();

    //--Iterator Handling
    void *PushIterator();
    void *Iterate();
    void *AutoIterate();
    char *GetIteratorName();
    void PopIterator();
    void ClearStack();

    //--Locking
    bool IsListLocked();
    void LockList();
    void UnlockList();
    void DeallocateLockData();

    //--Random Pointer Handling
    void SetRandomPointerToHead();
    void SetRandomPointerToTail();
    bool IncrementRandomPointer();
    bool DecrementRandomPointer();
    void *GetRandomPointerEntry();
    char *GetRandomPointerName();
    void *RemoveRandomPointerEntry();
    void *LiberateRandomPointerEntry();
    void *SetToHeadAndReturn();
    void *SetToTailAndReturn();
    void *IncrementAndGetRandomPointerEntry();
    void *DecrementAndGetRandomPointerEntry();
    bool SetRandomPointerToThis(void *pPtr);
    StarlightLinkedListEntry *GetRandomPointerWholeEntry();

    //--Removal
    void *RemoveElementS(const char *pSearch);
    void *RemoveElementI(int slot);
    bool RemoveElementP(void *pPtr);

    //--Searches (StarlightLinkedListSearches.cc)
    const char *GetNameOfElementBySlot(int pSlot);
    int GetSlotOfElementByName(const char *pName);
    int GetSlotOfElementByPtr(void *pPtr);
    void *GetElementByName(const char *pSearch);
    void *GetElementByPartialName(const char *pSearch);
    void *GetElementBySlot(int slot);
    bool IsElementOnList(void *pPtr);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};
