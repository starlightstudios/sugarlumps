//--Base
#include "StarlightLinkedList.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"

//=========================================== System ==============================================
StarlightLinkedList::StarlightLinkedList(bool pHandleDeallocate)
{
    //--[StarlightLinkedList]
    //--System
    mIsLocked = false;

    //--List Handling
    mListHead = NULL;
    mListTail = NULL;
    mListCurrent = NULL;

    //--List Properties
    mListSize = 0;
    mHandlesDeallocation = pHandleDeallocate;
    rStringCompareFuncPtr = &strcmp;

    //--Iterator Stack
    mStackSize = 0;
    mIteratorStack = NULL;

    //--Locking
    mLockedList = NULL;

    //--Buckets
    mBucketEntries = 0;
    mIsBucketingActive = false;
    mBucketHead = NULL;
    mBucketTail = NULL;

    //--Debug
    mDebugFlag = false;
}
StarlightLinkedList::~StarlightLinkedList()
{
    //--Standard.
    ClearList();

    //--Locked stuff.
    DeallocateLockData();
}
void StarlightLinkedList::DeleteThis(void *pPtr)
{
    delete ((StarlightLinkedList *)pPtr);
}

//====================================== Property Queries =========================================
int StarlightLinkedList::GetListSize()
{
    return mListSize;
}
void *StarlightLinkedList::GetHead()
{
    //--Unlocked case: Return the head's data, or NULL if list size was 0.
    if(!mIsLocked)
    {
        if(!mListHead) return NULL;
        return mListHead->rData;
    }
    //--Locked case: Returns 0th element. List cannot be locked if there are 0 elements, so
    //  there is no need to range-check.
    else
    {
        return mLockedList[0].rData;
    }
}
void *StarlightLinkedList::GetTail()
{
    //--Unlocked case: Return the tail's data, or NULL if list size was 0.
    if(!mIsLocked)
    {
        if(!mListTail) return NULL;
        return mListTail->rData;
    }
    //--Locked case: Returns the last element. No need to range check.
    else
    {
        return mLockedList[mListSize-1].rData;
    }
}

//========================================= Manipulators ==========================================
void StarlightLinkedList::SetDebugFlag(bool pFlag)
{
    mDebugFlag = pFlag;
}
void StarlightLinkedList::SetDeallocation(bool pFlag)
{
    mHandlesDeallocation = pFlag;
}
void StarlightLinkedList::SetCaseSensitivity(bool pFlag)
{
    if(pFlag)
    {
        rStringCompareFuncPtr = &strcmp;
    }
    else
    {
        rStringCompareFuncPtr = &strcasecmp;
    }
}

//=========================================== Sorting =============================================
int StarlightLinkedList::CompareEntries(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() below.
    StarlightLinkedListEntry **rEntryA = (StarlightLinkedListEntry **)pEntryA;
    StarlightLinkedListEntry **rEntryB = (StarlightLinkedListEntry **)pEntryB;

    return strcmp((*rEntryA)->mName, (*rEntryB)->mName);
}
int StarlightLinkedList::CompareEntriesRev(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() below. This one is Z to A.
    StarlightLinkedListEntry **rEntryA = (StarlightLinkedListEntry **)pEntryA;
    StarlightLinkedListEntry **rEntryB = (StarlightLinkedListEntry **)pEntryB;

    return (strcmp((*rEntryA)->mName, (*rEntryB)->mName) * -1);
}
void StarlightLinkedList::SortList(bool pReverseFlag)
{
    //--Sorts the list using the standard CompareEntries function, or CompareEntriesRev if the flag
    //  is set. This works regardless of the subtype since the entries are sorted, not their data.
    //--Lists cannot be sorted when locked. Lock them after sorting, please!
    if(mIsLocked) return;

    //--Sort normal.
    if(!pReverseFlag)
    {
        SortListUsing(&CompareEntries);
    }
    //--Sort reversed.
    else
    {
        SortListUsing(&CompareEntriesRev);
    }
}
void StarlightLinkedList::SortListUsing(SortFnPtr pSortFunction)
{
    //--Sorts the linked list using the provided sorting function. It is up to the coder to make sure
    //  all entries on the list are compatible with the sorting function.
    //--Usage of the built-in CompareEntries function is guaranteed to work.
    //--As above, lists cannot be sorted when locked.
    if(mListSize < 2 || !pSortFunction || mIsLocked) return;

    //--Array of entries
    StarlightLinkedListEntry **tEntriesArray = (StarlightLinkedListEntry **)malloc(sizeof(StarlightLinkedListEntry *) * mListSize);

    //--Fill it with the list's data
    StarlightLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        tEntriesArray[i] = rEntry;
        rEntry = rEntry->rNext;
    }

    //--Sort it.
    qsort(tEntriesArray, mListSize, sizeof(StarlightLinkedListEntry *), pSortFunction);

    //--Dump it back into the RLL.
    mListHead = tEntriesArray[0];
    mListTail = tEntriesArray[mListSize-1];

    rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        if(i == 0)
        {
            rEntry->rPrev = NULL;
        }
        else
        {
            rEntry->rPrev = tEntriesArray[i-1];
        }
        if(i == mListSize - 1)
        {
            rEntry->rNext = NULL;
        }
        else
        {
            rEntry->rNext = tEntriesArray[i+1];
        }
        rEntry = rEntry->rNext;
    }
    free(tEntriesArray);
}

//========================================= Core Methods ==========================================
void *StarlightLinkedList::DeleteHead()
{
    //--Deletes the head of the list. Auto-detects special cases where the list is one-object large.
    //--Fails if the list is locked.
    if(mIsLocked) return NULL;
    if(mListHead == NULL) return NULL;
    if(mListTail == mListHead) return DeleteWholeList();

    //--Reposition the pointers.
    StarlightLinkedListEntry *tEntry = mListHead;
    mListHead = tEntry->rNext;
    if(mListHead) mListHead->rPrev = NULL;
    DecrementList();

    //--Done.
    return DeleteEntry(tEntry);
}
void *StarlightLinkedList::DeleteTail()
{
    //--As above, but for the tail. Fails if locked.
    if(mIsLocked) return NULL;
    if(mListTail == NULL) return NULL;
    if(mListTail == mListHead) return DeleteWholeList();

    //--Deletion, store the pointer blah blah.
    StarlightLinkedListEntry *tEntry = mListTail;
    mListTail = tEntry->rPrev;
    if(mListTail) mListTail->rNext = NULL;
    DecrementList();

    //--Done.
    return DeleteEntry(tEntry);
}
void StarlightLinkedList::ClearList()
{
    //--Removes everything on the list, one at a time. If mHandlesDeallocation is true, then it
    //  will delete the pointer attached using the provided function pointer. If not, it is
    //  untouched, and something else should handle deletion if appropriate.
    //--The list cannot be cleared if the list is locked.
    if(mIsLocked) return;
    StarlightLinkedListEntry *tEntry = mListHead;
    StarlightLinkedListEntry *nEntry;
    for(int i = 0; i < mListSize; i ++)
    {
        nEntry = tEntry->rNext;
        DeleteEntry(tEntry);
        tEntry = nEntry;
    }
    mListSize = 0;
    mListHead = NULL;
    mListTail = NULL;
}
void *StarlightLinkedList::DeleteEntry(StarlightLinkedListEntry *pEntry)
{
    //--Worker function, takes in an entry and deallocates it. The rData pointer is passed back,
    //  though the deletion function MAY have deleted it (or not, some functions may preserve data).
    //--Returns NULL on error.
    if(!pEntry) return NULL;

    //--Get Pointers
    void *rDataPtr = NULL;

    //--Linked List handles deallocation
    if(mHandlesDeallocation)
    {
        //--Entity has a function to delete it.
        if(pEntry->rDeletionFunc)
        {
            pEntry->rDeletionFunc(pEntry->rData);
        }
        //--Entity is a primitive, the deletion function was NULL. This is effectively a free() call.
        //  Generally, though, using FreeThis() is preferred if possible.
        else
        {
            #if defined SLL_BARK_ON_NO_DELETION_POINTER
                DebugManager::ForcePrint("StarlightLinkedList - Error, no deletion function for entry %s\n", pEntry->mName);
            #endif
            free(pEntry->rData);
        }
    }
    //--List returns a pointer to allow the program to deallocate.
    else
    {
        rDataPtr = pEntry->rData;
    }

    //--The rest.
    free(pEntry->mName);
    free(pEntry);
    return rDataPtr;
}

//===================================== Private Core Methods ======================================
void *StarlightLinkedList::DeleteWholeList()
{
    //--Called when the list is composed of a single member, is a special case of DeleteHead()
    //  and DeleteTail(). Is private for a reason!
    //--Does nothing if the list is locked. Shouldn't be called in any case, but you can't be too
    //  careful, can you?
    if(mIsLocked) return NULL;
    StarlightLinkedListEntry *tEntry = mListHead;

    //--Delete and store the data. If deallocation is on, the pointer may be unstable.
    void *rDataPtr = DeleteEntry(tEntry);

    //--Clear off the list.
    mListTail = NULL;
    mListHead = NULL;
    mListSize = 0;

    //--Return.
    return rDataPtr;
}
void StarlightLinkedList::DecrementList()
{
    //--Called whenever a member of the list is deleted. Is private since the list's size should
    //  not be interfered with externally, that may cause dangling pointers.
    //--Fails if the list is locked.
    if(mIsLocked) return;
    mListSize --;

    //--Detects the case of whole-list deletion.
    if(mListSize == 0)
    {
        mListHead = NULL;
        mListTail = NULL;
    }
}
