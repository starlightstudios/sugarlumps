//--Base
#include "StarlightLinkedList.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void StarlightLinkedList::SetRandomPointerToHead()
{
    //--Unlocked.
    if(!mIsLocked)
    {
        mListCurrent = mListHead;
    }
    //--Locked.
    else
    {
        mLockedRandomPointer = 0;
    }
}
void StarlightLinkedList::SetRandomPointerToTail()
{
    //--Unlocked.
    if(!mIsLocked)
    {
        mListCurrent = mListTail;
    }
    //--Locked.
    else
    {
        mLockedRandomPointer = mListSize - 1;
    }
}
bool StarlightLinkedList::IncrementRandomPointer()
{
    //--Unlocked.
    if(!mIsLocked)
    {
        if(!mListCurrent)
        {
            mListCurrent = mListHead;
        }
        else
        {
            mListCurrent = mListCurrent->rNext;
        }
    }
    //--Locked.
    else
    {
        mLockedRandomPointer ++;
    }
    return true;
}
bool StarlightLinkedList::DecrementRandomPointer()
{
    //--Unlocked.
    if(!mIsLocked)
    {
        if(!mListCurrent)
        {
            mListCurrent = mListTail;
        }
        else
        {
            mListCurrent = mListCurrent->rPrev;
        }
    }
    //--Locked.
    else
    {
        mLockedRandomPointer --;
    }

    return true;
}
void *StarlightLinkedList::GetRandomPointerEntry()
{
    //--Unlocked.
    if(!mIsLocked)
    {
        if(!mListCurrent) return NULL;
        return mListCurrent->rData;
    }
    //--Locked.
    else
    {
        if(mLockedRandomPointer < 0 || mLockedRandomPointer >= mListSize) return NULL;
        return mLockedList[mLockedRandomPointer].rData;
    }
}
char *StarlightLinkedList::GetRandomPointerName()
{
    //--Unlocked.
    if(!mIsLocked)
    {
        if(!mListCurrent) return NULL;
        return mListCurrent->mName;
    }
    //--Locked.
    else
    {
        if(mLockedRandomPointer < 0 || mLockedRandomPointer >= mListSize) return NULL;
        return mLockedList[mLockedRandomPointer].mName;
    }
}
void *StarlightLinkedList::RemoveRandomPointerEntry()
{
    //--Removes the Entry currently pointed at by the RandomPointer.  If deletion is turned on, then
    //  the entry is summarily deleted.
    //--Fails if the list is locked, obviously.
    if(mIsLocked) return NULL;

    //--Special cases
    if(!mListCurrent) return NULL;
    void *rReturnPtr = NULL;

    //--Object is the head. See below for why this is NULL'd.
    if(mListCurrent == mListHead)
    {
        rReturnPtr = DeleteHead();
        mListCurrent = NULL;
    }
    //--Object is the tail.
    else if(mListCurrent == mListTail)
    {
        rReturnPtr = DeleteTail();
    }
    //--General case.
    else
    {
        //--Cross the pointers for the victim's neighbours.
        StarlightLinkedListEntry *rPrevEntry = mListCurrent->rPrev;
        StarlightLinkedListEntry *rNextEntry = mListCurrent->rNext;
        rPrevEntry->rNext = rNextEntry;
        rNextEntry->rPrev = rPrevEntry;

        //--Deletion.
        rReturnPtr = DeleteEntry(mListCurrent);
        DecrementList();

        //--Move the RLLPointer to the previous entry. Typically, a while() loop will iterate up
        //  at the end of the loop, so this will cause the next entry to remain unchanged.
        mListCurrent = rPrevEntry;
    }

    return rReturnPtr;
}
void *StarlightLinkedList::LiberateRandomPointerEntry()
{
    //--The same as removal, except the entry is never deleted regardless of the deallocation flag.
    //  This is done by simply flipping it off for the duration of one deletion call.
    //--Fails if the list is locked.
    if(mIsLocked) return NULL;

    bool tOldFlag = mHandlesDeallocation;
    mHandlesDeallocation = false;
    void *rEntry = RemoveRandomPointerEntry();
    mHandlesDeallocation = tOldFlag;
    return rEntry;
}
void *StarlightLinkedList::SetToHeadAndReturn()
{
    SetRandomPointerToHead();
    return GetRandomPointerEntry();
}
void *StarlightLinkedList::SetToTailAndReturn()
{
    SetRandomPointerToTail();
    return GetRandomPointerEntry();
}
void *StarlightLinkedList::IncrementAndGetRandomPointerEntry()
{
    IncrementRandomPointer();
    return GetRandomPointerEntry();
}
void *StarlightLinkedList::DecrementAndGetRandomPointerEntry()
{
    DecrementRandomPointer();
    return GetRandomPointerEntry();
}
bool StarlightLinkedList::SetRandomPointerToThis(void *pPtr)
{
    //--Given a specific data pointer, searches the list for that pointer and sets the RLLPointer
    //  to the first match. If the object exists multiple times on the list, the first one found
    //  will be the one the pointer sets itself to.
    //--Returns a boolean indicating if the object was found on the list. Note that, even if the
    //  object is not found, mListCurrent IS changed.
    if(!mIsLocked)
    {
        mListCurrent = mListHead;
        while(mListCurrent)
        {
            if(mListCurrent->rData == pPtr) return true;
            mListCurrent = mListCurrent->rNext;
        }
    }
    //--Locked version.
    else
    {
        for(mLockedRandomPointer = 0; mLockedRandomPointer < mListSize; mLockedRandomPointer ++)
        {
            if(mLockedList[mLockedRandomPointer].rData == pPtr) return true;
        }
    }

    //--Entry not found.
    return false;
}
StarlightLinkedListEntry *StarlightLinkedList::GetRandomPointerWholeEntry()
{
    //--Returns the whole entry, that is, mName, rNext, rPrev, and all that jazz can be accessed.
    //  Only use this if you know what you're doing!
    //--Fails if the list is locked, since a different structure is used.
    if(mIsLocked) return NULL;
    return mListCurrent;
}
