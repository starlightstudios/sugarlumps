//--Base
#include "StarlightLinkedList.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

StarlightLinkedListEntry *StarlightLinkedList::AllocateEntry(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Worker function, allocates and returns an RLLEntry with the matching data. Is a static public
    //  function, does not actually add anything to the list. If, for some strange reason, you want
    //  RLLEntries for something other than the list, feel free!
    //--Returns NULL on error.
    if(!pName || !pElement) return NULL;

    StarlightLinkedListEntry *nEntry = (StarlightLinkedListEntry *)malloc(sizeof(StarlightLinkedListEntry));
    nEntry->mName = (char *)malloc(sizeof(char) * (strlen(pName)+1));
    strcpy(nEntry->mName, pName);
    nEntry->rNext = NULL;
    nEntry->rPrev = NULL;
    nEntry->rData = pElement;
    nEntry->rDeletionFunc = pDeletionPtr;
    return nEntry;
}
void *StarlightLinkedList::AddElement(const char *pName, void *pElement)
{
    //--Adds the element to the end of the list. Note that this could change, it is not guaranteed
    //  that this invocation will always be a tail-add, it's an "I don't care" add. If your logic
    //  depends on a tail-add, use AddElementAsTail().
    //--Note: All the addition functions return the element that was being added (the data pointer,
    //  NOT the RLLEntry) in case you're using if(created) logic.
    //--Fails if the list is locked.
    if(mIsLocked) return pElement;
    return AddElementAsTail(pName, pElement);
}
void *StarlightLinkedList::AddElement(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Adds the element as the tail, and gives the DeletionFunction the passed pointer
    if(mIsLocked) return pElement;
    return AddElementAsTail(pName, pElement, pDeletionPtr);
}
void *StarlightLinkedList::AddElementAsHead(const char *pName, void *pElement)
{
    //--Cascading overload of below. Passes NULL for the rDeletionFunction.
    if(mIsLocked) return pElement;
    return AddElementAsHead(pName, pElement, NULL);
}
void *StarlightLinkedList::AddElementAsHead(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Adds the specified element to the head of the list. The DeletionFunctionPtr can be NULL,
    //  which is identical to free().
    //--If bucketing is active, pass to the bucket.
    if(mIsBucketingActive)
    {
        AddElementBucket(pName, pElement, pDeletionPtr);
        return pElement;
    }

    //--Returns NULL on error. Fails if the list was locked.
    if(mIsLocked) return pElement;

    //--Get the entry itself.
    StarlightLinkedListEntry *nEntry = AllocateEntry(pName, pElement, pDeletionPtr);
    if(!nEntry) return NULL;

    //--If the list was empty, this becomes the head and tail.
    if(mListSize == 0)
    {
        mListHead = nEntry;
        mListTail = nEntry;
    }
    //--If the list had exactly one element, this becomes the head, and the other becomes the tail.
    else if(mListSize == 1)
    {
        mListHead = nEntry;
        mListHead->rNext = mListTail;
        mListTail->rPrev = mListHead;
    }
    //--In the general case, this becomes the head and nothing else changes.
    else
    {
        mListHead->rPrev = nEntry;
        nEntry->rNext = mListHead;
        mListHead = nEntry;
    }

    //--Out of politeness, we return a pointer to the data in case you needed it.
    mListSize ++;
    return nEntry->rData;
}
void *StarlightLinkedList::AddElementAsTail(const char *pName, void *pElement)
{
    //--Same as adding to the head, except... to the tail. As usual, cascading overloads are provided.
    if(mIsLocked) return pElement;
    return AddElementAsTail(pName, pElement, NULL);
}
void *StarlightLinkedList::AddElementAsTail(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Adds the specified element to the tail of the list. The DeletionFunctionPtr can be NULL,
    //  which is identical to free().
    if(mIsBucketingActive)
    {
        AddElementBucket(pName, pElement, pDeletionPtr);
        return pElement;
    }

    //--Returns NULL on error.
    if(mIsLocked) return pElement;

    //--Allocate the entry.
    StarlightLinkedListEntry *nEntry = AllocateEntry(pName, pElement, pDeletionPtr);
    if(!nEntry) return NULL;

    //--If the list was empty, this becomes the head and tail.
    if(mListSize == 0)
    {
        mListHead = nEntry;
        mListTail = nEntry;
    }
    //--If the list had exactly one element, this becomes the tail, and the other becomes the head.
    else if(mListSize == 1)
    {
        mListTail = nEntry;
        mListHead->rNext = mListTail;
        mListTail->rPrev = mListHead;
    }
    //--In the general case, this becomes the tail and nothing else changes.
    else
    {
        mListTail->rNext = nEntry;
        nEntry->rPrev = mListTail;
        mListTail = nEntry;
    }

    //--Out of politeness, we return a pointer to the data in case you needed it.
    mListSize ++;
    return nEntry->rData;
}
