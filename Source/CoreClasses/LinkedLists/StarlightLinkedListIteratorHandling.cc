//--Base
#include "StarlightLinkedList.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void *StarlightLinkedList::PushIterator()
{
    //--Creates a new iterator for the list, and puts it on the top of the stack. Returns the DataPtr
    //  from the first entry on the list.
    //--Returns NULL if the list is empty. If the list is locked, the same basic idea runs but
    //  the iterator is handled differently.
    if(!mIsLocked)
    {
        //--Error check. List must have contents.
        if(mListHead == NULL) return NULL;

        //--Create and push the iterator.
        SLLIteratorEntry *nEntry = (SLLIteratorEntry *)malloc(sizeof(SLLIteratorEntry));
        nEntry->rCurrentEntry = mListHead;
        nEntry->rNextStackObject = mIteratorStack;
        nEntry->mSlot = 0;
        mIteratorStack = nEntry;

        mStackSize ++;
        return mListHead->rData;
    }
    //--Locked version. The iterator only needs to know which slot it's on.
    else
    {
        //--Create and push the iterator.
        SLLIteratorEntry *nEntry = (SLLIteratorEntry *)malloc(sizeof(SLLIteratorEntry));
        nEntry->mSlot = 0;
        mIteratorStack = nEntry;

        //--Return the 0th element. Easy!
        mStackSize ++;
        return mLockedList[0].rData;
    }
}
void *StarlightLinkedList::Iterate()
{
    //--First Iterator on the stack increments itself.  Returns the rData pointer to the entry
    //  it stops at. Fails if there's no iterators in place.
    if(!mIteratorStack) return NULL;

    //--Unlocked version.
    if(!mIsLocked)
    {
        //--Error check.
        if(!mIteratorStack->rCurrentEntry) return NULL;

        //--Move to the next entry. If off the edge of the list, return NULL.
        mIteratorStack->mSlot ++;
        mIteratorStack->rCurrentEntry = mIteratorStack->rCurrentEntry->rNext;
        if(!mIteratorStack->rCurrentEntry) return NULL;

        //--Pass back the data pointer.
        return mIteratorStack->rCurrentEntry->rData;
    }
    //--The locked version just returns by the next slot, or NULL if off the edge.
    else
    {
        mIteratorStack->mSlot ++;
        return mLockedList[mIteratorStack->mSlot].rData;
    }
}
void *StarlightLinkedList::AutoIterate()
{
    //--Same as above, except if the rCurrentEntry->rNext is NULL, automatically pops the iterator,
    //  saving a function call.
    if(!mIteratorStack || !mIteratorStack->rCurrentEntry) return NULL;

    //--Unlocked logic. Uses the RLL data.
    if(!mIsLocked)
    {
        //--Error check.
        if(!mIteratorStack->rCurrentEntry) return NULL;

        //--Next element.
        mIteratorStack->mSlot ++;
        mIteratorStack->rCurrentEntry = mIteratorStack->rCurrentEntry->rNext;

        //--If we went off the edge, pop the iterator.
        if(!mIteratorStack->rCurrentEntry)
        {
            PopIterator();
            return NULL;
        }

        //--Normal case: Return the data.
        return mIteratorStack->rCurrentEntry->rData;
    }
    //--The locked version just returns by the next slot, or NULL if off the edge.
    else
    {
        //--Next element.
        mIteratorStack->mSlot ++;

        //--Edge check.
        if(mIteratorStack->mSlot >= mListSize)
        {
            PopIterator();
            return NULL;
        }

        //--Normal case.
        return mLockedList[mIteratorStack->mSlot].rData;
    }
}
char *StarlightLinkedList::GetIteratorName()
{
    //--Returns the name of the Entry on the top of the Iterator stack.
    if(!mIteratorStack) return NULL;

    //--Unlocked version.
    if(!mIsLocked)
    {
        if(!mIteratorStack->rCurrentEntry) return NULL;
        return mIteratorStack->rCurrentEntry->mName;
    }
    //--Locked version.
    else
    {
        if(mIteratorStack->mSlot < 0 || mIteratorStack->mSlot >= mListSize) return NULL;
        return mLockedList[mIteratorStack->mSlot].mName;
    }
}
void StarlightLinkedList::PopIterator()
{
    //--Remove the current Iterator from the front of the stack. Works the same regardless of
    //  locked or unlocked cases.
    if(!mIteratorStack) return;

    SLLIteratorEntry *rOldHead = mIteratorStack;
    mIteratorStack = rOldHead->rNextStackObject;
    free(rOldHead);

    mStackSize --;
}
void StarlightLinkedList::ClearStack()
{
    //--Remove all the Iterators, and clear the stack. Works the same regardless of locking.
    SLLIteratorEntry *rCurEntry = mIteratorStack;
    SLLIteratorEntry *rNextEntry;
    while(rCurEntry)
    {
        rNextEntry = rCurEntry->rNextStackObject;
        free(rCurEntry);
        rCurEntry = rNextEntry;
    }

    mStackSize = 0;
    mIteratorStack = NULL;
}
