//--Base
#include "StarlightLinkedList.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Definitions.h"


//--Libraries
//--Managers

//--[Property Queries]
bool StarlightLinkedList::IsListLocked()
{
    return mIsLocked;
}

//--[Manipulators]
void StarlightLinkedList::LockList()
{
    //--Lock the list, copying the data over to a memory array. The original list is deallocated
    //  but the members are not. When locked, the list cannot be edited and is read-only. A locked
    //  list is parsed considerably faster than an unlocked one.
    //--The iterator stack will still be functional, as it tracks slots implicitly.
    //--The list cannot be locked with 0 elements, that'd be nonsensical.
    if(mIsLocked || mListSize < 1) return;
    mIsLocked = true;

    //--Allocate space.
    mLockedList = (SLLEntryLocked *)malloc(sizeof(SLLEntryLocked) * mListSize);

    //--Setup.
    StarlightLinkedListEntry *rEntry = mListHead;

    //--Place/Copy the data over.
    for(int i = 0; i < mListSize; i ++)
    {
        //--Copy the name.
        mLockedList[i].mName = InitializeString(rEntry->mName);
        mLockedList[i].rData = rEntry->rData;
        mLockedList[i].rDeletionFunc = rEntry->rDeletionFunc;

        //--Check for Random Pointer.
        if(rEntry == mListCurrent) mLockedRandomPointer = i;

        //--Next element.
        rEntry = rEntry->rNext;
    }

    //--Wipe the original list. Don't deallocate anything.
    int tOrigListSize = mListSize;
    bool tOldDealloc = mHandlesDeallocation;
    mHandlesDeallocation = false;
    while(mListSize > 0) DeleteHead();

    //--Clean up.
    mListSize = tOrigListSize;
    mHandlesDeallocation = tOldDealloc;

    //--Debug
    fprintf(stderr, "After lock, list contains %i elements.\n", mListSize);
}
void StarlightLinkedList::UnlockList()
{
    //--Unlock the list, returning the data to the linked format. Does nothing if the list wasn't locked.
    //--The Iterator Stack will cross-load the data over as necessary. It doesn't need special handling.
    if(!mIsLocked) return;
    mIsLocked = false;

    //--Setup.
    int tOrigListSize = mListSize;
    mListSize = 0;

    //--Add elements until we're out.
    for(int i = 0; i < tOrigListSize; i ++)
    {
        //--New element.
        AddElementAsTail(mLockedList[i].mName, mLockedList[i].rData, mLockedList[i].rDeletionFunc);

        //--Wipe the data.
        free(mLockedList[i].mName);
    }

    //--Free up the locked list entirely.
    free(mLockedList);
    mLockedList = NULL;

    //--Re-update the Random Pointer.
    mListCurrent = mListHead;
    for(int i = 0; i < mLockedRandomPointer; i ++)
    {
        mListCurrent = mListCurrent->rNext;
    }

    //--Re-update the iterator stack with the newly placed elements.
    SLLIteratorEntry *rIterator = mIteratorStack;
    while(rIterator)
    {
        //--Get the entry.
        rIterator->rCurrentEntry = mListHead;
        for(int i = 0; i < rIterator->mSlot; i ++)
        {
            rIterator->rCurrentEntry = rIterator->rCurrentEntry->rNext;
        }

        //--Next iterator.
        rIterator = rIterator->rNextStackObject;
    }

    //--Debug
    fprintf(stderr, "After unlock, list contains %i elements.\n", mListSize);
}

//--[Core Methods]
void StarlightLinkedList::DeallocateLockData()
{
    //--Removes all locked data. Useful for destructors. Does nothing if not locked.
    if(!mIsLocked) return;

    //--Wipe names/data.
    for(int i = 0; i < mListSize; i ++)
    {
        //--Wipe the name.
        free(mLockedList[i].mName);

        //--Wipe data if deallocation is on.
        if(mHandlesDeallocation && mLockedList[i].rDeletionFunc)
        {
            mLockedList[i].rDeletionFunc(mLockedList[i].rData);
        }
    }

    //--Free up the locked list entirely.
    free(mLockedList);
    mLockedList = NULL;
}
