//--Base
#include "StarlightLinkedList.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Definitions.h"

//--Libraries
//--Managers

bool StarlightLinkedList::IsBucketing()
{
    return mIsBucketingActive;
}
int StarlightLinkedList::GetBucketedEntries()
{
    return mBucketEntries;
}
void StarlightLinkedList::SetBucketFlag(bool pFlag)
{
    mIsBucketingActive = pFlag;
}
void StarlightLinkedList::AddElementBucket(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Similar to adding something normally, but does not give preference for sides. Instead,
    //  sets a flag based on the head/tail case. Right now, only tail entries occur.
    if(!pName || !pElement) return;

    //--Baseline.
    StarlightLinkedListBucketEntry *nEntry = (StarlightLinkedListBucketEntry *)malloc(sizeof(StarlightLinkedListBucketEntry));
    nEntry->mAppendsAtHead = false;
    nEntry->mName = InitializeString(pName);
    nEntry->rData = pElement;
    nEntry->rDeletionFunc = pDeletionPtr;
    nEntry->rNext = NULL;

    //--Set list position as start of the bucket list.
    if(!mBucketHead)
    {
        mBucketHead = nEntry;
        mBucketTail = nEntry;
    }
    //--Set list when there's 1 or more entries.
    else
    {
        mBucketTail->rNext = nEntry;
        mBucketTail = nEntry;
    }

    //--Counter.
    mBucketEntries ++;
}
void StarlightLinkedList::MergeBucket()
{
    //--Places all the entries in the bucket on the list, and wipes the bucket out in the process.
    mIsBucketingActive = false;

    //--Pass through.
    while(mBucketHead)
    {
        //--Store the next entry.
        StarlightLinkedListBucketEntry *rNextEntry = mBucketHead->rNext;

        //--Depending on the flag, add to the head or tail.
        if(mBucketHead->mAppendsAtHead)
        {
            AddElementAsHead(mBucketHead->mName, mBucketHead->rData, mBucketHead->rDeletionFunc);
        }
        //--Tail case.
        else
        {
            AddElementAsTail(mBucketHead->mName, mBucketHead->rData, mBucketHead->rDeletionFunc);
        }

        //--In all cases, wipe out the bucket entry.
        free(mBucketHead->mName);
        free(mBucketHead);
        mBucketHead = rNextEntry;
    }

    //--The bucket should now be wiped out. Reset its variables.
    mBucketEntries = 0;
    mBucketHead = NULL;
    mBucketTail = NULL;

    //--If this flag is set, bucketing gets turned off immediately after merging. If the flag is
    //  not set, bucketing will remain in place. The coder can, of course, handle this manually.
    #if defined SLL_MERGING_DEACTIVATES_BUCKETING
        mIsBucketingActive = false;
    #else
        mIsBucketingActive = true;
    #endif
}
void StarlightLinkedList::DumpBucket()
{
    //--Dumps all entries in the bucket without adding them to the list. The deletion state of the
    //  list is respected, as is the deletion function.
    while(mBucketHead)
    {
        //--Store the next entry.
        StarlightLinkedListBucketEntry *rNextEntry = mBucketHead->rNext;

        //--If the list's deletion flag is on, delete the member data if it has a function.
        if(mHandlesDeallocation && mBucketHead->rDeletionFunc)
        {
            mBucketHead->rDeletionFunc(mBucketHead->rData);
        }

        //--In all cases, wipe out the bucket entry.
        free(mBucketHead->mName);
        free(mBucketHead);
        mBucketHead = rNextEntry;
    }

    //--The bucket should now be wiped out. Reset its variables.
    mBucketEntries = 0;
    mBucketHead = NULL;
    mBucketTail = NULL;

    //--If this flag is set, bucketing gets turned off immediately after dumping. If the flag is
    //  not set, bucketing will remain in place. The coder can, of course, handle this manually.
    #if defined SLL_DUMPING_DEACTIVATES_BUCKETING
        mIsBucketingActive = false;
    #else
        mIsBucketingActive = true;
    #endif
}
