//--Base
#include "StarlightBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
#include "GLDfn.h"

//--Libraries
//--Managers

//--Functions dedicated to the uploading of data to the graphics card.  For the functions which
//  retrieve this data:
//  Hard drive:  StarlightBitmapImageProcessing.cc
//  SLF Files:   StarlightLumpManagerImages.cc

//--Static Image Loading Variables.  These variables can be manipulated by Lua or the program at any
//  time, they are public.
const int StarlightBitmap::cGLLookups[4] = {GL_COMPRESSED_RGB_S3TC_DXT1_EXT,  GL_COMPRESSED_RGBA_S3TC_DXT1_EXT,
                                        GL_COMPRESSED_RGBA_S3TC_DXT3_EXT, GL_COMPRESSED_RGBA_S3TC_DXT5_EXT};

void StarlightBitmap::SetPrecacheFlags(StarlightBitmapPrecache &sDataPack)
{
    //--Sets a precache to use the standard flags.
    memcpy(&sDataPack.mFlags, &xStaticFlags, sizeof(StarlightBitmapFlags));
}
uint32_t StarlightBitmap::UploadData(uint8_t *pArray, int pWidth, int pHeight)
{
    //--Overload to automatically generate a new texture with uncompressed images.
    return StarlightBitmap::UploadData(pArray, pWidth, pHeight, 0);
}
uint32_t StarlightBitmap::UploadData(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle)
{
    //--Uploads the data assuming it is not a compressed type, and is not monocolor.
    //--Sends a dummy size through, as size is not needed when using uncompressed data.
    return UploadBitmap(pArray, pWidth, pHeight, pForceHandle, cNoCompression, sizeof(uint32_t));
}
uint32_t StarlightBitmap::UploadCompressed(StarlightBitmapPrecache *pDataPack)
{
    //--Overload using a pack of data.  Neither the pack nor its data are deallocated here.
    if(!pDataPack) return 0;

    //--Set the flags according to the precache.  Store the old ones.
    StarlightBitmapFlags tOldFlags;
    memcpy(&tOldFlags, &xStaticFlags, sizeof(StarlightBitmapFlags));
    memcpy(&xStaticFlags, &pDataPack->mFlags, sizeof(StarlightBitmapFlags));

    //--Upload.
    uint32_t tHandle = UploadCompressed(pDataPack->mArray, pDataPack->mWidth, pDataPack->mHeight, pDataPack->mForceHandle, pDataPack->mCompressionType, pDataPack->mDataSize);

    //--Reset the flags.
    memcpy(&xStaticFlags, &tOldFlags, sizeof(StarlightBitmapFlags));

    //--Done.
    return tHandle;
}
uint32_t StarlightBitmap::UploadCompressed(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize)
{
    //--Synonym of UploadBitmap.
    return UploadBitmap(pArray, pWidth, pHeight, pForceHandle, pCompressionType, pDataSize);
}
uint32_t StarlightBitmap::UploadMonoColor(uint8_t *pArray, uint32_t pForceHandle)
{
    //--The bitmap's data represents a single pixel color, which should be applied across the
    //  entire area represented by the bitmap.  A texture of a single pixel is uploaded, which
    //  should then be stretched.
    return UploadBitmap(pArray, 1, 1, pForceHandle, cIsMonoColor, sizeof(uint32_t));
}
uint32_t StarlightBitmap::UploadBitmap(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize)
{
    //--Worker function, actually uploads the data using whatever state flags were set.  Needs to
    //  know all the details about the image in question, including array type and size.
    //--Returns 0 on error or if the image didn't need any work done.
    if(!pArray || pWidth < 1 || pHeight < 1 || pDataSize < 1) return 0;

    //--Multi-part case.
    if(pWidth > (int)xMaxTextureSize || pHeight > (int)xMaxTextureSize)
    {
        UploadBitmapMultiTexture(pArray, pWidth, pHeight, pCompressionType, pDataSize);
        return 0;
    }

    //--Clear the binding, just in case.
    glBindTexture(GL_TEXTURE_2D, 0);

    //--If the pForceHandle was 0, generate a new handle.  Otherwise, use the handle requested.
    uint32_t tTextureHandle = pForceHandle;
    if(!tTextureHandle) glGenTextures(1, &tTextureHandle);

    //--Flags.
    bool tIsCompressedType = false;

    //--[Check the compression type.]
    //--Error.
    if(pCompressionType == cInvalidCompression)
    {
        return 0;
    }
    //--R8G8B8A8 32-Bit unsigned.
    else if(pCompressionType == cNoCompression)
    {
    }
    //--PNG not supported yet.
    else if(pCompressionType == cPNGCompression)
    {
        return 0;
    }
    //--Pre-compiled compressed bitmap.
    else if(pCompressionType >= cDXT1Compression && pCompressionType <= cDXT5Compression)
    {
        tIsCompressedType = true;
    }
    //--Array contains a single R8G8B8A8 pixel.
    else if(pCompressionType == cIsMonoColor)
    {
        pWidth = 1;
        pHeight = 1;
    }
    //--Error.
    else
    {
        fprintf(stderr, "Warning: %i is not a valid compression flag!\n", pCompressionType);
        return 0;
    }

    //--Set the flags associated with this image.
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tTextureHandle);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelZoom(1.0f, -1.0f);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, xStaticFlags.mUplMinFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, xStaticFlags.mUplMagFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, xStaticFlags.mUplBaseLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, xStaticFlags.mUplMaxLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, xStaticFlags.mUplWrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, xStaticFlags.mUplWrapT);

    //--Issue the upload.
    if(!tIsCompressedType)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, pWidth, pHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pArray);
    }
    else
    {
        uint32_t tCompression = cGLLookups[pCompressionType - cDXT1Compression];
        sglCompressedTexImage2D(GL_TEXTURE_2D, 0, tCompression, pWidth, pHeight, 0, pDataSize, pArray);
    }

    //--Generate mipmaps if any of the flags requested it.
    if(xStaticFlags.mUplMinFilter == GL_LINEAR_MIPMAP_LINEAR  || xStaticFlags.mUplMinFilter == GL_LINEAR_MIPMAP_NEAREST ||
       xStaticFlags.mUplMinFilter == GL_NEAREST_MIPMAP_LINEAR || xStaticFlags.mUplMinFilter == GL_NEAREST_MIPMAP_NEAREST)
    {
        //sglGenerateMipmap(GL_TEXTURE_2D);
    }

    //--Issue a call to the LoadInterrupt.  Automatically fails if the interrupt was disabled.
    if(xInterruptCall && !xSuppressInterrupt) xInterruptCall();

    //--Return the handle if all went well.
    return tTextureHandle;
}
uint8_t *CopyRegion(uint8_t *pArray, int pWid, int pHei, int pX1, int pY1, int pX2, int pY2, int &sSizeX, int &sSizeY, int &sDataSize)
{
    //--Given a data array of size pWid and pHei, create a new array with a region copy and return it.
    if(!pArray || pWid < 1 || pHei < 1) return NULL;

    //--Range clamp.
    if(pX1 < 0) pX1 = 0;
    if(pY1 < 0) pY1 = 0;
    if(pX2 >= pWid) pX2 = pWid - 1;
    if(pY2 >= pHei) pY2 = pHei - 1;

    //--Calc space of new array.
    sSizeX = pX2 - pX1;
    sSizeY = pY2 - pY1;

    sDataSize = sizeof(uint8_t) * sSizeY * sSizeX * 4;
    uint8_t *nArray = (uint8_t *)malloc(sDataSize);
    fprintf(stderr, "Array values %i %i to %i %i\n", pX1, pY1, pX2, pY2);

    //--Begin copying.
    for(int x = pX1; x < pX2; x ++)
    {
        for(int y = pY1; y < pY2; y ++)
        {
            int tCurArray  = ((x-pX1) + ((y-pY1) * sSizeX)) * 4;
            int tOrigArray = (x + (y * pWid)) * 4;
            nArray[tCurArray+0] = pArray[tOrigArray+0];
            nArray[tCurArray+1] = pArray[tOrigArray+1];
            nArray[tCurArray+2] = pArray[tOrigArray+2];
            nArray[tCurArray+3] = pArray[tOrigArray+3];
        }
    }
    fprintf(stderr, " Done\n");

    //--Pass it back.
    return nArray;
}

void StarlightBitmap::UploadBitmapMultiTexture(uint8_t *pArray, int pWidth, int pHeight, int pCompressionType, int pDataSize)
{
    //--When the requested texture is too large for a single image to contain due to limitations of
    //  the graphics card, we can split it into multiple smaller textures and render them side by side.
    //  This function statically returns the data needed.
    //--This only works with uncompressed data right now.
    if(!pArray || pWidth < 1 || pHeight < 1 || pDataSize < 1 || !xMaxTextureSize) return;
    xMultiGLHandles = NULL;
    xThisIsMultiTexture = true;

    //--Figure out how many textures will be needed.
    xMultiTexX = pWidth / xMaxTextureSize;
    xMultiTexY = pHeight / xMaxTextureSize;
    xMultiTexX = 2;
    xMultiTexY = 1;
    if(xMultiTexX < 1 || xMultiTexY < 1)
    {
        xThisIsMultiTexture = false;
        return;
    }

    //--Allocate space.
    xMultiGLHandles = (uint32_t **)malloc(sizeof(uint32_t *) * xMultiTexX);
    for(int x = 0; x < xMultiTexX; x ++)
    {
        xMultiGLHandles[x] = (uint32_t *)malloc(sizeof(uint32_t) * xMultiTexY);
        for(int y = 0; y < xMultiTexY; y ++)
        {
            glGenTextures(1, &xMultiGLHandles[x][y]);
        }
    }

    //--Size storage.
    xMultiTexSizeX = pWidth;
    xMultiTexSizeY = pHeight;

    //--With all the needed textures generated, split the data array up and call the upload for each one.
    int tSizeX, tSizeY, tDataSize;
    for(int x = 0; x < xMultiTexX; x ++)
    {
        for(int y = 0; y < xMultiTexY; y ++)
        {
            //--Generate a new data array.
            uint8_t *tDataArray = CopyRegion(pArray, pWidth, pHeight, x*xMaxTextureSize, y*xMaxTextureSize, ((x+1)*xMaxTextureSize), ((y+1)*xMaxTextureSize), tSizeX, tSizeY, tDataSize);

            //--Upload it and get the data handle back.
            UploadBitmap(tDataArray, tSizeX, tSizeY, xMultiGLHandles[x][y], pCompressionType, tDataSize);

            //--Clean.
            free(tDataArray);
        }
    }
}
