//--Base
#include "StarlightBitmap.h"
#include "Definitions.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//--Note:  Doesn't actually require the StarlightBitmap, but since that's the class that uses the majority
//  of the color operations, it's stored there.  These are actually defined in Definitions.h.
/*
StarlightColor MapRGBI(uint8_t pRed, uint8_t pGreen, uint8_t pBlue)
{
    return MapRGBAI(pRed, pGreen, pBlue, 0xFF);
}
StarlightColor MapRGBAI(uint8_t pRed, uint8_t pGreen, uint8_t pBlue, uint8_t pAlpha)
{
    return MapRGBAF((float)pRed / 255.0f, (float)pGreen / 255.0f, (float)pBlue / 255.0f, (float)pAlpha / 255.0f);
}
StarlightColor MapRGBF(float pRed, float pGreen, float pBlue)
{
    return MapRGBAF(pRed, pGreen, pBlue, 1.0f);
}
StarlightColor MapRGBAF(float pRed, float pGreen, float pBlue, float pAlpha)
{
    StarlightColor nColor;
    nColor.r = pRed;
    nColor.g = pGreen;
    nColor.b = pBlue;
    nColor.a = pAlpha;
    return nColor;
}*/

//--Static functions to set the color mixer using a StarlightColor. Can be overloaded or modified.
void StarlightBitmap::SetMixer(StarlightColor pColor)
{
    glColor4f(pColor.r, pColor.g, pColor.b, pColor.a);
}
void StarlightBitmap::SetMixer(StarlightColor pColor, float pForceAlpha)
{
    glColor4f(pColor.r, pColor.g, pColor.b, pForceAlpha);
}
