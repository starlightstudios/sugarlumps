//--Base
#include "StarlightBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void StarlightBitmap::DrawRect(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor, float pThickness)
{
    //--Renders a rectangle at the provided location, in the provided color.
    glLineWidth(pThickness);
    glColor4f(pColor.r, pColor.g, pColor.b, pColor.a);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_LINE_LOOP);
        glVertex2f(pLft, pTop);
        glVertex2f(pRgt, pTop);
        glVertex2f(pRgt, pBot);
        glVertex2f(pLft, pBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor4i(255, 255, 255, 255);
}
void StarlightBitmap::DrawRectFill(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor)
{
    //--Renders a rectangle at the provided location in the provided color, but the rectangle is
    //  filled in.
    glColor4f(pColor.r, pColor.g, pColor.b, pColor.a);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(pLft, pTop);
        glVertex2f(pRgt, pTop);
        glVertex2f(pRgt, pBot);
        glVertex2f(pLft, pBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
}
void StarlightBitmap::DrawPolyLines(float *pArray, StarlightColor pColor, float pScale)
{
    //--Renders a polygon that is not filled, as a series of line segments. The 0th entry of the
    //  array is how many sides to expect.
    if(!pArray) return;

    int tSides = (int)pArray[0];
    glColor4f(pColor.r, pColor.g, pColor.b, pColor.a);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_POLYGON);
        for(int i = 0; i < tSides; i ++)
        {
            glVertex2f(pArray[(i*2)+1] * pScale, pArray[(i*2)+2] * pScale);
        }
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor4i(255, 255, 255, 255);
}
