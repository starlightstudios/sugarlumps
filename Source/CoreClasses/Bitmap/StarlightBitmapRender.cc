//--Base
#include "StarlightBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//--[Draw Series]
//--Using the 'Draw' commands handles most of the work for you. It binds, translates, and renders
//  the bitmap automatically.
void StarlightBitmap::Draw()
{
    Draw(0, 0, 0);
}
void StarlightBitmap::Draw(int pTargetX, int pTargetY)
{
    Draw(pTargetX, pTargetY, 0);
}
void StarlightBitmap::Draw(int pTargetX, int pTargetY, int pFlags)
{
    //--Multi-purpose drawing function. Binds the bitmap and renders it in 2 dimensions, automatically
    //  translating and flipping if necessary.

    //--Multi-texture case.
    if(mUsesMultipleTextures)
    {
        DrawMultiTexture(pTargetX, pTargetY, pFlags);
        return;
    }
    if(!mGLHandle) return;

    //--Texture coordinates.
    float tTxL = 0.0f;
    float tTxT = 0.0f;
    float tTxR = 1.0f;
    float tTxB = 1.0f;

    //--Horizontal flipping.
    int tUseFlipFlag = 0;
    if(pFlags & STARLIGHT_FLIP_HORIZONTAL)
    {
        pTargetX += (mTrueWidth - mWidth - mXOffset);
        tTxL = 1.0f;
        tTxR = 0.0f;
        tUseFlipFlag |= STARLIGHT_FLIP_HORIZONTAL;
    }
    else
    {
        pTargetX += mXOffset;
    }

    //--Vertical flipping.
    if(pFlags & STARLIGHT_FLIP_VERTICAL)
    {
        pTargetY += (mTrueHeight - mHeight - mYOffset);
        tTxT = 1.0f;
        tTxB = 0.0f;
        tUseFlipFlag |= STARLIGHT_FLIP_VERTICAL;
    }
    else
    {
        pTargetY += mYOffset;
    }

    //--Internal flipping. Flips the texture but not the positioning. This is caused by OpenGL's
    //  internal flipping of bitmaps to match the mathematical coordinate system.
    if(mIsInternallyFlipped)
    {
        float tTemp = tTxB;
        tTxB = tTxT;
        tTxT = tTemp;
    }

    //--Positioning.
    glTranslatef(pTargetX, pTargetY, 0.0f);
    float tLft = 0.0f;
    float tTop = 0.0f;
    float tRgt = mWidth;
    float tBot = mHeight;

    //--List handling. There is a GLList for each of the flipping configurations. If the list has
    //  not been generated yet, generate it and compile. Otherwise, simply call it.
    //--The tUseFlipFlag will correspond to the array position to use.
    //--A StarlightBitmap may not necessarily use all four configurations if it is never flipped.
    if(!mGLLists[tUseFlipFlag])
    {
        mGLLists[tUseFlipFlag] = glGenLists(1);
        glNewList(mGLLists[tUseFlipFlag], GL_COMPILE_AND_EXECUTE);

        //--Actual rendering.
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glBegin(GL_QUADS);
            glTexCoord2f(tTxL, tTxT); glVertex2f(tLft, tTop);
            glTexCoord2f(tTxR, tTxT); glVertex2f(tRgt, tTop);
            glTexCoord2f(tTxR, tTxB); glVertex2f(tRgt, tBot);
            glTexCoord2f(tTxL, tTxB); glVertex2f(tLft, tBot);
        glEnd();

        //--Clean up.
        glEndList();
    }
    else
    {
        glCallList(mGLLists[tUseFlipFlag]);
    }

    //--Clean.
    glTranslatef(-pTargetX, -pTargetY, 0.0f);
}
void StarlightBitmap::DrawMultiTexture(int pTargetX, int pTargetY, int pFlags)
{
    //--If the bitmap is broken into parts, renders each part instead of all them at once. Does not
    //  use glLists to boost render speed, and ignores flip instructions!
    glTranslatef(pTargetX, pTargetY, 0.0f);

    //--Setup.
    float tLft = 0.0f;
    float tTop = 0.0f;
    float tRgt = mWidth;
    float tBot = mHeight;

    for(int x = 0; x < mMultiTexX; x ++)
    {
        for(int y = 0; y < xMultiTexY; y ++)
        {
            //--Resolve
            tLft = x * xMaxTextureSize;
            tTop = y * xMaxTextureSize;
            tRgt = ((x+1) * xMaxTextureSize);
            tBot = ((y+1) * xMaxTextureSize);
            if(tRgt > mWidth) tRgt = mWidth;
            if(tBot > mHeight) tBot = mHeight;

            //--Actual rendering.
            glBindTexture(GL_TEXTURE_2D, mMultiGLHandles[x][y]);
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(tLft, tTop);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(tRgt, tTop);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(tRgt, tBot);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(tLft, tBot);
            glEnd();
        }
    }

    //--Clean.
    glTranslatef(-pTargetX, -pTargetY, 0.0f);
}

//--[Render Series]
//--This allows more 'hands-on' control over the rendering process. If performed in sequence, the
//  bitmap will be translated, rotated, rendered, and then the GL state is reset.
//--Note that, at no point, is the glhandle bound. Use a call of Bind() to do that.
void StarlightBitmap::TranslateOffsets()
{
    //--Translates down the StarlightBitmap's internal offsets.  Assumes nothing about the GLState.
    glTranslatef(mXOffset, mYOffset, 0.0f);
}
void StarlightBitmap::Rotate(float pAngle)
{
    //--Translate to the center and rotate.
    glTranslatef(mWidth /  2.0f, mHeight /  2.0f, 0.0f);
    glRotatef(pAngle, 0.0f, 0.0f, 1.0f);
    glTranslatef(mWidth / -2.0f, mHeight / -2.0f, 0.0f);
}
void StarlightBitmap::RenderAt()
{
    //--Renders a quad using this StarlightBitmap's internal sizes.
    glBegin(GL_QUADS);
        glTexCoord2f(0, 1); glVertex2f(     0,       0);
        glTexCoord2f(1, 1); glVertex2f(mWidth,       0);
        glTexCoord2f(1, 0); glVertex2f(mWidth, mHeight);
        glTexCoord2f(0, 0); glVertex2f(     0, mHeight);
    glEnd();
}
void StarlightBitmap::Unrotate(float pAngle)
{
    //--Undoes a Rotate() call.
    glTranslatef(mWidth /  2.0f, mHeight /  2.0f, 0.0f);
    glRotatef(pAngle, 0.0f, 0.0f, -1.0f);
    glTranslatef(mWidth / -2.0f, mHeight / -2.0f, 0.0f);
}
void StarlightBitmap::UntranslateOffsets()
{
    //--Undoes a Translate() call.
    glTranslatef(-mXOffset, -mYOffset, 0.0f);
}
