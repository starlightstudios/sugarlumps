//--Base
#include "StarlightBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void StarlightBitmap::AllocateHitboxes(int pNewTotal)
{
    //--Free old data.
    free(mHitboxes);
    mTotalHitboxes = 0;
    if(pNewTotal < 1) return;

    //--Allocate.  Do not clear.
    mTotalHitboxes = pNewTotal;
    mHitboxes = (SgHitbox *)malloc(sizeof(SgHitbox) * mTotalHitboxes);
}
void StarlightBitmap::SetHitbox(int pSlot, int pLft, int pTop, int pRgt, int pBot)
{
    //--Sets the specified hitbox the old fashioned way.
    if(pSlot < 0 || pSlot >= mTotalHitboxes) return;
    mHitboxes[pSlot].mLft = pLft;
    mHitboxes[pSlot].mTop = pTop;
    mHitboxes[pSlot].mRgt = pRgt;
    mHitboxes[pSlot].mBot = pBot;
}
void StarlightBitmap::SetHitbox(int pSlot, SgHitbox pNewHitbox)
{
    //--Overload that copies a structure.
    if(pSlot < 0 || pSlot >= mTotalHitboxes) return;
    mHitboxes[pSlot].mLft = pNewHitbox.mLft;
    mHitboxes[pSlot].mTop = pNewHitbox.mTop;
    mHitboxes[pSlot].mRgt = pNewHitbox.mRgt;
    mHitboxes[pSlot].mBot = pNewHitbox.mBot;
}
int StarlightBitmap::GetTotalHitboxes()
{
    return mTotalHitboxes;
}
SgHitbox StarlightBitmap::GetHitbox(int pSlot)
{
    //--Guards against an out-of-range case with a dummy hitbox.  Safe even if there were 0 hitboxes!
    SgHitbox tDummyBox;
    if(pSlot < 0 || pSlot >= mTotalHitboxes) return tDummyBox;
    return mHitboxes[pSlot];
}
