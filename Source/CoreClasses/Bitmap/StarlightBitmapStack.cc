//--Base
#include "StarlightBitmap.h"

//--Classes
//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

//--Stack controllers. Push, pop, all that stuff.
StarlightLinkedList *StarlightBitmap::xStaticFlagStack = new StarlightLinkedList(false);

//--[Functions]
void StarlightBitmap::PushBitmapFlags()
{
    //--Create and copy.
    StarlightBitmapFlags *nStoragePack = (StarlightBitmapFlags *)malloc(sizeof(StarlightBitmapFlags));
    memcpy(nStoragePack, &xStaticFlags, sizeof(StarlightBitmapFlags));

    //--Push the stack back one.
    xStaticFlagStack->AddElementAsHead("X", nStoragePack);

    //--Populate the stack head with the default flags.
    xStaticFlags.mUplMinFilter = GL_NEAREST;
    xStaticFlags.mUplMagFilter = GL_LINEAR;
    xStaticFlags.mUplBaseLevel = 0;
    xStaticFlags.mUplMaxLevel = 0;
    xStaticFlags.mUplWrapS = GL_CLAMP_TO_BORDER;
    xStaticFlags.mUplWrapT = GL_CLAMP_TO_BORDER;

    //--Debug.
    //fprintf(stderr, "Pushed stack. Top is now %i\n", xStaticFlags.mUplMinFilter);
}
void StarlightBitmap::PopBitmapFlags()
{
    //--If the stack has nothing on it, stop here.
    if(xStaticFlagStack->GetListSize() < 1)
    {
        //fprintf(stderr, "Failed to pop stack, no entries.\n");
        return;
    }

    //--Get the head. Remove it from the list.
    StarlightBitmapFlags *tStoragePack = (StarlightBitmapFlags *)xStaticFlagStack->RemoveElementI(0);
    memcpy(&xStaticFlags, tStoragePack, sizeof(StarlightBitmapFlags));

    //--Deallocate the storage pack now that we're done with it.
    //Memory::AddGarbage(tStoragePack, &FreeThis);
    free(tStoragePack);

    //--Debug.
    //fprintf(stderr, "Popped stack. Top is now %i\n", xStaticFlags.mUplMinFilter);
}
