//--Base
#include "StarlightBitmap.h"

//--Classes
//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

bool StarlightBitmap::xForcePowerOfTwo = false;
bool StarlightBitmap::gLockTarget = false;
bool StarlightBitmap::xSuppressInterrupt = false;
InterruptPtr StarlightBitmap::xInterruptCall = NULL;

//--Statistics
int32_t StarlightBitmap::xMaxTextureSize = 1024;

//--Multi-Texture
bool StarlightBitmap::xThisIsMultiTexture = false;
int StarlightBitmap::xMultiTexX = 0;
int StarlightBitmap::xMultiTexY = 0;
int StarlightBitmap::xMultiTexSizeX = 0;
int StarlightBitmap::xMultiTexSizeY = 0;
uint32_t **StarlightBitmap::xMultiGLHandles = NULL;

///========================================== System ==============================================
void StarlightBitmap::DefaultInit(uint32_t pGLHandle)
{
    //--System function, calls the common behavior between the constructors, becuase MinGW doesn't
    //  support C2011 or whatever yet.

    //--System
    mIsInternallyFlipped = false;
    mXOffset = 0;
    mYOffset = 0;
    mWidth = 0;
    mHeight = 0;
    mTrueWidth = 0;
    mTrueHeight = 0;
    mUseMipmapping = false;

    //--GL Stuff
    mOwnsHandle = true;
    mGLHandle = pGLHandle;
    for(int i = 0; i < STARLIGHTBITMAP_LISTS; i ++) mGLLists[i] = 0;

    //--Multi-part images for old systems.
    mUsesMultipleTextures = false;
    mMultiTexX = 0;
    mMultiTexY = 0;
    mMultiGLHandles = NULL;

    //--Cache
    mIsHoldingData = false;
    mDontDiscardDataOnUpload = false;
    mDataLength = 0;
    mCompressionType = cNoCompression;
    rLoadBuffer = NULL;

    //--If we already have a valid handle, get some data from it.
    if(mGLHandle)
    {
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &mWidth);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &mHeight);
        mTrueWidth = mWidth;
        mTrueHeight = mHeight;
    }

    //--Hitboxes
    mTotalHitboxes = 0;
    mHitboxes = NULL;
}
StarlightBitmap::StarlightBitmap()
{
    //--'Zero' constructor.  A blank StarlightBitmap which is populated with data later by a different
    //  function.
    DefaultInit(0);
}
StarlightBitmap::StarlightBitmap(uint32_t pGLHandle)
{
    //--Default constructor.  Accepts a GLHandle and wraps it.  Pass 0 to cause the "NULL" behavior.
    //  The Bitmap considers itself the texture's owner.
    DefaultInit(pGLHandle);
}
StarlightBitmap::StarlightBitmap(const char *pPath)
{
    //--Constructor which accepts a path from the hard drive, and loads the bitmap at that path.
    //  The texture is uploaded and the handle is found.
    //--Passing NULL will cause NULL behavior, no texture.
    uint32_t tHandle = StarlightBitmap::ProcessBitmap(pPath);
    DefaultInit(tHandle);

    //--If the handle came back as zero, then this might be a multi-texture.
    if(!tHandle && xThisIsMultiTexture)
    {
        //--Copy over the multi-texture data.
        fprintf(stderr, "Detected multi-texture in %i %s\n", tHandle, pPath);
        fprintf(stderr, " Sizes were %ix%i textures\n", xMultiTexX, xMultiTexY);
        mUsesMultipleTextures = true;
        mMultiTexX = xMultiTexX;
        mMultiTexY = xMultiTexY;
        mMultiGLHandles = xMultiGLHandles;

        //--Calc sizes.
        mWidth = xMultiTexSizeX;
        mHeight = xMultiTexSizeY;

        //--Null off old data.
        xThisIsMultiTexture = false;
        xMultiGLHandles = NULL;
    }
}
StarlightBitmap::StarlightBitmap(uint8_t *pData, int pWidth, int pHeight)
{
    //--Constructor which accepts data (typically from the EIM) and uploads it to get the texture
    //  handle.  Doesn't free the data!
    uint32_t tHandle = StarlightBitmap::UploadData(pData, pWidth, pHeight);
    DefaultInit(tHandle);
}
StarlightBitmap::~StarlightBitmap()
{
    //--Clear the texture, if you own it.
    if(mOwnsHandle && mGLHandle) glDeleteTextures(1, &mGLHandle);

    //--Clear multi-part handles.
    for(int x = 0; x < mMultiTexX; x ++)
    {
        for(int y = 0; y < mMultiTexY; y ++)
        {
            //glDeleteTextures(1, &mMultiGLHandles[x][y]);
        }
        free(mMultiGLHandles[x]);
    }
    free(mMultiGLHandles);

    //--Clear any lists.
    for(int i = 0; i < STARLIGHTBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
    }

    //--If there's anything in the load buffer, clear it.
    free(rLoadBuffer);
    rLoadBuffer = NULL;
    free(mHitboxes);
}
void StarlightBitmap::DeleteThis(void *pPtr)
{
    delete ((StarlightBitmap *)pPtr);
}
void StarlightBitmap::SetupFlags()
{
    //--Called at program startup, sets the default flags profile.
    StarlightBitmapFlags *nDefaultFlags = (StarlightBitmapFlags *)malloc(sizeof(StarlightBitmapFlags));
    nDefaultFlags->mUplMinFilter = GL_LINEAR;
    nDefaultFlags->mUplMagFilter = GL_NEAREST;
    nDefaultFlags->mUplBaseLevel = 0;
    nDefaultFlags->mUplMaxLevel = 0;
    nDefaultFlags->mUplWrapS = GL_CLAMP_TO_BORDER;
    nDefaultFlags->mUplWrapT = GL_CLAMP_TO_BORDER;

    //--Register.
    xFlagsProfiles->AddElement("Default", nDefaultFlags, &FreeThis);

    //--We also create a profile for the StarlightFont.
    StarlightBitmapFlags *nFontFlags = (StarlightBitmapFlags *)malloc(sizeof(StarlightBitmapFlags));
    nFontFlags->mUplMinFilter = GL_LINEAR;
    nFontFlags->mUplMagFilter = GL_LINEAR;
    nFontFlags->mUplBaseLevel = 0;
    nFontFlags->mUplMaxLevel = 0;
    nFontFlags->mUplWrapS = GL_CLAMP_TO_BORDER;
    nFontFlags->mUplWrapT = GL_CLAMP_TO_BORDER;
    xFlagsProfiles->AddElement("FontDefault", nFontFlags, &FreeThis);

    //--Set the name of the active profile.
    ResetString(xActiveProfileName, "Default");
}

///--[Public Statics]
char *StarlightBitmap::xActiveProfileName = NULL;
StarlightLinkedList *StarlightBitmap::xFlagsProfiles = new StarlightLinkedList(true);
StarlightBitmapFlags StarlightBitmap::xStaticFlags = {GL_NEAREST, GL_NEAREST, 0, 0, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER};

///===================================== Property Queries =========================================
int StarlightBitmap::GetWidth()
{
    return mWidth;
}
int StarlightBitmap::GetHeight()
{
    return mHeight;
}
int StarlightBitmap::GetTrueWidth()
{
    return mTrueWidth;
}
int StarlightBitmap::GetTrueHeight()
{
    return mTrueHeight;
}
int StarlightBitmap::GetXOffset()
{
    return mXOffset;
}
int StarlightBitmap::GetYOffset()
{
    return mYOffset;
}
uint32_t StarlightBitmap::GetGLTextureHandle()
{
    return mGLHandle;
}

///======================================== Manipulators ==========================================
void StarlightBitmap::Bind()
{
    glBindTexture(GL_TEXTURE_2D, mGLHandle);
}
void StarlightBitmap::SetInternalFlip(bool pFlag)
{
    mIsInternallyFlipped = pFlag;
}
void StarlightBitmap::SetSizes(int pXSize, int pYSize)
{
    mWidth = pXSize;
    mHeight = pYSize;
}
void StarlightBitmap::SetOffset(int pXOffset, int pYOffset)
{
    mXOffset = pXOffset;
    mYOffset = pYOffset;
}
void StarlightBitmap::SetTrueSizes(int pTrueWid, int pTrueHei)
{
    mTrueWidth = pTrueWid;
    mTrueHeight = pTrueHei;
}
void StarlightBitmap::SetMipMapping(bool pFlag)
{
    mUseMipmapping = pFlag;
}
void StarlightBitmap::AssignHandle(uint32_t pHandle, bool pTakeOwnership)
{
    //--Assigns the handle to the bitmap, and also assigns ownership, if necessary.  The old
    //  handle is deleted if it was owned.  Passing zero will deallocate the handle and set the
    //  bitmap to NULL mode.
    if(mOwnsHandle && mGLHandle)
    {
        glDeleteTextures(1, &mGLHandle);
        mGLHandle = 0;
        mOwnsHandle = false;
    }
    if(!pHandle) return;

    mGLHandle = pHandle;
    mOwnsHandle = pTakeOwnership;

    //--Retrieve data. Note that the 'true' values cannot be discerned from just the bitmap.
    glBindTexture(GL_TEXTURE_2D, mGLHandle);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &mWidth);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &mHeight);
    mTrueWidth = mWidth;
    mTrueHeight = mHeight;

    //--Clear any lists. Any lists that existed before are now invalid.
    for(int i = 0; i < STARLIGHTBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
        mGLLists[i] = 0;
    }
}

///======================================== Core Methods ==========================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
void StarlightBitmap::VerifyFlagProfile(StarlightBitmapFlags &sFlagProfile)
{
    //--Goes through the flag profiles and locks them to legal values. If an unknown value was used,
    //  the default value is used instead.

    //--The MagFilter can be GL_LINEAR or GL_NEAREST. If a mipmap value was used, then use the
    //  version without mipmapping.
    if(sFlagProfile.mUplMagFilter == GL_LINEAR_MIPMAP_LINEAR || sFlagProfile.mUplMagFilter == GL_LINEAR_MIPMAP_LINEAR)
    {
        sFlagProfile.mUplMagFilter = GL_LINEAR;
    }
    else if(sFlagProfile.mUplMagFilter == GL_NEAREST_MIPMAP_LINEAR || sFlagProfile.mUplMagFilter == GL_NEAREST_MIPMAP_LINEAR)
    {
        sFlagProfile.mUplMagFilter = GL_NEAREST;
    }
    //--Legal cases.
    else if(sFlagProfile.mUplMagFilter == GL_LINEAR || sFlagProfile.mUplMagFilter == GL_NEAREST)
    {

    }
    //--Illegal, unknown case.
    else
    {
        sFlagProfile.mUplMagFilter = GL_LINEAR;
    }

    //--MinFilter can use mipmapping. It just has to be a legal filter.
    if(sFlagProfile.mUplMinFilter == GL_NEAREST || sFlagProfile.mUplMinFilter == GL_LINEAR ||
       sFlagProfile.mUplMinFilter == GL_NEAREST_MIPMAP_LINEAR || sFlagProfile.mUplMinFilter == GL_NEAREST_MIPMAP_NEAREST ||
       sFlagProfile.mUplMinFilter == GL_LINEAR_MIPMAP_LINEAR || sFlagProfile.mUplMinFilter == GL_LINEAR_MIPMAP_NEAREST)
    {
    }
    //--Illegal, unknown case.
    else
    {
        sFlagProfile.mUplMinFilter = GL_NEAREST_MIPMAP_LINEAR;
    }
}
void StarlightBitmap::RegisterFlagProfile(const char *pProfileName)
{
    //--Registers a new profile using the current setup of flags. If the profile already exists, it
    //  is overwritten. The Default cannot be overwritten.
    if(!pProfileName || !strcasecmp(pProfileName, "Default") || !strcasecmp(pProfileName, "NULL")) return;

    //--Check if the profile already exists.
    StarlightBitmapFlags *rCheckFlags = (StarlightBitmapFlags *)xFlagsProfiles->GetElementByName(pProfileName);
    if(rCheckFlags)
    {
        memcpy(rCheckFlags, &xStaticFlags, sizeof(StarlightBitmapFlags));
        return;
    }

    //--Flag set doesn't exist. Create and save.
    StarlightBitmapFlags *nNewFlags = (StarlightBitmapFlags *)malloc(sizeof(StarlightBitmapFlags));
    memcpy(nNewFlags, &xStaticFlags, sizeof(StarlightBitmapFlags));
    xFlagsProfiles->AddElement(pProfileName, nNewFlags, &FreeThis);
}
void StarlightBitmap::ActivateFlagProfile(const char *pProfileName)
{
    //--Activates a set of flags. If they don't exist, or NULL is passed, resets to Default.
    //  Since passing NULL always returns NULL when checking by name, this is simplified.
    //--Because "NULL" can never be saved by RegisterFlagProfile(), it will always return NULL.
    StarlightBitmapFlags *rCheckFlags = (StarlightBitmapFlags *)xFlagsProfiles->GetElementByName(pProfileName);
    if(!rCheckFlags)
    {
        ActivateFlagProfile("Default");
        return;
    }

    //--The flags exist, set them.
    memcpy(&xStaticFlags, rCheckFlags, sizeof(StarlightBitmapFlags));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, xStaticFlags.mUplMinFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, xStaticFlags.mUplMagFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, xStaticFlags.mUplBaseLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, xStaticFlags.mUplMaxLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, xStaticFlags.mUplWrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, xStaticFlags.mUplWrapT);
}
char *StarlightBitmap::GetActiveFlagProfileName()
{
    //--Returns the name of the active profile. Do NOT free it!
    if(!xActiveProfileName) ResetString(xActiveProfileName, "Default");
    return xActiveProfileName;
}

///======================================== Lua Hooking ===========================================
void StarlightBitmap::HookToLuaState(lua_State *pLuaState)
{
    /* SB_SetTextureProperty("Push Stack")
       SB_SetTextureProperty("Pop Stack")
       SB_SetTextureProperty("MagFilter", iEnumeration)
       SB_SetTextureProperty("MinFilter", iEnumeration)
       SB_SetTextureProperty("Wrap S",    iEnumeration)
       SB_SetTextureProperty("Wrap T",    iEnumeration)
       SB_SetTextureProperty("Save Profile", sProfileName)
       SB_SetTextureProperty("Use Profile", sProfileName)
       Sets the property specified for all future texture uploads.  Maps ignore this setting.  Should
       be called during loading at startup, or during enemy loading, only.
       You can also specify pushing and popping of the property stack, if desired. */
    lua_register(pLuaState, "SB_SetTextureProperty", &Hook_SB_SetTextureProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_SB_SetTextureProperty(lua_State *L)
{
    //SB_SetTextureProperty("Push Stack")
    //SB_SetTextureProperty("Pop Stack")
    //SB_SetTextureProperty("MagFilter",    iEnumeration)
    //SB_SetTextureProperty("MinFilter",    iEnumeration)
    //SB_SetTextureProperty("Wrap S",       iEnumeration)
    //SB_SetTextureProperty("Wrap T",       iEnumeration)
    //SB_SetTextureProperty("Save Profile", sProfileName)
    //SB_SetTextureProperty("Use Profile",  sProfileName)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SB_SetTextureProperty");

    //--Switcher.
    const char *pPropertyName = lua_tostring(L, 1);
    if(!strcmp(pPropertyName, "Push Stack") && tArgs == 1)
    {
        StarlightBitmap::PushBitmapFlags();
    }
    else if(!strcmp(pPropertyName, "Pop Stack") && tArgs == 1)
    {
        StarlightBitmap::PopBitmapFlags();
    }
    else if(!strcmp(pPropertyName, "MagFilter") && tArgs == 2)
    {
        StarlightBitmap::xStaticFlags.mUplMagFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "MinFilter") && tArgs == 2)
    {
        StarlightBitmap::xStaticFlags.mUplMinFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Wrap S") && tArgs == 2)
    {
        StarlightBitmap::xStaticFlags.mUplWrapS = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Wrap T") && tArgs == 2)
    {
        StarlightBitmap::xStaticFlags.mUplWrapT = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Save Profile") && tArgs == 2)
    {
        StarlightBitmap::RegisterFlagProfile(lua_tostring(L, 2));
    }
    else if(!strcmp(pPropertyName, "Use Profile") && tArgs == 2)
    {
        StarlightBitmap::ActivateFlagProfile(lua_tostring(L, 2));
    }
    //--Error check.
    else
    {
        LuaPropertyError("SB_SetTextureProperty", pPropertyName, tArgs);
    }

    //--Verify the static flags. This keeps them in range.
    //StarlightBitmap::VerifyFlagProfile(StarlightBitmap::xStaticFlags);

    return 0;
}
