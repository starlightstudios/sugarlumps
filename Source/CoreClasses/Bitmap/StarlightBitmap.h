///====================================== StarlightBitmap =========================================
//--Originally a wrapper around Allegro's bitmaps, they have since shed almost all resemblence to
//  the fated API of old, and are now a wrapper around OpenGL behavior. Most of the standards put
//  in place by Allegro remain, though.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Definitions ========================================
#define FLIP_HORIZONTAL 1
#define FLIP_VERTICAL 2
#define FLIP_BOTH (FLIP_HORIZONTAL | FLIP_VERTICAL)
#define STARLIGHT_FLIP_HORIZONTAL 1
#define STARLIGHT_FLIP_VERTICAL 2
#define STARLIGHT_FLIP_BOTH (STARLIGHT_FLIP_HORIZONTAL | STARLIGHT_FLIP_VERTICAL)
typedef void(*InterruptPtr)();

#define STARLIGHTBITMAP_LISTS 4

///===================================== Local Structures =========================================
///--[SgHitbox]
//--Represents a 2D hitbox for an image.
typedef struct SgHitbox
{
    uint16_t mLft;
    uint16_t mTop;
    uint16_t mRgt;
    uint16_t mBot;
}SgHitbox;

///--[StarlightBitmapPrecache]
//--GL Flags needed for a bitmap.
typedef struct
{
    uint32_t mUplMinFilter;
    uint32_t mUplMagFilter;
    uint32_t mUplBaseLevel;
    uint32_t mUplMaxLevel;
    uint32_t mUplWrapS;
    uint32_t mUplWrapT;
}StarlightBitmapFlags;

///--[StarlightBitmapPrecache]
//--Stores the data necessary to make a bitmap after loading from the hard drive.
typedef struct StarlightBitmapPrecache
{
    uint8_t *mArray;
    int32_t mXOffset;
    int32_t mYOffset;
    int32_t mWidth;
    int32_t mHeight;
    int32_t mTrueWidth;
    int32_t mTrueHeight;
    uint32_t mForceHandle;
    uint8_t mCompressionType;
    uint32_t mDataSize;
    uint8_t mTotalHitboxes;
    SgHitbox *mHitboxes;
    StarlightBitmapFlags mFlags;
}StarlightBitmapPrecache;

///--[StarlightColor]
//--Color mapping for 32-bit colors.
typedef struct StarlightColor
{
    float r, g, b, a;
}StarlightColor;

///========================================== Classes =============================================
class StarlightBitmap
{
    //--[Constants]
    public:
    //--GL-compression codes.  These are mirrored in the StarlightLumps program.
    static const uint8_t cInvalidCompression = 0;
    static const uint8_t cNoCompression = 1;
    static const uint8_t cPNGCompression = 2;
    static const uint8_t cDXT1Compression = 3;
    static const uint8_t cDXT1CompressionWithAlpha = 4;
    static const uint8_t cDXT3Compression = 5;
    static const uint8_t cDXT5Compression = 6;
    static const uint8_t cIsMonoColor = 7;
    static const uint8_t cSupportedCompressions = 8;
    static const int cGLLookups[4];

    private:
    //--System
    bool mIsInternallyFlipped;
    int mXOffset, mYOffset;
    int mWidth, mHeight;
    int mTrueWidth, mTrueHeight;
    bool mUseMipmapping;

    //--GL Stuff.
    bool mOwnsHandle;
    uint32_t mGLHandle;
    uint32_t mGLLists[STARLIGHTBITMAP_LISTS];

    //--Multi-part images for old systems.
    bool mUsesMultipleTextures;
    int mMultiTexX, mMultiTexY;
    uint32_t **mMultiGLHandles;

    //--Cache
    bool mIsHoldingData;
    bool mDataNeedsUpload;
    bool mDontDiscardDataOnUpload;
    uint32_t mDataLength;
    uint32_t mCompressionType;
    uint8_t *rLoadBuffer;

    //--Uploading
    static StarlightLinkedList *mUploadingList;

    //--Hitboxes
    int mTotalHitboxes;
    SgHitbox *mHitboxes;

    //--Monocolor mode.
    bool mIsMonoColor;
    StarlightColor mMonoColor;

    protected:

    public:
    //--System
    void DefaultInit(uint32_t pGLHandle);
    StarlightBitmap();
    StarlightBitmap(uint32_t pGLHandle);
    StarlightBitmap(const char *pPath);
    StarlightBitmap(uint8_t *pData, int pWidth, int pHeight);
    ~StarlightBitmap();
    static void DeleteThis(void *pPtr);
    static void SetupFlags();

    //--Public Variables
    //--Static Variables (Defined in StarlightBitmapPaletteMacros.cc)
    static bool gLockTarget;
    static bool xSuppressInterrupt;
    static InterruptPtr xInterruptCall;

    //--Static multi-texture checkers.
    static bool xThisIsMultiTexture;
    static int xMultiTexX, xMultiTexY;
    static int xMultiTexSizeX, xMultiTexSizeY;
    static uint32_t **xMultiGLHandles;

    //--Static Image Loading Variables
    static char *xActiveProfileName;
    static StarlightLinkedList *xStaticFlagStack; //Declared in StarlightBitmapStack.cc
    static StarlightBitmapFlags xStaticFlags;
    static StarlightLinkedList *xFlagsProfiles;

    //--Static Engine options
    static bool xForcePowerOfTwo;

    //--GL System data.
    static int32_t xMaxTextureSize;

    //--Property Queries
    int GetWidth();
    int GetHeight();
    int GetTrueWidth();
    int GetTrueHeight();
    int GetXOffset();
    int GetYOffset();
    uint32_t GetGLTextureHandle();

    //--Manipulators
    void Bind();
    void SetInternalFlip(bool pFlag);
    void SetSizes(int pXSize, int pYSize);
    void SetOffset(int pXOffset, int pYOffset);
    void SetTrueSizes(int pTrueWid, int pTrueHei);
    void SetMipMapping(bool pFlag);
    void AssignHandle(uint32_t pHandle, bool pTakeOwnership);

    //--Core Methods
    void TranslateOffsets();
    void Rotate(float pAngle);
    void RenderAt();
    void Unrotate(float pAngle);
    void UntranslateOffsets();

    //--Color Macros
    static void SetMixer(StarlightColor pColor);
    static void SetMixer(StarlightColor pColor, float pForceAlpha);

    //--Hitbox Methods
    void AllocateHitboxes(int pNewTotal);
    void SetHitbox(int pSlot, int pLft, int pTop, int pRgt, int pBot);
    void SetHitbox(int pSlot, SgHitbox pNewHitbox);
    int GetTotalHitboxes();
    SgHitbox GetHitbox(int pSlot);

    //--Caching Methods
    void UploadCheck();
    void SetHoldCacheFlag(bool pFlag);
    void CacheData(uint8_t *pData, uint32_t pDataLength, uint32_t pCompressionFlag);

    //--Loader Methods
    static void SetPrecacheFlags(StarlightBitmapPrecache &sDataPack);
    static uint32_t ProcessBitmap(const char *pPath);
    static uint8_t *LoadBitmap(const char *pPath, int &sWidth, int &sHeight);
    #ifdef _ALLEGRO_PROJECT_
    static uint8_t *StripBitmap(ALLEGRO_BITMAP *pBitmap);
    #endif
    static uint32_t UploadData(uint8_t *pArray, int pWidth, int pHeight);
    static uint32_t UploadData(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle);
    static uint32_t UploadCompressed(StarlightBitmapPrecache *pDataPack);
    static uint32_t UploadCompressed(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize);
    static uint32_t UploadMonoColor(uint8_t *pArray, uint32_t pForceHandle);
    static uint32_t UploadBitmap(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize);
    static void UploadBitmapMultiTexture(uint8_t *pArray, int pWidth, int pHeight, int pCompressionType, int pDataSize);

    //--Shaders
    static void SetSwapColorsTotal(uint32_t pProgramHandle, int pTotal);
    static void SetSwapColor(uint32_t pProgramHandle, int pSlot, StarlightColor pSrcColor, StarlightColor pDstColor);

    //--Stack Methods
    static void PushBitmapFlags();
    static void PopBitmapFlags();

    //--Update
    //--File I/O
    //--Drawing
    void Draw(int pTargetX, int pTargetY, int pFlags);
    void Draw();
    void Draw(int pTargetX, int pTargetY);
    void DrawMultiTexture(int pTargetX, int pTargetY, int pFlags);

    //--Primitives
    static void DrawRect(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor, float pThickness);
    static void DrawRectFill(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor);
    static void DrawPolyLines(float *pArray, StarlightColor pColor, float pWidth);

    //--Pointer Routing
    //--Static Functions
    static void VerifyFlagProfile(StarlightBitmapFlags &sFlagProfile);
    static void RegisterFlagProfile(const char *pProfileName);
    static void ActivateFlagProfile(const char *pProfileName);
    static char *GetActiveFlagProfileName();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SB_SetTextureProperty(lua_State *L);
