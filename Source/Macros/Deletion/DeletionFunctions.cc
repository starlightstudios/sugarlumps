#include "DeletionFunctions.h"
#include "Program.h"

void FreeThis(void *pPtr)
{
    //--Function fits the format of a DeletionPtr, but free()'s the target.
    free(pPtr);
}
void DontDeleteThis(void *pPtr)
{
    //--Function fits the format for a DeletionPtr, but doesn't actually do any
    //  deletion.  Useful if a StarlightLinkedList has garbage cleanup active, but
    //  the pPtr is a reference.
}
void FreeDubChar(void *pPtr)
{
    //--Frees a char ** pointer.
    char **rPtr = (char **)pPtr;
    free(*rPtr);
}
void DubFreeDubChar(void *pPtr)
{
    //--Frees a char ** pointer, which was also allocated.
    char **rPtr = (char **)pPtr;
    free(*rPtr);
    free(rPtr);
}

void DeletePointer(void *pPtr, int pType)
{
}
DeletionFunctionPtr GetDeletionFunction(int pType)
{
    return NULL;
}
