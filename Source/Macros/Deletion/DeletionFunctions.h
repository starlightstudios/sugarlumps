//--Notes:  This file contains functions for deleting or freeing memory.  These
//  are mostly used by StarlightLinkedLists for garbage cleanup.

#pragma once

#include "Definitions.h"

void FreeThis(void *pPtr);
void DontDeleteThis(void *pPtr);
void FreeDubChar(void *pPtr);
void DubFreeDubChar(void *pPtr);

void DeletePointer(void *pPtr, int pType);
DeletionFunctionPtr GetDeletionFunction(int pType);
