//--Base
#include "Startup.h"

//--Classes
#include "AutoLoadLump.h"
#include "DataLump.h"
#include "ImageLump.h"
#include "SugarLumpFile.h"
#include "LuaTarLump.h"
#include "TimeStampInfo.h"

//--CoreClasses
#include "StarlightFileSystem.h"
#include "StarlightLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"
#include "GLManager.h"

void SetupGlobals()
{
    ///--[Documentation]
    //--This is effectively the Global's constructor. Except it doesn't have one normally because
    //  it's a singleton. Regardless, this is called literally at program execution time.
    GLOBAL *rGlobal = Global::Shared();

    //--[Variables]
    rGlobal->gQuit = false;
    rGlobal->gExecutionList = new StarlightLinkedList(true);

    //--[Statics]
    rGlobal->gFileSystemStack = new StarlightLinkedList(true);
    rGlobal->gActiveLumpFile = NULL;

    //--[Libraries]
    rGlobal->gDataLibrary = new DataLibrary();;

    //--[Managers]
    rGlobal->gLuaManager = NULL;
    rGlobal->gGLManager = NULL;
}
void HandleCommandLine(int pTotalArgs, char *pArgs[])
{
    ///--[Documentation]
    //--After the program has set its Globals to their default values, we parse the command line
    //  to change the defaults as necessary.
    GLOBAL *rGlobal = Global::Shared();

    //--Iterate.
    for(int i = 1; i < pTotalArgs; i ++)
    {
        //--Redirect console output into another file.
        if(!strcasecmp(pArgs[i], "-Redirect") && i + 1 < pTotalArgs)
        {
            freopen(pArgs[i+1], "w", stderr);
        }

        //--Adds a file to the execution list.
        if(!strcasecmp(pArgs[i], "-Call") && i + 1 < pTotalArgs)
        {
            char *nCallString = (char *)malloc(sizeof(char) * (strlen(pArgs[i+1]) + 1));
            strcpy(nCallString, pArgs[i+1]);
            rGlobal->gExecutionList->AddElement("X", nCallString, &FreeThis);
        }
    }
}
void Initialize_Everything()
{
    ///--[Documentation]
    //--Called at program startup, initializes Allegro, then bootstraps all the managers. Once
    //  completed, the program is ready to run.
    GLOBAL *rGlobal = Global::Shared();

    ///--[Allegro]
    if(!al_init())
    {
        fprintf(stderr, "Allegro failed to initialize\n");
        return;
    }

    //--Init the addons
    al_init_image_addon();

    ///--[Manager construction]
    //--Lua Manager comes first, as the others will hook against it.
    rGlobal->gLuaManager = new LuaManager();
    RegisterLuaFunctions(rGlobal->gLuaManager->GetLuaState());

    ///--[DataLibrary]
    rGlobal->gDataLibrary = new DataLibrary();

    ///--[Create the Display]
    //--This is needed for OpenGL texture compression. If not using one of those compression types
    //  it is acceptable to flag this not to run.
    rGlobal->gGLManager = new GLManager();
    rGlobal->gGLManager->CreateDisplay();
    rGlobal->gGLManager->WrangleExec();

    //--[Test]
    /*
    ALLEGRO_BITMAP *tBitmap;
    tBitmap = al_load_bitmap("TestRoom.png");
    fprintf(stderr, "Loaded bitmap - ");
    al_destroy_bitmap(tBitmap);
    fprintf(stderr, "Destroyed bitmap\n");*/
    return;
}

void Register_Events(ALLEGRO_EVENT_QUEUE *pQueue)
{
    ///--[Documentation]
    //--Register events with Allegro's event queue, allowing for buffered input.
    //  This program nominally has no input.
}
void RegisterLuaFunctions(lua_State *pLuaState)
{
    ///--[Documentation]
    //--All functions in the program should be declared statically if Lua is
    //  going to use them, and registered here.
    if(!pLuaState) return;

    //--[Classes]
    AutoLoadLump::HookToLuaState(pLuaState);
    DataLump::HookToLuaState(pLuaState);
    ImageLump::HookToLuaState(pLuaState);
    SugarLumpFile::HookToLuaState(pLuaState);
    LuaTarLump::HookToLuaState(pLuaState);
    TimeStampInfo::HookToLuaState(pLuaState);

    //--[CoreClasses]
    StarlightFileSystem::HookToLuaState(pLuaState);

    //--[Libraries]
    DataLibrary::HookToLuaState(pLuaState);

    //--[Managers]
    DebugManager::HookToLuaState(pLuaState);
}
