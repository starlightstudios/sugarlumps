//--Takes down the program.  All managers and objects should be destroyed here
//  and allow the program to exit.

#pragma once

void TakeDownProgram();
