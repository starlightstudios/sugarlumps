//--Commonly used string macros.

#pragma once

char *CopyString(const char *pString);
void RemoveWhitespace(char *pString);
char *GetQuoteString(const char *pString);

