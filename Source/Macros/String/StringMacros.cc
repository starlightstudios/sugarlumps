//--Base
#include "Prototype.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

char *CopyString(const char *pString)
{
    //--Returns the specified string as a dynamically allocated string.
    if(!pString) return NULL;

    char *nString = (char *)malloc(sizeof(char) * (strlen(pString) + 1));
    strcpy(nString, pString);
    return nString;
}
void RemoveWhitespace(char *pString)
{
    //--Modifies the provided string, removing all whitespace at the beginning.
    if(!pString) return;

    //--Setup
    int tWhiteLetters = 0;

    //--Scan.
    for(int i = 0; i < (int)strlen(pString); i ++)
    {
        if(pString[i] != ' ' && pString[i] != '\t') break;
        tWhiteLetters ++;
    }

    //--No whitespace, done.
    if(!tWhiteLetters) return;

    //--Shift letters over. This includes the null.
    for(int i = 0; i < (int)strlen(pString) - tWhiteLetters + 1; i ++)
    {
        pString[i] = pString[i+tWhiteLetters];
    }
}
char *GetQuoteString(const char *pString)
{
    //--Returns the string that was surrounded by quotes in the passed in string as a heap pointer.
    //  Ex: |hello" good fellow| is returned as |hello|.
    if(!pString) return NULL;

    int tLen = 0;
    for(int i = 0; i < (int)strlen(pString); i ++)
    {
        tLen ++;
        if(pString[i] == '\"') break;
    }
    if(tLen == 0) return NULL;

    char *nReturnString = (char *)malloc(sizeof(char) * (tLen + 1));
    strncpy(nReturnString, pString, sizeof(char) * tLen);
    nReturnString[tLen-1] = '\0';
    return nReturnString;
}
