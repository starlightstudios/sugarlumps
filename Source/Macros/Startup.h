//--Function delcarations relevant to the program's start sequence.

#pragma once

#include "Definitions.h"

void SetupGlobals();
void HandleCommandLine(int pTotalArgs, char *pArgs[]);
void Initialize_Everything();
void Register_Events(ALLEGRO_EVENT_QUEUE *pQueue);
void RegisterLuaFunctions(lua_State *pLuaState);
