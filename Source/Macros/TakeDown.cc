//--Base
#include "TakeDown.h"

//--Classes
#include "RootLump.h"
#include "SugarLumpFile.h"

//--CoreClasses
#include "StarlightFileSystem.h"
#include "StarlightLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"
#include "GLManager.h"

void TakeDownProgram()
{
    //--Frees and clears all program memory, in the opposite order it was created in.  Allegro goes
    //  last.
    GLOBAL *rGlobal = Global::Shared();

    //--Delete any static members of the Global that may still be active.
    delete rGlobal->gExecutionList;

    //--[Statics]
    delete rGlobal->gFileSystemStack;
    delete rGlobal->gActiveLumpFile;

    //--[Libraries]
    delete rGlobal->gDataLibrary;

    //--[Managers]
    delete rGlobal->gLuaManager;

    //--[Allegro]
    rGlobal->gGLManager->DestroyDisplay();
    al_uninstall_system();

    return;
}
