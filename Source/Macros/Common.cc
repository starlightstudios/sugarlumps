#include "Definitions.h"

void ResetString(char *&sString, const char *pNewString)
{
    //--Deallocates a string and sets it to the pNewString.  Passing NULL for the pNewString will
    //  simple deallocate and NULL the sString.
    //--MAKE SURE the sString was either NULL or a valid string before passing in to this, this call
    //  should not be used on raw allocated memory.
    if(sString == pNewString) return;

    free(sString);
    sString = NULL;
    if(!pNewString) return;

    sString = (char *)malloc(sizeof(char) * (strlen(pNewString) + 1));
    strcpy(sString, pNewString);
}

char *InitializeString(const char *pFormat, ...)
{
    //--Returns a newly allocated heap string with the provided format/arguments. Can never return
    //  null, as it will return a single null character string on error instead.
    //--The returned string must be freed as normal.
    if(!pFormat)
    {
        char *nString = (char *)malloc(sizeof(char) * 1);
        nString[0] = '\0';
        return nString;
    }

    //--Get the variable arg list.
    va_list tArgList;
    va_start (tArgList, pFormat);

    //--Print the args into a buffer.
    char tBuffer[256];
    vsprintf (tBuffer, pFormat, tArgList);

    //--Now that the length is known, ResetString can allocate it properly.
    char *nString = (char *)malloc(sizeof(char) * (strlen(tBuffer) + 1));
    strcpy(nString, tBuffer);

    //--Clean up.
    va_end (tArgList);
    return nString;
}
