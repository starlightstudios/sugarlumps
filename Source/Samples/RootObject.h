///===================================== RootObject ========================================
//--Base of the Starlight reflection tree. RootObjects contain a type which should be one of the
//  types set in Definitions.h. They also contain a UniqueID which is a static counter. No two objects
//  can logically have the same ID.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class RootObject
{
    private:

    protected:
    //--System
    int mType;
    int mUniqueID;

    public:
    //--System
    RootObject();
    virtual ~RootObject();

    //--Property Queries
    int GetType();
    int GetID();
    virtual void DummyFunc();
};

