#include "RootObject.h"
#include "Global.h"

///========================================== System ==============================================
RootObject::RootObject()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_FAIL;
}
RootObject::~RootObject()
{
}

///===================================== Property Queries =========================================
int RootObject::GetType()
{
    return mType;
}
int RootObject::GetID()
{
    return mUniqueID;
}
void RootObject::DummyFunc()
{

}
