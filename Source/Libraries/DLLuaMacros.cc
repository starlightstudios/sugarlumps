//--Base
#include "DLLuaMacros.h"

//--Classes
#include "SugarLumpFile.h"

//--CoreClasses
//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"

void HookMacrosToLuaState(lua_State *pLuaState)
{
    //--[Debug]
    /* DL_PrintSysVars(Path[])
       Prints all the SysVars (VM_ functions) to the specified file.*/
    lua_register(pLuaState, "DL_PrintSysVars", &Hook_DL_PrintSysVars);
}

//--Debug
int Hook_DL_PrintSysVars(lua_State *L)
{
    //--Macro, prints all sysvars to a file.
    //DL_PrintSysVars(Path[])
    if(lua_gettop(L) != 1) return fprintf(stderr, "DL_PrintSysVars:  Failed, invalid argument list\n");

    DataLibrary::Fetch()->WriteVMToTextfile(lua_tostring(L, 1));

    return 0;
}
