//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
#include "DeletionFunctions.h"

//--Generics
#include "StarlightLinkedList.h"

//--GUI
//--Libraries
//--Managers

//--These functions allow the DataLibrary to dynamically delete pointers at
//  certain intervals using human-readable list names.  The DataLibrary entry
//  is deleted as well as its pointer, if a function was provided to handle
//  that.  Be careful when using this or it will leave dangling pointers!

void DataLibrary::CreateDeletionList(const char *pListName)
{
    //--Creates a new DeletionList, which is a RLL of DLPaths.
    if(!pListName) return;

    StarlightLinkedList *nList = new StarlightLinkedList(true);
    mDeletionMasterList->AddElement(pListName, nList, &StarlightLinkedList::DeleteThis);
}
void DataLibrary::AddToDeletionList(const char *pListName, const char *pDLPath, DeletionFunctionPtr pDeletionPtr)
{
    //--Creates a new DLPath entry and places it on the specified deletion list
    //  for later.  If the function pointer is NULL, then &DontDeleteThis is
    //  sent through by default, and the entry is expected as a placeholder.
    if(!pListName || !pDLPath) return;

    //--Locate the list.
    StarlightLinkedList *rDeletionList = (StarlightLinkedList *)mDeletionMasterList->GetElementByName(pListName);
    if(!rDeletionList) return;

    //--Allocate a new DeletionPack
    DeletionPack *nDelPack = (DeletionPack *)malloc(sizeof(DeletionPack));
    nDelPack->mDLPath = NULL;
    nDelPack->rFuncPtr = NULL;

    //--Place the DLPath on the package.  Do NOT check to make sure it exists!
    //  That is handled only when the entry goes for deletion.  If something
    //  else already deleted it, it'd be ignored safely.
    nDelPack->mDLPath = (char *)malloc(sizeof(char) * (strlen(pDLPath) + 1));
    strcpy(nDelPack->mDLPath, pDLPath);

    //--Set the deletion function.  If NULL, use DontDeleteThis.
    nDelPack->rFuncPtr = pDeletionPtr;
    if(!nDelPack->rFuncPtr) nDelPack->rFuncPtr = &DontDeleteThis;

    //--Register the pack onto the DeletionList.
    rDeletionList->AddElement("X", nDelPack, &DataLibrary::DeleteDelPack);
}
void DataLibrary::ClearDeletionList(const char *pListName)
{
    //--Deletes the specified list, and everything that was on it.  This is
    //  handled automatically by the RLL functions.
    if(!pListName) return;

    //--Remove the list.  It will be automatically deleted.
    mDeletionMasterList->RemoveElementS(pListName);
}
