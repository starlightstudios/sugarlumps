///--[DLLuaMacros.h]
//--These are implementation-specific Lua macros for the DataLibrary.  They are not considered to be
//  "Core" Starlight operations.

#pragma once

#include "Definitions.h"

//--Registration
void HookMacrosToLuaState(lua_State *pLuaState);

//--Debug
int Hook_DL_PrintSysVars(lua_State *L);

