///======================================== DataLibrary ===========================================
//--General purpose archiver of pointers, by name.  Everything is stored in StarlightLinkedLists as
//  void pointers, and the type is expected to be known by the caller.  If the object is known to
//  be a RootObject then the type can be resolved from there.
//--Supports up to four levels of indexing.  These are called, in order, Section Catalogue Heading
//  and then the pointer itself is in the Index.
//--Example:  Section/Catalogue/Heading/Pointer
//            Graphics/Ponies/Applejack/AJSmiling
//--Additional functionality is provided for storing variables within the library as Sysvars.  These
//  are Lua-ready variables which can be strings or floating-point.
//--The DL can also store pointers by activity in a stack.  Lua can push/pull to and from this stack
//  which allows for complex object manipulation in decentralized Lua scripts.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[SysVar]
//--Represents a generic variable, either string or numeric.
typedef struct
{
    float mNumeric;
    char *mAlpha;
    bool mIsSaved;
}SysVar;

///--[PointerPack]
//--Stores a pointer.
typedef struct
{
    uint32_t mIndex;
    void *rPointer;
}PointerPack;

///--[DeletionPack]
//--Package that stores a deletion function for a given entry to use.
typedef struct
{
    char *mDLPath;
    DeletionFunctionPtr rFuncPtr;
}DeletionPack;

///--[ResolvePack]
//--Package used to store where in the DataLibrary a given entry is.
typedef struct
{
    char mSection[80];
    char mCatalogue[80];
    char mHeading[80];
    char mName[80];

    StarlightLinkedList *rSection;
    StarlightLinkedList *rCatalogue;
    StarlightLinkedList *rHeading;
    void *rEntry;
}ResolvePack;

///===================================== Local Definitions ========================================
///--[Library Position Constants]
#define DL_ROOT -1
#define DL_SECTION 0
#define DL_CATALOGUE 1
#define DL_HEADING 2
#define DL_NAME 3

///========================================== Classes =============================================
class DataLibrary
{
    private:
    //--System
    bool mDebugMode;
    bool mErrorMode;

    //--Storage
    StarlightLinkedList *mSectionList;
    StarlightLinkedList *mActiveEntityStack;
    StarlightLinkedList *mPointerIndexList;
    StarlightLinkedList *mDeletionMasterList;

    //--Counters
    int mRunningIndexCounter;

    //--Dummies
    RootObject *mDummyPtr;

    protected:

    public:
    //--System
    DataLibrary();
    ~DataLibrary();
    static void DeleteSysVar(void *pPtr);
    static void DeleteDelPack(void *pPtr);

    //--Public Variables
    bool mFailSilently;
    void *rActiveObject;
    StarlightLinkedList *rActiveSection;
    StarlightLinkedList *rActiveCatalogue;
    StarlightLinkedList *rActiveHeading;
    char *mActiveSectionName;
    char *mActiveCatalogueName;
    char *mActiveHeadingName;
    int mErrorCode;
    static bool xBlockSpecialDeletion;

    //--Property Queries
    StarlightLinkedList *GetSection(const char *pResolveString);
    StarlightLinkedList *GetCatalogue(const char *pResolveString);
    StarlightLinkedList *GetHeading(const char *pResolveString);
    void *GetEntry(const char *pResolveString);
    bool DoesEntryExist(const char *pResolveString);
    bool IsActiveValid();
    bool IsActiveValid(int pTypeFlag);
    bool IsActiveValid(int pTypeStart, int pTypeEnd);

    //--Manipulators
    void setErrorFlag(bool pFlag);
    void SetErrorCode(int pCode);
    void AddPath(const char *pDLPath);
    void RegisterPointer(const char *pDLPath, void *pPointer);
    void RegisterPointer(const char *pDLPath, void *pPointer, DeletionFunctionPtr pDeletionFunction);
    void *RemovePointer(const char *pDLPath);

    void SetActiveObject(const char *pDLPath);
    void PushActiveEntity();
    void PushActiveEntity(void *pReplacer);
    void *PopActiveEntity();

    //--Core Methods
    private:
    void AddSection(const char *pName);
    void AddCatalogue(const char *pName);
    void AddHeading(const char *pName);
    public:

    //--Deletion Lists
    void CreateDeletionList(const char *pListName);
    void AddToDeletionList(const char *pListName, const char *pDLPath, DeletionFunctionPtr pDeletionPtr);
    void ClearDeletionList(const char *pListName);

    //--Pointer Indexing
    StarlightLinkedList *GetIndexList();
    uint32_t IndexObject(void *pObject);
    uint32_t IndexDLActiveObject();
    void *GetObjectByIndex(uint32_t pIndex);
    void ClearIndexes();

    //--Purging (DataLibraryPurging.cc)
    void *Purge(const char *pDLPath);
    void *Purge(const char *pDLPath, bool pBlockDeallocation);
    //--Savefiles (DataLibrarySavefile.cc)
    void WriteData(FILE *fOutfile);
    void WriteCatalogue(FILE *fOutfile, StarlightLinkedList *pSection, StarlightLinkedList *pCatalogue);
    void WriteHeading(FILE *fOutfile, StarlightLinkedList *pSection, StarlightLinkedList *pCatalogue, StarlightLinkedList *pHeading);
    void ReadData(FILE *fInfile, int pVersion);
    void ClearVMData();
    void WriteVMToTextfile(const char *pPath);

    //--Vars (All lua functions)
    //  Space

    //--Worker Functions
    void ErrorNotFound(const char *pCallerName, const char *pResolveString);
    ResolvePack *CreateResolvePack(const char *pResolveString);
    ResolvePack *CreateResolvePack(const char *pResolveString, bool pSkipLists);
    static void GetNames(const char *pResolveString, ResolvePack &sPack);
    void GetLists(ResolvePack &sResolvePack);
    static char *GetPathPart(const char *pResolveString, int pPartFlag);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static DataLibrary *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_DL_AddPath(lua_State *L);
int Hook_DL_SetActiveObject(lua_State *L);

int Hook_DL_Purge(lua_State *L);

int Hook_DL_PushActiveEntity(lua_State *L);
int Hook_DL_PopActiveEntity(lua_State *L);

int Hook_DL_ClearActiveEntity(lua_State *L);

//--Variable Manager Handling
int Hook_VM_Exists(lua_State *L);
int Hook_VM_SetVar(lua_State *L);
int Hook_VM_RemVar(lua_State *L);
int Hook_VM_GetVar(lua_State *L);
int Hook_VM_SetSaveFlag(lua_State *L);

//--Pointer Index Handling
int Hook_DL_IndexDLObject(lua_State *L);
int Hook_DL_PushIndexedObject(lua_State *L);
int Hook_DL_ClearPointerIndex(lua_State *L);

