//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
//--Generics
#include "StarlightLinkedList.h"

//--GUI
//--Libraries
//--Managers

//--Functions related to the temporary pointer index.  In Lua, we cannot
//  reliably work with pointers due to instability problems.  These functions
//  allow us to work with pointers SO LONG AS THEY ARE ALL CALLED WITHIN THE
//  SAME SCRIPT (or sequence thereof.)
//--Pointers held by these functions are inherently unstable.  The list itself
//  is also unstable.  Be sure to clear it after using it.

StarlightLinkedList *DataLibrary::GetIndexList()
{
    return mPointerIndexList;
}
uint32_t DataLibrary::IndexObject(void *pObject)
{
    //--Worker routine, takes the provided object and indexes it, returning
    //  the index it was stored under.  Returns 0 if the object was NULL.
    if(!pObject) return 0;

    //--Construct a wrapper.
    PointerPack *nPack = (PointerPack *)malloc(sizeof(PointerPack));
    nPack->mIndex = mRunningIndexCounter;
    nPack->rPointer = pObject;

    //--Increment the counter.
    mRunningIndexCounter ++;

    //--Register it.  Deallocation is on, auto-free.
    mPointerIndexList->AddElement("X", nPack);

    return nPack->mIndex;
}
uint32_t DataLibrary::IndexDLActiveObject()
{
    //--Takes the currently active object on the DataLibrary and indexes it.
    return IndexObject(rActiveObject);
}
void *DataLibrary::GetObjectByIndex(uint32_t pIndex)
{
    //--Returns the object at the provided index, or NULL if not found, NULL
    //  if 0.  Void pointer, hope you know what it is!
    if(!pIndex) return NULL;

    PointerPack *rPack = (PointerPack *)mPointerIndexList->PushIterator();
    while(rPack)
    {
        if(rPack->mIndex == pIndex)
        {
            mPointerIndexList->PopIterator();
            return rPack->rPointer;
        }
        rPack = (PointerPack *)mPointerIndexList->AutoIterate();
    }
    return NULL;
}
void DataLibrary::ClearIndexes()
{
    //--Cleans the list off and resets the counter.
    mPointerIndexList->ClearList();
    mRunningIndexCounter = 1;
}
