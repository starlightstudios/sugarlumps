//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
#include "DLLuaMacros.h"

//--Generics
//--GUI
//--Libraries
//--Managers

///======================================== Lua Hooking ===========================================
void DataLibrary::HookToLuaState(lua_State *pLuaState)
{
    /* DL_AddPath(sPath[])
       Creates the path specified.  If any members don't exist, they are created as well.
       Format:  Root/Section/Catalogue/Heading/ */
    lua_register(pLuaState, "DL_AddPath",         &Hook_DL_AddPath);

    /* DL_SetActiveObject(sPath[])
       Sets the specified object as the rActiveObject.  If the object is not found, sets NULL. */
    lua_register(pLuaState, "DL_SetActiveObject", &Hook_DL_SetActiveObject);

    /* DL_Purge(sPath[], bBlockDealloc)
       Deletes the specified DL divider. Pass true or false for BlockDealloc.  True will cause the
       divider to delete all its pieces, false will not.  Beware dangling pointers.
       Format:  Root/Section/Catalogue/Heading/
       Whatever the final piece (Heading, Catalogue, Section) is will be destroyed.
       The special path "ALL" will wipe the whole library. */
    lua_register(pLuaState, "DL_Purge", &Hook_DL_Purge);

    /* DL_PushActiveEntity()
       Pushes the rActiveEntity onto the top of the Activity Stack.  Replaces it with NULL. */
    lua_register(pLuaState, "DL_PushActiveEntity", &Hook_DL_PushActiveEntity);

    /* DL_PopActiveEntity()
       Pops off the top of the Activity Stack onto rActiveEntity. */
    lua_register(pLuaState, "DL_PopActiveEntity",  &Hook_DL_PopActiveEntity);

    /* DL_ClearActiveEntity()
       Sets the ActiveEntity to NULL */
    lua_register(pLuaState, "DL_ClearActiveEntity", &Hook_DL_ClearActiveEntity);

    //--Variable Manager Handling
    lua_register(pLuaState, "VM_Exists",      &Hook_VM_Exists);
    lua_register(pLuaState, "VM_SetVar",      &Hook_VM_SetVar);
    lua_register(pLuaState, "VM_RemVar",      &Hook_VM_RemVar);
    lua_register(pLuaState, "VM_GetVar",      &Hook_VM_GetVar);
    lua_register(pLuaState, "VM_SetSaveFlag", &Hook_VM_SetSaveFlag);

    //--Pointer Index Handling
    /* DL_IndexDLObject()
       Takes the currently active object on the DataLibrary, and indexes it,
       returning the code it was stored under. */
    lua_register(pLuaState, "DL_IndexDLObject", &Hook_DL_IndexDLObject);

    /* DL_PushIndexedObject(Index)
       DL_PushIndexedObject(Index, IsEMObject, IsAIObject)
       Pushes an object onto the matching activity stack.  If both are false,
       the DataLibrary's stack is used.  Consult specific function documentation
       to see which stack should be used.
       Obviously, only values returned by an indexer should be used.
       Zero will always push NULL.  The value is implicitly unsigned.
       If both EM and AI are marked as true, the object is only pushed to the EM. */
    lua_register(pLuaState, "DL_PushIndexedObject", &Hook_DL_PushIndexedObject);

    /* DL_ClearPointerIndex()
       Once you are done with the pointer index, you should clear it.  The next
       tick the game runs could delete object on the list and cause crashing
       if their pointers are accessed. */
    lua_register(pLuaState, "DL_ClearPointerIndex", &Hook_DL_ClearPointerIndex);

    //--Macros
    HookMacrosToLuaState(pLuaState);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_DL_AddPath(lua_State *L)
{
    //DL_AddPath(sDLPath[])
    if(lua_gettop(L) != 1) return fprintf(stderr, "DL_AddPath:  Failed, invalid argument list\n");

    //--Setup
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->AddPath(lua_tostring(L, 1));
    return 0;
}
int Hook_DL_SetActiveObject(lua_State *L)
{
    //DL_SetActiveObject(sDLPath[])
    if(lua_gettop(L) != 1) return fprintf(stderr, "DL_SetActiveObject:  Failed, invalid argument list\n");

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->SetActiveObject(lua_tostring(L, 1));

    return 0;
}

//--Deletion
int Hook_DL_Purge(lua_State *L)
{
    //DL_Purge(DLPath[], BlockDealloc)
    if(lua_gettop(L) != 2) return fprintf(stderr, "DL_Purge:  Failed, invalid argument list\n");

    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    bool tOldSuppress = rDataLibrary->mFailSilently;
    rDataLibrary->mFailSilently = true;
    rDataLibrary->Purge(lua_tostring(L, 1), lua_toboolean(L, 2));
    rDataLibrary->mFailSilently = tOldSuppress;

    return 0;
}

//--Entity Stack controllers
int Hook_DL_PushActiveEntity(lua_State *L)
{
    //DL_PushActiveEntity()
    //No Arguments

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->PushActiveEntity();
    return 0;
}
int Hook_DL_PopActiveEntity(lua_State *L)
{
    //DL_PopActiveEntity()
    //No Arguments

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->PopActiveEntity();
    return 0;
}
int Hook_DL_ClearActiveEntity(lua_State *L)
{
    //DL_ClearActiveEntity()
    //No arguments

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->rActiveObject = NULL;
    return 0;
}

//--Pointer Index Handling
int Hook_DL_IndexDLObject(lua_State *L)
{
    //DL_IndexDLObject()
    uint32_t tIndex = DataLibrary::Fetch()->IndexDLActiveObject();
    lua_pushinteger(L, tIndex);
    return 1;
}
int Hook_DL_PushIndexedObject(lua_State *L)
{
    //DL_PushIndexedObject(Index)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return fprintf(stderr, "DL_PushIndexedObject:  Failed, invalid argument list\n");

    uint32_t tIndex = lua_tointeger(L, 1);
    if(tArgs == 1)
    {
        DataLibrary::Fetch()->rActiveObject = DataLibrary::Fetch()->GetObjectByIndex(tIndex);
    }
    return 0;
}
int Hook_DL_ClearPointerIndex(lua_State *L)
{
    //DL_ClearPointerIndex()
    DataLibrary::Fetch()->ClearIndexes();
    return 0;
}
