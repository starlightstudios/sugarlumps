//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

//--Variable Manager Handling.  These are a series of Lua functions that are
//  specially designed to operate on script variables.  The variables are of
//  type SysVar, defined in DataLibrary.h.  By default, they are all flagged to
//  be saved by the SaveManager, though you can switch this off.

//--The SaveManager will make a copy of the DataLibrary's Root/Vars/ section
//  and reinit it on game load.  Any other variables stored on any other list
//  will be ignored!

//--Note:  VM_SetVar will create a var if it doesn't exist.  If the existence
//  of a variable matters, you can check if it exists with VM_Exists.

int Hook_VM_Exists(lua_State *L)
{
    //--Returns true if the entry exists.  This doesn't check for a specific
    //  SysVar, it actually just returns true if the DL entry exists at all.
    //VM_Exists(DLPath[])
    if(lua_gettop(L) != 1)
    {
        fprintf(stderr, "VM_Exists:  Failed, invalid argument list\n");
        lua_pushboolean(L, false);
        return 1;
    }

    //--Check
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    void *rCheckPtr = rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    lua_pushboolean(L, (rCheckPtr != NULL));
    return 1;
}
int Hook_VM_SetVar(lua_State *L)
{
    //--Sets the variable.  If it doesn't exist, creates it.
    //VM_SetVar(DLPath[], "S", String[])
    //VM_SetVar(DLPath[], "N", String[])
    if(lua_gettop(L) != 3) return fprintf(stderr, "VM_SetVar:  Failed, invalid argument list\n");

    //--Fetch the var.
    bool tNewReg = false;
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    SysVar *rVar = (SysVar *)rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    //--Doesn't exist?  Create one.
    if(!rVar)
    {
        rVar = (SysVar *)malloc(sizeof(SysVar));
        rVar->mIsSaved = true;
        rVar->mNumeric = 0.0f;
        rVar->mAlpha = (char *)malloc(sizeof(char) * (strlen("NULL") + 1));
        strcpy(rVar->mAlpha, "NULL");
        tNewReg = true;
    }

    //--Set the variable appropriately.
    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "S"))
    {
        const char *rString = lua_tostring(L, 3);
        free(rVar->mAlpha);
        rVar->mAlpha = (char *)malloc(sizeof(char) * (strlen(rString) + 1));
        strcpy(rVar->mAlpha, rString);
    }
    else if(!strcmp(rTypeString, "N"))
    {
        rVar->mNumeric = lua_tonumber(L, 3);
    }

    //--If this is a new var, reg it.
    if(tNewReg)
    {
        rLibrary->AddPath(lua_tostring(L, 1));
        rLibrary->RegisterPointer(lua_tostring(L, 1), rVar, &DataLibrary::DeleteSysVar);
    }
    return 0;
}
int Hook_VM_RemVar(lua_State *L)
{
    //--Removes and deallocates the variable.
    //VM_RemVar(DLPath[])
    if(lua_gettop(L) != 1) return fprintf(stderr, "VM_RemVar:  Failed, invalid argument list\n");

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    SysVar *rVar = (SysVar *)rLibrary->RemovePointer(lua_tostring(L, 1));

    //--Doesn't exist?  Done!
    if(!rVar) return 0;

    //--Deallocate it.
    free(rVar->mAlpha);
    free(rVar);
    return 0;
}
int Hook_VM_GetVar(lua_State *L)
{
    //--Returns the value in the variable, based on the type passed.
    //VM_GetVar(DLPath[], "S")
    //VM_GetVar(DLPath[], "N")
    if(lua_gettop(L) != 2)
    {
        fprintf(stderr, "VM_GetVar:  Failed, invalid argument list\n");
        lua_pushnumber(L, 0.0f);
        return 1;
    }

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    SysVar *rVar = (SysVar *)rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    //--Doesn't exist?  Done.
    if(!rVar)
    {
        //fprintf(stderr, "VM_GetVar:  Failed, var not found\n");
        lua_pushnumber(L, 0.0f);
        return 1;
    }

    //--Return the variable appropriately.
    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "S"))
    {
        lua_pushstring(L, rVar->mAlpha);
        return 1;
    }
    else if(!strcmp(rTypeString, "N"))
    {
        lua_pushnumber(L, rVar->mNumeric);
        return 1;
    }

    fprintf(stderr, "VM_GetVar:  Failed, can't resolve %s\n", rTypeString);
    lua_pushnumber(L, 0.0f);
    return 1;
}
int Hook_VM_SetSaveFlag(lua_State *L)
{
    //--Edits the save flag.  If true, the game will save it and load it.  This
    //  is true by default.
    //VM_SetSaveFlag(DLPath[], Flag)
    if(lua_gettop(L) != 2) return fprintf(stderr, "VM_SetSaveFlag:  Failed, invalid argument list\n");

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    SysVar *rVar = (SysVar *)rLibrary->RemovePointer(lua_tostring(L, 1));

    //--Doesn't exist?  Done!
    if(!rVar) return 0;

    //--Flip the flag.
    rVar->mIsSaved = lua_toboolean(L, 2);
    return 0;
}
