//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
//--Generics
#include "StarlightLinkedList.h"

//--GUI
//--Libraries
//--Managers

void DataLibrary::WriteData(FILE *fOutfile)
{
    //--Not used in the SugarLump execution.
}
void DataLibrary::WriteCatalogue(FILE *fOutfile, StarlightLinkedList *pSection, StarlightLinkedList *pCatalogue)
{
    //--Not used in the SugarLump execution.
}
void DataLibrary::WriteHeading(FILE *fOutfile, StarlightLinkedList *pSection, StarlightLinkedList *pCatalogue, StarlightLinkedList *pHeading)
{
    //--Not used in the SugarLump execution.
}
void DataLibrary::ReadData(FILE *fInfile, int pVersion)
{
    //--Not used in the SugarLump execution.
}
void DataLibrary::ClearVMData()
{
    //--Not used in the SugarLump execution.
}
void DataLibrary::WriteVMToTextfile(const char *pPath)
{
    //--Not used in the SugarLump execution.
}
