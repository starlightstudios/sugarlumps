//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--Generics
#include "StarlightLinkedList.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
bool DataLibrary::xBlockSpecialDeletion = false;
DataLibrary::DataLibrary()
{
    //--System
    mDebugMode = false;
    mErrorMode = true;

    //--Storage
    mSectionList = new StarlightLinkedList(true);
    mActiveEntityStack = new StarlightLinkedList(false);
    mPointerIndexList = new StarlightLinkedList(true);
    mDeletionMasterList = new StarlightLinkedList(true);

    //--Counters
    mRunningIndexCounter = 1;

    //--Dummies
    mDummyPtr = new RootObject();

    //--Public Variables
    mFailSilently = false;
    rActiveObject    = NULL;
    rActiveSection   = NULL;
    rActiveCatalogue = NULL;
    rActiveHeading   = NULL;
    mActiveSectionName   = NULL;
    mActiveCatalogueName = NULL;
    mActiveHeadingName   = NULL;
    mErrorCode = 0;
}
DataLibrary::~DataLibrary()
{
    delete mSectionList;
    delete mActiveEntityStack;
    delete mPointerIndexList;
    DataLibrary::xBlockSpecialDeletion = true;
    delete mDeletionMasterList;
    delete mDummyPtr;
}
void DataLibrary::DeleteSysVar(void *pPtr)
{
    SysVar *rPtr = (SysVar *)pPtr;
    free(rPtr->mAlpha);
    free(rPtr);
}
void DataLibrary::DeleteDelPack(void *pPtr)
{
    //--Deletes a Deletion pack.  If the static boolean is true, then do NOT
    //  delete the content with its function pointer.
    DeletionPack *rPack = (DeletionPack *)pPtr;
    if(!DataLibrary::xBlockSpecialDeletion && rPack->mDLPath && rPack->rFuncPtr)
    {
        void *rPtr = DataLibrary::Fetch()->GetEntry(rPack->mDLPath);
        rPack->rFuncPtr(rPtr);
    }
    free(rPack->mDLPath);
    free(rPack);
}

///===================================== Property Queries =========================================
StarlightLinkedList *DataLibrary::GetSection(const char *pResolveString)
{
    //--Finds the Section specified.  Must be of format Root/Section/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    StarlightLinkedList *rSection = tResolvePack->rSection;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetSection(N)", pResolveString);
    return rSection;
}
StarlightLinkedList *DataLibrary::GetCatalogue(const char *pResolveString)
{
    //--Finds the Catalogue specified.  Must be of format Root/Section/Catalogue/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    StarlightLinkedList *rCatalogue = tResolvePack->rCatalogue;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetCatalogue(N)", pResolveString);
    return rCatalogue;
}
StarlightLinkedList *DataLibrary::GetHeading(const char *pResolveString)
{
    //--Finds the Heading specified.  Must be of format Root/Section/Catalogue/Heading/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    StarlightLinkedList *rHeading = tResolvePack->rHeading;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetHeading(N)", pResolveString);
    return rHeading;
}
void *DataLibrary::GetEntry(const char *pResolveString)
{
    //--Finds the Entry specified.  Must be of format Root/Section/Catalogue/Heading/Name
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    void *rEntry = tResolvePack->rEntry;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetEntry(N)", pResolveString);
    return rEntry;
}
bool DataLibrary::DoesEntryExist(const char *pResolveString)
{
    //--Returns true if the entry exists, false if it does not.
    return (GetEntry(pResolveString) != NULL);
}
bool DataLibrary::IsActiveValid()
{
    //--Returns true if the rActiveObject is not NULL.
    return (rActiveObject != NULL);
}
bool DataLibrary::IsActiveValid(int pTypeFlag)
{
    //--Returns true if the rActiveObject is not NULL, AND has the matching type flag set.
    //  Otherwise, false.
    if(!rActiveObject) return false;
    RootObject *rRootObject = (RootObject *)rActiveObject;
    return (rRootObject->GetType() == pTypeFlag);
}
bool DataLibrary::IsActiveValid(int pTypeStart, int pTypeEnd)
{
    //--Returns true if the rActiveObject is not NULL, AND has its type flag between the start and
    //  end flags passed.  Otherwise, false.
    if(!rActiveObject) return false;
    RootObject *rRootObject = (RootObject *)rActiveObject;
    return (rRootObject->GetType() >= pTypeStart && rRootObject->GetType() <= pTypeEnd);
}

///======================================= Manipulators ===========================================
void DataLibrary::setErrorFlag(bool pFlag)
{
    mErrorMode = pFlag;
}
void DataLibrary::SetErrorCode(int pCode)
{
    mErrorCode = pCode;
}
void DataLibrary::AddPath(const char *pDLPath)
{
    //--Adds the required path to the DataLibrary.  If any of the sections, catalogues, or headings
    //  specified already exist, then they are considered activated.  If they don't exist they are
    //  created, and set as active.
    //--The below three functions (AddSection, AddCatalogue, AddHeading) were built to be called
    //  through this function.
    ResolvePack *tResolvePack = CreateResolvePack(pDLPath);
    rActiveSection = tResolvePack->rSection;
    rActiveCatalogue = tResolvePack->rCatalogue;
    rActiveHeading = tResolvePack->rHeading;

    //--Create the parts.  If any pieces were already active, they fail.
    AddSection(tResolvePack->mSection);
    AddCatalogue(tResolvePack->mCatalogue);
    AddHeading(tResolvePack->mHeading);

    //--Clean
    free(tResolvePack);
}
void DataLibrary::RegisterPointer(const char *pDLPath, void *pPointer)
{
    //--Overload, doesn't use a deletion function.
    RegisterPointer(pDLPath, pPointer, &DontDeleteThis);
}
void DataLibrary::RegisterPointer(const char *pDLPath, void *pPointer, DeletionFunctionPtr pDeletionFunction)
{
    //--Registers a pointer at the given locations set.  The DataLibrary does not support the
    //  registration of NULL pointers, and will fail if that is passed.
    if(!pDLPath || !pPointer) return;

    //--Resolve
    StarlightLinkedList *rHeading = GetHeading(pDLPath);
    char *tName = DataLibrary::GetPathPart(pDLPath, DL_NAME);
    if(!rHeading || !tName) return;

    //--Register the Pointer
    rHeading->AddElement(tName, pPointer, pDeletionFunction);
    free(tName);
}
void *DataLibrary::RemovePointer(const char *pDLPath)
{
    //--Removes a pointer from its list.  The pointer is returned, but if the list had called for
    //  it to be deallocated, then the pointer returned will be unstable.  If not, the pointer will
    //  be valid and you are responsible for deallocating it.
    //--Returns NULL if something went wrong, or the entry wasn't found.
    if(!pDLPath) return NULL;

    //--Resolve
    StarlightLinkedList *rHeading = GetHeading(pDLPath);
    char *tName = DataLibrary::GetPathPart(pDLPath, DL_NAME);
    if(!rHeading || !tName) return NULL;

    //--Get the pointer itself.
    void *rElement = rHeading->RemoveElementS(tName);
    free(tName);
    return rElement;
}

//--Activity Stack
void DataLibrary::SetActiveObject(const char *pDLPath)
{
    //--Sets the rActiveObject based on the path passed.  If the object is not found, it is set to
    //  NULL instead.
    rActiveObject = GetEntry(pDLPath);
}
void DataLibrary::PushActiveEntity()
{
    //--Overload of below, pushes NULL.
    PushActiveEntity(NULL);
}
void DataLibrary::PushActiveEntity(void *pReplacer)
{
    //--Pushes the rActiveObject back one on the Activity Stack.  If the rActiveObject was NULL
    //  then a dummy pointer is pushed onto the stack instead, as RLL's do not support NULL entries.
    //--Passing NULL is acceptable, it will place NULL on top of the Activity Stack.
    if(!rActiveObject) rActiveObject = mDummyPtr;
    mActiveEntityStack->AddElementAsHead("X|DL_ActiveEntity", rActiveObject);
    rActiveObject = pReplacer;
}
void *DataLibrary::PopActiveEntity()
{
    //--Pops off the rActiveObject and replaces it with the 0th entry on the Activity Stack.  If
    //  the stack was empty, this can be NULL.  In addition, if the mDummyPtr is spotted, the
    //  rActiveObject also becomes NULL.
    rActiveObject = mActiveEntityStack->RemoveElementI(0);
    if(rActiveObject == mDummyPtr) rActiveObject = NULL;
    return rActiveObject;
}

///======================================= Core Methods ===========================================
//--Note:  These are private for a reason!
void DataLibrary::AddSection(const char *pName)
{
    //--Adds the specified section, and sets it as active.  If there is already a section active
    //  then fail.
    if(!pName || rActiveSection) return;

    StarlightLinkedList *nSection = new StarlightLinkedList(true);
    rActiveSection = nSection;
    mSectionList->AddElement(pName, nSection, &StarlightLinkedList::DeleteThis);
}
void DataLibrary::AddCatalogue(const char *pName)
{
    //--Adds the specified catalogue to the rActiveSection.  As above, don't call this directly,
    //  call it through AddPath().
    if(!pName || !rActiveSection || rActiveCatalogue) return;

    StarlightLinkedList *nCatalogue = new StarlightLinkedList(true);
    rActiveCatalogue = nCatalogue;
    rActiveSection->AddElement(pName, nCatalogue, &StarlightLinkedList::DeleteThis);
}
void DataLibrary::AddHeading(const char *pName)
{
    //--Adds the specified heading to the rActiveCatalogue.  Call this through AddPath().
    if(!pName || !rActiveCatalogue || rActiveHeading) return;

    StarlightLinkedList *nHeading = new StarlightLinkedList(true);
    rActiveHeading = nHeading;
    rActiveCatalogue->AddElement(pName, nHeading, &StarlightLinkedList::DeleteThis);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
DataLibrary *DataLibrary::Fetch()
{
    return Global::Shared()->gDataLibrary;
}
