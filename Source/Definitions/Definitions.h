///======================================== Definitions ===========================================
//--Definitions of class forward values, so other classes can freely include
//  without getting bogged down in the details.
//--Also has some universal typedefs, for likewise efficacy.

#pragma once

///--[System Headers]
#include "pch.h"

///--[OpenGL]
#include "GLDfn.h"

///--[RootObject]
//--Many classes inherit from this.
#include "RootObject.h"

///--[Function Pointers]
#ifndef _TypedefFunctionPtr_
#define _TypedefFunctionPtr_
class BaseEffect;
class ALBitmap;

//--General Purpose
typedef void(*DeletionFunctionPtr)(void *);
typedef bool(*LogicFnPtr)(void *);
typedef void(*UpdateFnPtr)(void *);
typedef void(*DrawFnPtr)(void *);

//--Class Specific
typedef ALBitmap*(*AnimationResolveFuncPtr)(void *, int&, int&);

#endif

///--[Unions]
//--Represents all kinds of pointer in one handy union.
union FlexPtr
{
    char *c;
    unsigned char *uc;
    short *s;
    unsigned short *us;
    int *i;
    unsigned int *ui;
    float *f;
    double *d;
    void *v;
};

///--[Pointer Type Macros]
//--Primitives
#define POINTER_TYPE_FAIL 0
#define POINTER_TYPE_BOOL 1
#define POINTER_TYPE_INT 2
#define POINTER_TYPE_FLOAT 3
#define POINTER_TYPE_STRING 4

//--Lump Types
#define POINTER_TYPE_LUMP_ROOT 10000
#define POINTER_TYPE_LUMP_IMAGE 10001
#define POINTER_TYPE_LUMP_MAPINFO 10002
#define POINTER_TYPE_LUMP_CLIP 10003
#define POINTER_TYPE_LUMP_LOML 10004
#define POINTER_TYPE_LUMP_LOMLCLIP 10005
#define POINTER_TYPE_LUMP_AUTOLOADER 10006
#define POINTER_TYPE_LUMP_DATA 10007

///--[Constants]
//--[Class Forward Declarations]
class Global;

#include "ClassDefinitions.h"
#include "CoreClassDefinitions.h"
#include "LibraryDefinitions.h"
#include "ManagerDefinitions.h"

///--[Lua Functions]
//--These are used by virtually every Lua function in some way, and are thus stuffed into the main
//  definitions to avoid having to include it in every Lua using class.
//--They are defined in the LuaManager's folder.
struct PropertyLookup;
int LuaArgError(const char *pFunctionName);
int LuaArgError(const char *pFunctionName, lua_State *L);
int LuaTypeError(const char *pFunctionName);
int LuaTypeError(const char *pFunctionName, lua_State *L);
int LuaPropertyError(const char *pFunctionName, const char *pSwitchType, int pArgs);
int LuaPushPropertyPtr(PropertyLookup *pLookup, lua_State *pLuaState);
void LuaSetPropertyPtr(PropertyLookup *pLookup, lua_State *pLuaState, int pIndex);
PropertyLookup *LuaDefineProperty(lua_State *pLuaState, int pIndexOfType, int pIndexOfProperty);

///--[Common Functions]
//--These are used often enough that every class should have access to them.  They are found in
//  Macros/Common/Definitions.cc
void ResetString(char *&sString, const char *pNewString);
char *InitializeString(const char *pFormat, ...);

