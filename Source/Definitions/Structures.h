//--Type-definitions of structures that are not used exclusively by one class.

#pragma once

//--Used in GetPropertyPtr functions.
typedef struct PropertyLookup
{
    void *rPtr;
    int mType;
}PropertyLookup;
