//--Forward definitions for manual definition of GL functions.  This is part
//  of SugarCube's GL Wrangling.

#pragma once

//--A
ALLEGRO_DEFINE_PROC_TYPE(void, GLATTACHSHADER_FUNC, (GLuint, GLuint));
extern GLATTACHSHADER_FUNC sglAttachShader;

//--C
ALLEGRO_DEFINE_PROC_TYPE(void, GLCOMPILESHADER_FUNC, (GLuint));
extern GLCOMPILESHADER_FUNC sglCompileShader;

ALLEGRO_DEFINE_PROC_TYPE(void, GLCOMPRESSEDTEX2D_FUNC, (GLenum, GLint, GLenum, GLsizei, GLsizei, GLint, GLsizei, const GLvoid *));
extern GLCOMPRESSEDTEX2D_FUNC sglCompressedTexImage2D;

ALLEGRO_DEFINE_PROC_TYPE(GLuint, GLCREATEPROGRAM_FUNC, (void));
extern GLCREATEPROGRAM_FUNC sglCreateProgram;

ALLEGRO_DEFINE_PROC_TYPE(GLuint, GLCREATESHADER_FUNC, (GLenum));
extern GLCREATESHADER_FUNC sglCreateShader;

//--D
ALLEGRO_DEFINE_PROC_TYPE(void, GLDELETEPROGRAM_FUNC, (GLuint));
extern GLDELETEPROGRAM_FUNC sglDeleteProgram;

ALLEGRO_DEFINE_PROC_TYPE(void, GLDELETESHADER_FUNC, (GLuint));
extern GLDELETESHADER_FUNC sglDeleteShader;

//--G
ALLEGRO_DEFINE_PROC_TYPE(void, GLGETCOMPRESSEDTEXIMAGEARB_FUNC, (GLenum, GLint, GLvoid *));
extern GLGETCOMPRESSEDTEXIMAGEARB_FUNC sglGetCompressedTexImage;

ALLEGRO_DEFINE_PROC_TYPE(void, GLGETSHADERIV_FUNC, (GLuint, GLenum, GLint *));
extern GLGETSHADERIV_FUNC sglGetShaderiv;

ALLEGRO_DEFINE_PROC_TYPE(void, GLGETSHADERINFOLOG_FUNC, (GLuint, GLsizei, GLsizei *, GLchar *));
extern GLGETSHADERINFOLOG_FUNC sglGetShaderInfoLog;

ALLEGRO_DEFINE_PROC_TYPE(void, GLGETPROGRAMIV_FUNC, (GLuint, GLenum, GLint *));
extern GLGETPROGRAMIV_FUNC sglGetProgramiv;

ALLEGRO_DEFINE_PROC_TYPE(void, GLGETPROGRAMINFOLOG_FUNC, (GLuint, GLsizei, GLsizei *, GLchar *));
extern GLGETPROGRAMINFOLOG_FUNC sglGetProgramInfoLog;

//--L
ALLEGRO_DEFINE_PROC_TYPE(void, GLLINKPROGRAM_FUNC, (GLuint));
extern GLLINKPROGRAM_FUNC sglLinkProgram;

//--S
ALLEGRO_DEFINE_PROC_TYPE(void, GLSHADERSOURCE_FUNC, (GLuint, GLsizei, const GLchar **, const GLint *));
extern GLSHADERSOURCE_FUNC sglShaderSource;

//--U
ALLEGRO_DEFINE_PROC_TYPE(void, GLUSEPROGRAM_FUNC, (GLuint));
extern GLUSEPROGRAM_FUNC sglUseProgram;


