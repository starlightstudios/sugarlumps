///=========================================== Global =============================================
//--Objects in the static global structure are typically expected to have the scope of the program's
//  runtime. Once created, they should only be deleted when the program is exiting.
//--This is doubly true for Manager classes, obviously.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
//--The global structure itself.
typedef struct
{
    //--[Variables]
    bool gQuit;
    StarlightLinkedList *gExecutionList;

    //--[Statics]
    StarlightLinkedList *gFileSystemStack;
    SugarLumpFile *gActiveLumpFile;

    //--[Libraries]
    DataLibrary *gDataLibrary;

    //--[Managers]
    LuaManager *gLuaManager;
    GLManager *gGLManager;
}GLOBAL;

///========================================== Classes =============================================
//--Singleton Global class. As above, don't delete this. It allows the global to be retrieved.
class Global
{
    private:
	Global();
	~Global()
	{
	}

    public:
	static GLOBAL *Shared()
	{
        static GLOBAL Global;
		return &Global;
	}
};
