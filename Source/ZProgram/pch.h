#ifndef _PCH_H_
#define _PCH_H_

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_opengl.h>

//Standard Headers
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdint.h>

//Lua
extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

#endif
