//--Base
#include "Program.h"

//--Classes
//--CoreClasses
#include "StarlightLinkedList.h"
#include "StarlightFileSystem.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"
#include "Startup.h"
#include "StringMacros.h"
#include "TakeDown.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"


int main(int pTotalArgs, char *pArgs[])
{
    ///--[Initialization Sequence]
    //--Command line
    SetupGlobals();
    GLOBAL *rGlobal = Global::Shared();

    //--Command line handling
    HandleCommandLine(pTotalArgs, pArgs);

    //--Initialization Sequence
    Initialize_Everything();

    //--Something went badly wrong in the init, so quit.
    if(rGlobal->gQuit) return 0;

    ///--[Debug]
    //SugarFileSystem *tFileSystem = new SugarFileSystem();
    //tFileSystem->ScanDirectory("Data/");
    //delete tFileSystem;

    ///--[Autoexec]
    //--No files to exec?  Run the Autoexec.lua file.
    StarlightLinkedList *rExecList = rGlobal->gExecutionList;
    if(rExecList->GetListSize() < 1)
    {
        rExecList->AddElement("X", CopyString("AutoExec.lua"), &DontDeleteThis);
    }

    ///--[Lua Handling]
    //--Call each of the Lua files specified by the execution list.
    char *rFilePath = (char *)rGlobal->gExecutionList->PushIterator();
    while(rFilePath)
    {
        LuaManager::Fetch()->ExecuteLuaFile(rFilePath);
        rFilePath = (char *)rGlobal->gExecutionList->AutoIterate();
    }

    ///--[Finish Up]
    //--Done, clean up and exit.
    TakeDownProgram();
    return 0;
}
