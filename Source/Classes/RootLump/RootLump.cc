//--Base
#include "RootLump.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///========================================== System ==============================================
RootLump::RootLump()
{
    //--System
    mTypeCode = POINTER_TYPE_LUMP_ROOT;
    mAddressInFile = 0;
    mAddressOfAddress = 0;
}
RootLump::~RootLump()
{

}
void RootLump::DeleteThis(void *pPtr)
{
    delete ((RootLump *)pPtr);
}

///===================================== Property Queries =========================================
int RootLump::GetType()
{
    return mTypeCode;
}

///======================================== Manipulators ==========================================
void RootLump::SetAddressOfAddress(uint64_t pAddress)
{
    mAddressOfAddress = pAddress;
}

///======================================== Core Methods ==========================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
void RootLump::WriteTo(uint64_t &sCounter, FILE *pOutfile)
{
    //--Write the Lump's data to a file.  The WriteData function should be overridden, the default
    //  behavior of WriteTo will store the position of the Lump in the file.
    mAddressInFile = sCounter;
    WriteData(sCounter, pOutfile);
}
void RootLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Override this in the daughter class.
}
void RootLump::WriteAddress(FILE *pOutfile)
{
    //--Write the address stored by the Lump to the provided file.  The SugarLumpFile takes care
    //  of writing the name.
    if(!pOutfile) return;
    fseek(pOutfile, mAddressOfAddress, SEEK_SET);
    fwrite(&mAddressInFile, sizeof(uint64_t), 1, pOutfile);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void HookToLuaState(lua_State *pLuaState)
{
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
