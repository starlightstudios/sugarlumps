///========================================= RootLump =============================================
//--Lump from which all the others inherit, has a standardized interface for writing to files and
//  incrementing the counters used by the SugarLumpFile.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class RootLump
{
    private:

    protected:
    //--System
    uint32_t mTypeCode;
    uint64_t mAddressInFile;
    uint64_t mAddressOfAddress;

    public:
    //--System
    RootLump();
    virtual ~RootLump();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    int GetType();

    //--Manipulators
    void SetAddressOfAddress(uint64_t pAddress);

    //--Core Methods
    //--Update
    //--File I/O
    void WriteTo(uint64_t &sCounter, FILE *pOutfile);
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);
    void WriteAddress(FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions
