///======================================= TimeStampInfo ==========================================
//--Stores time stamps for all files that the program knows about. These are "Last edited"
//  time stamps, so the program can only recompile files that need it.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
//--A time stamp entry, representing a file and when it was edited.
typedef struct
{
    bool mExistedThisPass;
    char *mPath;
    time_t mLastModified;
    off_t mSize;
}TimeStampEntry;

///===================================== Local Definitions ========================================
#define TSI_DEBUG_NONE 0x00
#define TSI_DEBUG_SUBDIRS 0x01
#define TSI_DEBUG_FILELOGS 0x02

///========================================== Classes =============================================
class TimeStampInfo
{
    private:
    //--System
    uint8_t mDebugLevel;

    //--Storage
    int mAtLeastOneChange;
    StarlightLinkedList *mFilesystemStack;
    StarlightLinkedList *mStorageList;

    //--Statics
    static StarlightLinkedList *xStaticList;

    protected:

    public:
    //--System
    TimeStampInfo();
    ~TimeStampInfo();
    static void DeleteThis(void *pPtr);
    static void DeleteTimeStampEntry(void *pPtr);
    static void DeleteFilesystemEntry(void *pPtr);

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetDebugLevel(uint8_t pFlags);

    //--Core Methods
    void WipeAllExistenceFlags();
    bool ScanDirectoryForChanges(const char *pPath);
    void LogFile(ALLEGRO_FS_ENTRY *pEntry);
    void LogFile(const char *pFileName, off_t pFileSize, time_t pLastModified);

    //--Update
    //--File I/O
    void PrintTimestampsTo(const char *pPath);
    void PrintTimestamps(FILE *fOutfile);
    void ScanTimestampsFrom(const char *pPath);
    void ScanTimestamps(FILE *fInfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static TimeStampInfo *GetStackHead();
    static TimeStampInfo *PushStack();
    static void PopStack();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_TS_SetDebugLevel(lua_State *L);
int Hook_TS_PushStack(lua_State *L);
int Hook_TS_PopStack(lua_State *L);
int Hook_TS_LoadFrom(lua_State *L);
int Hook_TS_PrintTo(lua_State *L);
int Hook_TS_ScanDirectory(lua_State *L);
