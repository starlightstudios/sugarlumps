//--Base
#include "TimeStampInfo.h"

//--Classes
//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
TimeStampInfo::TimeStampInfo()
{
    //--[TimeStampInfo]
    //--System
    mDebugLevel = TSI_DEBUG_NONE;

    //--Storage
    mAtLeastOneChange = false;
    mFilesystemStack = new StarlightLinkedList(true);;
    mStorageList = new StarlightLinkedList(true);
}
TimeStampInfo::~TimeStampInfo()
{
    delete mFilesystemStack;
    delete mStorageList;
}

///--[Static Functions]
void TimeStampInfo::DeleteThis(void *pPtr)
{
    TimeStampInfo *rInfo = (TimeStampInfo *)pPtr;
    delete rInfo;
}
void TimeStampInfo::DeleteTimeStampEntry(void *pPtr)
{
    TimeStampEntry *rEntry = (TimeStampEntry *)pPtr;
    free(rEntry->mPath);
    free(rEntry);
}
void TimeStampInfo::DeleteFilesystemEntry(void *pPtr)
{
    al_destroy_fs_entry((ALLEGRO_FS_ENTRY *)pPtr);
}

///--[Private Statics]
StarlightLinkedList *TimeStampInfo::xStaticList = new StarlightLinkedList(true);

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void TimeStampInfo::SetDebugLevel(uint8_t pFlags)
{
    mDebugLevel = pFlags;
}

///======================================== Core Methods ==========================================
void TimeStampInfo::WipeAllExistenceFlags()
{
    ///--[Documentation]
    //--Resets all timestamp entries to not think they exist. If a scan completes and a file is
    //  not found, then that file is purged from the system, and the timestamps are considered to
    //  have been modified.
    //--When reading off an existing filesystem save, existence is defaulted to false. This function
    //  can be used as a resetter.
    TimeStampEntry *rEntry = (TimeStampEntry *)mStorageList->PushIterator();
    while(rEntry)
    {
        rEntry->mExistedThisPass = false;
        rEntry = (TimeStampEntry *)mStorageList->AutoIterate();
    }
}
bool TimeStampInfo::ScanDirectoryForChanges(const char *pPath)
{
    ///--[Documentation]
    //--Scans the requested pathname for changes, returning true if any files were modified, added,
    //  or removed since the last time this was called.
    if(!pPath) return false;

    //--Create an entry and try to open the directory. If we can't, fail.
    ALLEGRO_FS_ENTRY *nNewStackHead = al_create_fs_entry(pPath);
    if(!al_open_directory(nNewStackHead))
    {
        al_destroy_fs_entry(nNewStackHead);
        return false;
    }

    //--Register it as the stack head.
    mFilesystemStack->AddElementAsHead("X", nNewStackHead, &TimeStampInfo::DeleteFilesystemEntry);

    //--Flag reset.
    if(mFilesystemStack->GetListSize() == 1) mAtLeastOneChange = false;

    //--Begin scanning this directory.
    ALLEGRO_FS_ENTRY *tCurrentEntry = al_read_directory(nNewStackHead);
    while(tCurrentEntry)
    {
        //--If this is a file...
        uint32_t tFileMode = al_get_fs_entry_mode(tCurrentEntry);
        if(tFileMode & ALLEGRO_FILEMODE_ISFILE)
        {
            LogFile(tCurrentEntry);
        }
        //--If this is a directory...
        else if(tFileMode & ALLEGRO_FILEMODE_ISDIR)
        {
            //--Get the path to the derived directory.
            const char *rDerivedPath = al_get_fs_entry_name(tCurrentEntry);

            //--Scan it.
            if(mDebugLevel & TSI_DEBUG_SUBDIRS) fprintf(stderr, "Scanning subdirectory %s\n", rDerivedPath);
            mAtLeastOneChange |= ScanDirectoryForChanges(rDerivedPath);
            if(mDebugLevel & TSI_DEBUG_SUBDIRS) fprintf(stderr, "After subdirectory %i\n", mAtLeastOneChange);
        }
        //--Unknown type of file.
        else
        {

        }

        //--Clean up the entry, get the next one.
        al_destroy_fs_entry(tCurrentEntry);
        tCurrentEntry = al_read_directory(nNewStackHead);
    }

    //--Clean up.
    al_close_directory(nNewStackHead);
    mFilesystemStack->RemoveElementI(0);
    return mAtLeastOneChange;
}
void TimeStampInfo::LogFile(ALLEGRO_FS_ENTRY *pEntry)
{
    ///--[Documentation]
    //--Given a filesystem entry, either updates the existing entry in the timestamp or adds a new
    //  one. If the file is added/was modified, trips the mAtLeastOneChange flag as well.
    if(!pEntry) return;

    //--Check the list of existing files to see if this entry already exists.
    const char *rFilePath = al_get_fs_entry_name(pEntry);
    if(!rFilePath) return;

    //--Scan.
    TimeStampEntry *rEntry = (TimeStampEntry *)mStorageList->GetElementByName(rFilePath);
    if(rEntry)
    {
        //--The entry did exist. Check the size and modified.
        time_t tTimeLastModified = al_get_fs_entry_mtime(pEntry);
        off_t tSize = al_get_fs_entry_size(pEntry);
        if(rEntry->mLastModified != tTimeLastModified || rEntry->mSize != tSize)
        {
            mAtLeastOneChange = true;
        }

        //--Update things (just in case).
        rEntry->mLastModified = tTimeLastModified;
        rEntry->mSize = tSize;

        //--Flip this flag on.
        rEntry->mExistedThisPass = true;
        if(mDebugLevel & TSI_DEBUG_FILELOGS) fprintf(stderr, "Scanned Existing %s\n", rEntry->mPath);
    }
    //--Entry did not exist, create a new one.
    else
    {
        //--Trip this flag.
        mAtLeastOneChange = true;

        //--Allocate and store.
        TimeStampEntry *nNewEntry = (TimeStampEntry *)malloc(sizeof(TimeStampEntry));
        nNewEntry->mExistedThisPass = true;
        nNewEntry->mPath = (char *)malloc(sizeof(char) * (strlen(rFilePath) + 1));
        strcpy(nNewEntry->mPath, rFilePath);
        nNewEntry->mLastModified = al_get_fs_entry_mtime(pEntry);
        nNewEntry->mSize = al_get_fs_entry_size(pEntry);
        mStorageList->AddElement(nNewEntry->mPath, nNewEntry, &TimeStampInfo::DeleteTimeStampEntry);

        //--Debug.
        if(mDebugLevel & TSI_DEBUG_FILELOGS) fprintf(stderr, "Scanned New %s\n", nNewEntry->mPath);
    }
}
void TimeStampInfo::LogFile(const char *pFileName, off_t pFileSize, time_t pLastModified)
{
    ///--[Documentation]
    //--Given a set of timestamp info, creates or overrides an existing log file. Unlike the above
    //  function, does NOT trip the change flag, because this function is used when loading saved
    //  data and not scanning a directory.
    if(!pFileName) return;

    //--Scan.
    TimeStampEntry *rEntry = (TimeStampEntry *)mStorageList->GetElementByName(pFileName);
    if(rEntry)
    {
        rEntry->mLastModified = pLastModified;
        rEntry->mSize = pFileSize;
    }
    //--Entry did not exist, allocate and store.
    else
    {
        TimeStampEntry *nNewEntry = (TimeStampEntry *)malloc(sizeof(TimeStampEntry));
        nNewEntry->mExistedThisPass = false;
        nNewEntry->mPath = (char *)malloc(sizeof(char) * (strlen(pFileName) + 1));
        strcpy(nNewEntry->mPath, pFileName);
        nNewEntry->mLastModified = pLastModified;
        nNewEntry->mSize = pFileSize;
        mStorageList->AddElement(nNewEntry->mPath, nNewEntry, &TimeStampInfo::DeleteTimeStampEntry);
    }

    //--Debug.
    if(mDebugLevel & TSI_DEBUG_FILELOGS) fprintf(stderr, "Logged %s - %li %li\n", pFileName, pFileSize, pLastModified);
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void TimeStampInfo::PrintTimestampsTo(const char *pPath)
{
    //--Prints timestamp information to the requested path (as opposed to a stream).
    FILE *fOutfile = fopen(pPath, "w");
    if(!fOutfile) return;
    PrintTimestamps(fOutfile);
    fclose(fOutfile);
}
void TimeStampInfo::PrintTimestamps(FILE *fOutfile)
{
    ///--[Debug]
    //--Prints the existing timestamp info to the requested file. The file can legally be a stream.
    //  Also, the file is in the same format used when reading off the hard drive, so this can be
    //  used to save the previous directory state.
    if(!fOutfile) return;

    //--First entry is how many timestamps to expect. This makes it easier to read from streams.
    fprintf(fOutfile, "%i\n", mStorageList->GetListSize());

    //--For each stamp...
    TimeStampEntry *rEntry = (TimeStampEntry *)mStorageList->PushIterator();
    while(rEntry)
    {
        //--Print the pathname|size|modified time.
        fprintf(fOutfile, "%s|%li|%li\n", rEntry->mPath, rEntry->mSize, rEntry->mLastModified);
        rEntry = (TimeStampEntry *)mStorageList->AutoIterate();
    }
}
void TimeStampInfo::ScanTimestampsFrom(const char *pPath)
{
    //--Reads timestamp information from the requested path.
    FILE *fInfile = fopen(pPath, "r");
    if(!fInfile) return;
    ScanTimestamps(fInfile);
    fclose(fInfile);
}
void TimeStampInfo::ScanTimestamps(FILE *fInfile)
{
    ///--[Documentation]
    //--Reads timestamp data in from the given file. If it's formatted incorrectly, the data might
    //  be useless.
    if(!fInfile) return;

    //--Buffers.
    char tNameBuffer[1024];
    char tSizeBuffer[1024];
    char tModiBuffer[1024];

    //--Get the expected number of entries.
    int tExpectedEntries = 0;
    fscanf(fInfile, "%i\n", &tExpectedEntries);

    //--Read entries.
    for(int i = 0; i < tExpectedEntries; i ++)
    {
        //--Read the name until we hit a '|'.
        char tCharacter = 0;
        int tBufCursor = 0;
        char *rUseBuffer = tNameBuffer;

        //--Loop characters. The \n will break out.
        while(true)
        {
            //--Read the next character.
            fread(&tCharacter, sizeof(char), 1, fInfile);

            //--Is it a bar?
            if(tCharacter == '|')
            {
                //--If on the name buffer, go to the next buffer.
                if(rUseBuffer == tNameBuffer)
                {
                    rUseBuffer = tSizeBuffer;
                    tBufCursor = 0;
                }
                //--If on the size buffer, go to the last-modified buffer.
                else if(rUseBuffer == tSizeBuffer)
                {
                    rUseBuffer = tModiBuffer;
                    tBufCursor = 0;
                }
                //--Too many bars, nullify.
                else
                {
                    rUseBuffer = NULL;
                }
            }
            //--It's not a bar, is it a \n?
            else if(tCharacter == 10 || tCharacter == 13)
            {
                break;
            }
            //--It's a normal letter.
            else
            {
                //--Put it in the buffer, or nowhere on error.
                if(rUseBuffer)
                {
                    rUseBuffer[tBufCursor+0] = tCharacter;
                    rUseBuffer[tBufCursor+1] = '\0';
                    tBufCursor ++;
                }
            }
        }

        //--Using the results, add/replace a timestamp entry.
        LogFile(tNameBuffer, atoi(tSizeBuffer), atoi(tModiBuffer));
    }
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
TimeStampInfo *TimeStampInfo::GetStackHead()
{
    //--Note: Can legally return NULL!
    return (TimeStampInfo *)xStaticList->GetElementBySlot(0);
}
TimeStampInfo *TimeStampInfo::PushStack()
{
    TimeStampInfo *nNewInfo = new TimeStampInfo();
    return (TimeStampInfo *)xStaticList->AddElementAsHead("X", nNewInfo, &TimeStampInfo::DeleteThis);
}
void TimeStampInfo::PopStack()
{
    xStaticList->RemoveElementI(0);
}

///======================================== Lua Hooking ===========================================
void TimeStampInfo::HookToLuaState(lua_State *pLuaState)
{
    /* TS_SetDebugLevel(iFlag)
       Sets the amount of debug information printed to stderr. */
    lua_register(pLuaState, "TS_SetDebugLevel", &Hook_TS_SetDebugLevel);

    /* TS_PushStack()
       Creates a new TimeStampInfo on the stack. Pop it when you're done with it. */
    lua_register(pLuaState, "TS_PushStack", &Hook_TS_PushStack);

    /* TS_PopStack()
       Pops the TimeStampInfo stack. */
    lua_register(pLuaState, "TS_PopStack", &Hook_TS_PopStack);

    /* TS_LoadFrom(sPathName)
       Loads timestamp info from a formatted text file at the given path. */
    lua_register(pLuaState, "TS_LoadFrom", &Hook_TS_LoadFrom);

    /* TS_PrintTo(sPathName)
       Prints the current stack head to the requested path. Fails if there's no stack head. */
    lua_register(pLuaState, "TS_PrintTo", &Hook_TS_PrintTo);

    /* TS_ScanDirectory(sPathName) (1 boolean)
       Scans the given directory (and any subdirectories) for changes. Returns true if any one file
       was added, removed, or modified from the last scan. If the directory has not been scanned
       and the TS has not loaded previous scan data, always returns true if the directory exists. */
    lua_register(pLuaState, "TS_ScanDirectory", &Hook_TS_ScanDirectory);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_TS_SetDebugLevel(lua_State *L)
{
    //TS_SetDebugLevel(iFlag)

    //--Check the stack head.
    TimeStampInfo *rStackHead = TimeStampInfo::GetStackHead();
    if(!rStackHead) return 0;

    //--Set.
    rStackHead->SetDebugLevel(lua_tointeger(L, 1));
    return 0;
}
int Hook_TS_PushStack(lua_State *L)
{
    //TS_PushStack()
    TimeStampInfo::PushStack();
    return 0;
}
int Hook_TS_PopStack(lua_State *L)
{
    //TS_PopStack()
    TimeStampInfo::PopStack();
    return 0;
}
int Hook_TS_LoadFrom(lua_State *L)
{
    //TS_LoadFrom(sPathName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return 0;

    //--Check the stack head.
    TimeStampInfo *rStackHead = TimeStampInfo::GetStackHead();
    if(!rStackHead) return 0;

    //--Load it.
    rStackHead->ScanTimestampsFrom(lua_tostring(L, 1));
    return 0;
}
int Hook_TS_PrintTo(lua_State *L)
{
    //TS_PrintTo(sPathName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return 0;

    //--Check the stack head.
    TimeStampInfo *rStackHead = TimeStampInfo::GetStackHead();
    if(!rStackHead) return 0;

    //--Print it.
    rStackHead->PrintTimestampsTo(lua_tostring(L, 1));
    return 0;
}
int Hook_TS_ScanDirectory(lua_State *L)
{
    //TS_ScanDirectory(sPathName) (1 boolean)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) { lua_pushboolean(L, false); return 1; }

    //--Check the stack head.
    TimeStampInfo *rStackHead = TimeStampInfo::GetStackHead();
    if(!rStackHead) { lua_pushboolean(L, false); return 1; }

    bool tResult = rStackHead->ScanDirectoryForChanges(lua_tostring(L, 1));
    lua_pushboolean(L, tResult);
    return 1;
}
