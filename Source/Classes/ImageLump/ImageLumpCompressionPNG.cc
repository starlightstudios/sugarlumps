//--Base
#include "ImageLump.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

void ImageLump::RipCompressedPNG(ALLEGRO_BITMAP *pDataMap)
{
    ///--[Documentation]
    //--Takes in a bitmap and spits out the data as a PNG file. It's not PNG-compressed data, it
    //  is literally an array of bytes representing a PNG file on the hard drive. This type of
    //  compression is very useful for Libgdx programs, as you can "trick" a program that must
    //  access files off a hard drive (for engine reasons) into using the SLF format.
    if(!pDataMap) return;

    ///--[Write]
    //--Save the bitmap onto the hard drive as a temporary file.
    al_save_bitmap("Save.png", pDataMap);

    ///--[Read]
    //--Open that puppy up as a binary file.
    FILE *fInfile = fopen("Save.png", "rb");
    if(!fInfile)
    {
        fprintf(stderr, "ImageLump:  Could not save file as PNG, check write protections\n");
        return;
    }

    //--Check filesize with ftell()
    fseek(fInfile, 0, SEEK_END);
    mDataArraySize = ftell(fInfile);
    fseek(fInfile, 0, SEEK_SET);

    //--Allocate and read
    mDataArray = (uint8_t *)malloc(mDataArraySize);
    fread(mDataArray, sizeof(uint8_t), mDataArraySize, fInfile);
}
