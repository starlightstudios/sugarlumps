//--Base
#include "ImageLump.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

void ImageLump::HandleDiffCase(const char *pPath, const char *pLoadPath)
{
    ///--[Documentation]
    //--Taking in a pathname, loads that pathname to memory and compares it to the currently stored
    //  bitmap. Every pixel which is identical is set to transparent. The result should be only
    //  the pixels that are different remain, and then trimming occurs afterwards. If the base is
    //  overlain with the diff'd image, smaller and faster animations can be made.
    //--This must be called pre-compression. Compression via jpeg or any other lossy method will
    //  cause failures in color comparisons.
    //--If NULL is passed, nothing happens. This is the default case, since this function is always
    //  called but fails if Diff mode is not activated.
    //--Alert readers might recognize this is a type of compression often used in video files. We don't
    //  specify keyframes here, that's up to a different implementation.
    DebugManager::PushPrint(false, "Diff case\n");
    if(!pPath)
    {
        DebugManager::PopPrint("Failed, path was NULL\n");
        return;
    }

    //--Make sure we have something to compare it to.
    if(!mRipBitmap)
    {
        DebugManager::PopPrint("Failed, no ripping bitmap to compare to.\n");
        return;
    }

    ///--[Bitmap Handling]
    //--Load the bitmap.
    ALLEGRO_BITMAP *tDiffBitmap = al_load_bitmap(pPath);
    if(!tDiffBitmap)
    {
        DebugManager::PopPrint("Failed, could not load the diff bitmap %s.\n", pPath);
        return;
    }

    //--Remove the mask colors
    al_convert_mask_to_alpha(tDiffBitmap, al_map_rgb( 64,  64,  64));
    al_convert_mask_to_alpha(tDiffBitmap, al_map_rgb( 34, 177,  76));
    al_convert_mask_to_alpha(tDiffBitmap, al_map_rgb(255,   0, 255));

    //--Setup
    ALLEGRO_COLOR tReadColor;
    ALLEGRO_COLOR tDiffColor;
    int tXSize = al_get_bitmap_width(mRipBitmap);
    int tYSize = al_get_bitmap_height(mRipBitmap);
    al_set_target_bitmap(mRipBitmap);

    ///--[Comparison]
    //--Begin comparison job.
    for(int x = 0; x < tXSize; x ++)
    {
        for(int y = 0; y < tYSize; y ++)
        {
            //--Get the colors.
            tReadColor = al_get_pixel(mRipBitmap,  x, y);
            tDiffColor = al_get_pixel(tDiffBitmap, x, y);

            //--Are they the same in both images?  Now transparent.
            if(tReadColor.r == tDiffColor.r && tReadColor.g == tDiffColor.g && tReadColor.b == tDiffColor.b)
            {
                al_put_pixel(x, y, al_map_rgba(0, 0, 0, 0));
            }
            //--Is the "new" pixel transparent, while the old one was not?  Pass an alpha of 1 for that.
            //  This allows for diff overmasking if the depth or alpha tester is used, but if it is
            //  not used, should not be visibly noticeable.
            else if(tDiffColor.a > 0 && tReadColor.a == 0)
            {
                al_put_pixel(x, y, al_map_rgba(255, 255, 255, 1));
            }
            //--Final case.  We don't allow pixels with 1 alpha to be stored for any reason.  If we
            //  have a 1, switch it to a 2.
            else
            {
                if(tReadColor.a == 1.0f / 255.0f) tReadColor.a = 2.0f / 255.0f;
                al_put_pixel(x, y, tReadColor);
            }
        }
    }

    //--Output it (DEBUG!)
    /*char tOutBuffer[512];
    strcpy(tOutBuffer, pLoadPath);
    for(uint32_t i = 0; i < strlen(tOutBuffer); i ++)
    {
        if(tOutBuffer[i] == '.')
        {
            tOutBuffer[i+0] = '1';
            tOutBuffer[i+1] = '.';
            tOutBuffer[i+2] = 'p';
            tOutBuffer[i+3] = 'n';
            tOutBuffer[i+4] = 'g';
            tOutBuffer[i+5] = '\0';
            i = strlen(tOutBuffer);
        }
    }
    al_save_bitmap(tOutBuffer, mRipBitmap);
    DebugManager::PopPrint("Complete.  Saved to %s.\n", tOutBuffer);*/
    DebugManager::PopPrint("Complete.\n");
    al_destroy_bitmap(tDiffBitmap);
}
void ImageLump::RemoveSpecialAlphas()
{
    ///--[Documentation]
    //--If a flag is set, then the alpha 1 is removed and replaced with an alpha of 2. This is because
    //  some programs may desire the 1 alpha to be a special color flag, and we don't support stencil
    //  or depth buffers (because they are only for advanced machines as of 2013).
    //--This is probably legacy code in future-year 2022.
    if(!xAllowSpecialAlphaTrimmer) return;

    //--Setup
    ALLEGRO_COLOR tReadColor;
    int tXSize = al_get_bitmap_width(mRipBitmap);
    int tYSize = al_get_bitmap_height(mRipBitmap);
    al_set_target_bitmap(mRipBitmap);

    //--Begin comparison job.
    for(int x = 0; x < tXSize; x ++)
    {
        for(int y = 0; y < tYSize; y ++)
        {
            //--Get the color.
            tReadColor = al_get_pixel(mRipBitmap,  x, y);

            //--Compare for special flags.
            if(tReadColor.a == 1.0f / 255.0f)
            {
                tReadColor.a = 2.0f / 255.0f;
                al_put_pixel(x, y, tReadColor);
            }
        }
    }
}
