//--Base
#include "ImageLump.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

void ImageLump::TrimBitmap()
{
    ///--[Documentation]
    //--Taking in the mRipBitmap, check the edges for transparency. If an entire row or column is
    //  completely transparent, the file doesn't need to save that, and we can trim it off.
    //--After this function is through, the image's edges will be removed. It may or may not have
    //  destroyed the original in the process!
    if(!mRipBitmap) return;

    //--Setup
    ALLEGRO_COLOR tReadColor;
    int tXSize = al_get_bitmap_width(mRipBitmap);
    int tYSize = al_get_bitmap_height(mRipBitmap);
    mDataWidth = tXSize;
    mDataHeight = tYSize;

    ///--[Left Edge]
    bool tDetectedNonBlank = false;
    for(int x = 0; x < tXSize; x ++)
    {
        for(int y = 0; y < tYSize; y ++)
        {
            tReadColor = al_get_pixel(mRipBitmap, x, y);
            if(tReadColor.a > 0)
            {
                mXOffset = x;
                x = tXSize;
                y = tYSize;
                tDetectedNonBlank = true;
            }
        }
    }

    //--If every single pixel is transparent, we write a special code of cIsMonoColor which tells
    //  the reader not to bother with any other data, the entire image is one pixel.
    if(!tDetectedNonBlank)
    {
        mLocalCompression = cIsMonoColor;
        return;
    }

    ///--[Top Edge]
    for(int y = 0; y < tYSize; y ++)
    {
        for(int x = 0; x < tXSize; x ++)
        {
            tReadColor = al_get_pixel(mRipBitmap, x, y);
            if(tReadColor.a > 0)
            {
                mYOffset = y;
                x = tXSize;
                y = tYSize;
            }
        }
    }

    ///--[Right Edge]
    for(int x = tXSize-1; x >= 0; x --)
    {
        for(int y = 0; y < tYSize; y ++)
        {
            tReadColor = al_get_pixel(mRipBitmap, x, y);
            if(tReadColor.a > 0)
            {
                mDataWidth = x-mXOffset+1;
                x = -1;
                y = tYSize;
            }
        }
    }

    ///--[Bottom Edge]
    for(int y = tYSize-1; y >= 0; y --)
    {
        for(int x = 0; x < tXSize; x ++)
        {
            tReadColor = al_get_pixel(mRipBitmap, x, y);
            if(tReadColor.a > 0)
            {
                mDataHeight = y-mYOffset+1;
                x = tXSize;
                y = -1;
            }
        }
    }

    ///--[Compression Flag Handling]
    //--If we have a compression flag set, we *must* trim this to multiples-of-four sizes. GL cannot
    //  compress textures that are not multiples of 4 in dimensions!
    if(mLocalCompression >= ImageLump::cDXT1Compression && mLocalCompression <= cDXT5Compression)
    {
        if(mDataWidth % 4 != 0)
        {
            mDataWidth += 4 - (mDataWidth % 4);
        }
        if(mDataHeight % 4 != 0)
        {
            mDataHeight += 4 - (mDataHeight % 4);
        }
    }

    //--Was anything offset/trimmed?
    if(mXOffset == 0 && mYOffset == 0 && mDataWidth == mTrueWidth && mDataHeight == mTrueHeight) return;

    ///--[New Bitmap Handling]
    //--Create a new, smaller bitmap. It could also, theoretically, be larger if the compression
    //  flag was set and the true X/Y sizes were odd numbers, AND no trimming occurred.
    //--An outside chance, BUT CAN YOU TAKE THAT RISK? Yes.
    ALLEGRO_BITMAP *nBitmap = al_create_bitmap(mDataWidth, mDataHeight);
    al_set_target_bitmap(nBitmap);
    al_clear_to_color(al_map_rgba(0, 0, 0, 0));

    //--Paste the old one onto it.
    al_draw_bitmap_region(mRipBitmap, mXOffset, mYOffset, mDataWidth, mDataHeight, 0, 0, 0);

    //--Destroy the old bitmap.
    al_destroy_bitmap(mRipBitmap);
    mRipBitmap = nBitmap;
}
