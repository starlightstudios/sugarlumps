//--Base
#include "ImageLump.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

void ImageLump::RipUncompressed(ALLEGRO_BITMAP *pDataMap)
{
    //--Takes in a bitmap and spits out the data of the bitmap as RGBA pixel data, 32-bit depth.
    //  Note that the image will be 'flipped' since OpenGL loads bitmaps upside-down.
    //--Returns NULL on failure.
    if(!pDataMap) return;

    //--Debug.
    if(xAllowLineCompression && RipCompressedByLine(pDataMap)) return;

    //--Declare the size needed.
    mDataArraySize = (sizeof(uint8_t) * (mDataWidth * mDataHeight * 4));
    mDataArray = (uint8_t *)malloc(mDataArraySize);

    //--Setup
    ALLEGRO_COLOR tFirstColor;
    ALLEGRO_COLOR tColor;
    uint8_t r, g, b, a;
    int tCursor = 0;

    //--Read each pixel out, bottom to top, left to right.
    bool mHasReadFirstPixel = false;
    bool mAllTheSameColor = true;
    //for(int y = 0; y < mDataHeight; y ++)
    for(int y = mDataHeight-1; y >= 0; y --)
    {
        for(int x = 0; x < mDataWidth; x ++)
        {
            //--Read the color, check to see if there was a match.
            tColor = al_get_pixel(pDataMap, x, y);

            //--On the first pass, store the color.
            if(!mHasReadFirstPixel)
            {
                tFirstColor = al_get_pixel(pDataMap, x, y);
                mHasReadFirstPixel = true;
            }
            //--All subsequent passes, check vs. first color.  If every single check is the
            //  same color, we have a monocolor bitmap.
            else if(mAllTheSameColor)
            {
                if(tFirstColor.r != tColor.r || tFirstColor.g != tColor.g ||
                   tFirstColor.b != tColor.b || tFirstColor.a != tColor.a)
                {
                    mAllTheSameColor = false;
                }
            }

            //--Store the color as it is.
            al_unmap_rgba(tColor, &r, &g, &b, &a);
            mDataArray[tCursor+0] = r;
            mDataArray[tCursor+1] = g;
            mDataArray[tCursor+2] = b;
            mDataArray[tCursor+3] = a;
            tCursor += (sizeof(uint8_t) * 4);
        }
    }

    //--Hey, were all the pixels the same color?  If so, just send through a single pixel of data
    //  and change the compression flag.
    //--This is currently deprecated since it is almost never used and just slows down compression.
    return;
    if(mAllTheSameColor)
    {
        //--Free old data.
        free(mDataArray);

        //--Declare the size needed.
        mDataArraySize = (sizeof(uint8_t) * 4);
        mDataArray = (uint8_t *)malloc(mDataArraySize);
        al_unmap_rgba(tColor, &r, &g, &b, &a);
        mDataArray[0] = r;
        mDataArray[1] = g;
        mDataArray[2] = b;
        mDataArray[3] = a;

        //--Flip the flag.  Other routines should check against this.
        mLocalCompression = cIsMonoColor;
    }
}
