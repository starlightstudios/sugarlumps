//--Base
#include "ImageLump.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

void ImageLump::RipCompressedDXT(ALLEGRO_BITMAP *pDataMap, int pCompressionFormat)
{
    ///--[Documentation]
    //--Provided an Allegro data map, and a GL compression format, spits out the same texture data
    //  as its compressed version. We let the GL driver do the hard work and just take the data
    //  back, so any valid compression type will do.
    //--See http://www.opengl.org/sdk/docs/man/xhtml/glTexImage2D.xml for a list of accepted
    //  formats. Also, the function doesn't care what type you pass in, so theoretically you could
    //  send in an uncompressed format, though that'd bark errors.
    //--This may or may not work on Vulkan, I don't know. Don't use it for Vulkan you goober!
    if(!pDataMap) return;

    ///--[Raw Data]
    //--Incidentally, the data that GL will need is exactly the same as the Bitmap type compression
    //  so we can let that routine do the work for us too.
    //--If the bitmap was monocolored, stop now.  The routine will have handled it already.
    RipUncompressed(pDataMap);
    if(mLocalCompression == cIsMonoColor) return;

    //--Check validity.
    if(!mDataArray) return;
    int tOldSize = mDataArraySize;
    DebugManager::Print("Sizes: %i %i\n", mDataWidth, mDataHeight);

    ///--[Upload]
    //--Upload this to the OpenGL context.
    uint32_t tHandle = 0;
    glGenTextures(1, &tHandle);
    glBindTexture(GL_TEXTURE_2D, tHandle);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA, mDataWidth, mDataHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mDataArray);

    //--Free the old data.
    mDataArraySize = 0;
    free(mDataArray);
    mDataArray = NULL;

    //--Error checks
    int tWasCompressed = 0;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED, &tWasCompressed);
    if(tWasCompressed)
    {
        DebugManager::Print("Texture compressed correctly\n");
    }
    else
    {
        DebugManager::Print("Texture failed to compress\n");
    }

    ///--[Get GL Data]
    //--Free the old data, and get the expected size.
    int tSizeVal = 0;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE_ARB, &tSizeVal);
    if(tSizeVal < 1)
    {
        fprintf(stderr, "Major error compressing image!\n");
        return;
    }
    DebugManager::Print("Size comparison: %i / %i = %f", tOldSize, tSizeVal, ((float)tSizeVal / (float)tOldSize));

    //--Alloc
    mDataArraySize = tSizeVal;
    mDataArray = (uint8_t *)malloc(sizeof(uint8_t) * mDataArraySize);

    //--Pull it back off the graphics card.
    //glPixelStorei(GL_PACK_ALIGNMENT, 1);
    sglGetCompressedTexImage(GL_TEXTURE_2D, 0, mDataArray);

    //--Destroy the old data.
    glDeleteTextures(1, &tHandle);
}
