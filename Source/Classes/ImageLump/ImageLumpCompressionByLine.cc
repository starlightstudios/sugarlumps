//--Base
#include "ImageLump.h"

//--Classes
//--CoreClasses
#include "StarlightBitmap.h"
#include "StarlightLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================= Structures ===========================================
///--[Color32]
//--Represents 32 bits of color split into four channels.
typedef struct Color32
{
    uint8_t mRed, mGreen, mBlue, mAlpha;
}Color32;

///--[PixelInfoPack]
//--Represents a line of pixels. Does not specify if that line is contiguous or multiple lines.
typedef struct PixelInfoPack
{
    //--Members
    uint16_t mPixelsTotal;
    uint16_t mPixelStart;
    Color32 *mPixels;

    //--Functions
    static void DeleteThis(void *pPtr)
    {
        PixelInfoPack *rPtr = (PixelInfoPack *)pPtr;
        free(rPtr->mPixels);
        free(rPtr);
    }
}PixelInfoPack;

///===================================== Primary Function =========================================
bool ImageLump::RipCompressedByLine(ALLEGRO_BITMAP *pDataMap)
{
    ///--[Documentation]
    //--Special format. Takes in an image and then compresses it using no special compression. Instead, each line
    //  of the bitmap (will attempt both vertical and horizontal if flagged to) will have a set of values indicating
    //  how many pixels in the image are not transparent. This works well with images that have long lines of
    //  opaque pixels followed by long lines of transparent ones.
    //--The program automatically checks the size of the resulting image against that of an uncompressed bitmap. If
    //  the header makes it larger, then the original uncompressed version is used. Set it, and forget it!
    //--The format is similar to gif compression: Each block has a position it starts at, how many pixels it contains,
    //  and then a list of the RGBA values for those pixels.

    ///--[Size Comparison]
    //--Comparison size. This is how big the image would be with no compression. If the size passes it, we
    //  immediately stop and go to standard no-compression.
    uint32_t tComparisonSize = (sizeof(uint8_t) * (mDataWidth * mDataHeight * 4));

    //--First, try horizontal mode.
    CompressionInfo *tHorizontalInfo = ScanHorizontal(pDataMap);

    //--Now, vertical.
    CompressionInfo *tVerticalInfo = ScanVertical(pDataMap);

    //--Print.
    if(ImageLump::xDebugLevel & IL_DEBUG_SIZES)
    {
        fprintf(stderr, "Comparison: %i bytes uncompressed.\n", tComparisonSize);
        fprintf(stderr, "            %i bytes horizontal.\n", tHorizontalInfo->mExpectedFileSize);
        fprintf(stderr, "            %i bytes vertical.\n", tVerticalInfo->mExpectedFileSize);
    }

    ///--[No Compression]
    //--Both are larger than the basic uncompressed version, so discard them.
    if(tHorizontalInfo->mExpectedFileSize > tComparisonSize && tVerticalInfo->mExpectedFileSize > tComparisonSize)
    {
        //--Delete them both, return false.
        CompressionInfo::DeleteThis(tHorizontalInfo);
        CompressionInfo::DeleteThis(tVerticalInfo);
        return false;
    }
    ///--[Horizontal Compression]
    //--At least one of them is smaller. In this case, the horizontal.
    else if(tHorizontalInfo->mExpectedFileSize < tVerticalInfo->mExpectedFileSize)
    {
        //--Allocate.
        int tCursor = 0;
        mDataArraySize = tHorizontalInfo->mExpectedFileSize;
        mDataArray = (uint8_t *)malloc(mDataArraySize);
        mLocalCompression = cHorizontalLineCompression;

        //--Start writing.
        PixelInfoPack *rInfoPack = (PixelInfoPack *)tHorizontalInfo->mDataList->PushIterator();
        while(rInfoPack)
        {
            //--Pack contains at least one pixel:
            if(rInfoPack->mPixelsTotal > 0)
            {
                //--Header: How many pixels we contain.
                uint16_t *rPixelPtr = (uint16_t *)&mDataArray[tCursor];
                *rPixelPtr = rInfoPack->mPixelsTotal;
                tCursor += sizeof(uint16_t);

                //--What X position the pixels start at.
                rPixelPtr = (uint16_t *)&mDataArray[tCursor];
                *rPixelPtr = rInfoPack->mPixelStart;
                tCursor += sizeof(uint16_t);

                //--Now write the pixel RGBA info.
                for(int i = 0; i < rInfoPack->mPixelsTotal; i ++)
                {
                    mDataArray[tCursor+0] = rInfoPack->mPixels[i].mRed;
                    mDataArray[tCursor+1] = rInfoPack->mPixels[i].mGreen;
                    mDataArray[tCursor+2] = rInfoPack->mPixels[i].mBlue;
                    mDataArray[tCursor+3] = rInfoPack->mPixels[i].mAlpha;
                    tCursor += sizeof(uint8_t) * 4;
                }
                //fprintf(stderr, " Wrote a pack, cursor is %i\n", tCursor);
            }
            //--Zero pixels. Just write 0, that indicates this is a line-end case.
            else
            {
                mDataArray[tCursor+0] = 0;
                mDataArray[tCursor+1] = 0;
                tCursor = tCursor + 2;
                //fprintf(stderr, " Newline: %i\n", tCursor);
            }

            //--Next.
            rInfoPack = (PixelInfoPack *)tHorizontalInfo->mDataList->AutoIterate();
        }

        //--Debug.
        if(ImageLump::xDebugLevel & IL_DEBUG_POSITIONS) fprintf(stderr, "After writing, position of cursor was %i\n", tCursor);

        //--Pass back true so we don't double-rip the image.
        CompressionInfo::DeleteThis(tHorizontalInfo);
        CompressionInfo::DeleteThis(tVerticalInfo);
        return true;
    }
    ///--[Vertical Compression]
    //--Vertical is smaller.
    else
    {
        //--Allocate.
        int tCursor = 0;
        mDataArraySize = tVerticalInfo->mExpectedFileSize;
        mDataArray = (uint8_t *)malloc(mDataArraySize);
        mLocalCompression = cVeritcalLineCompression;

        //--Start writing.
        PixelInfoPack *rInfoPack = (PixelInfoPack *)tVerticalInfo->mDataList->PushIterator();
        while(rInfoPack)
        {
            //--Pack contains at least one pixel:
            if(rInfoPack->mPixelsTotal > 0)
            {
                //--Header: How many pixels we contain.
                uint16_t *rPixelPtr = (uint16_t *)&mDataArray[tCursor];
                *rPixelPtr = rInfoPack->mPixelsTotal;
                tCursor += sizeof(uint16_t);

                //--What X position the pixels start at.
                rPixelPtr = (uint16_t *)&mDataArray[tCursor];
                *rPixelPtr = rInfoPack->mPixelStart;
                tCursor += sizeof(uint16_t);

                //--Now write the pixel RGBA info.
                for(int i = 0; i < rInfoPack->mPixelsTotal; i ++)
                {
                    mDataArray[tCursor+0] = rInfoPack->mPixels[i].mRed;
                    mDataArray[tCursor+1] = rInfoPack->mPixels[i].mGreen;
                    mDataArray[tCursor+2] = rInfoPack->mPixels[i].mBlue;
                    mDataArray[tCursor+3] = rInfoPack->mPixels[i].mAlpha;
                    tCursor += sizeof(uint8_t) * 4;
                }
            }
            //--Zero pixels. Just write 0, that indicates this is a line-end case.
            else
            {
                mDataArray[tCursor+0] = 0;
                mDataArray[tCursor+1] = 0;
                tCursor = tCursor + 2;
            }

            //--Next.
            rInfoPack = (PixelInfoPack *)tVerticalInfo->mDataList->AutoIterate();
        }

        //--Debug.
        if(ImageLump::xDebugLevel & IL_DEBUG_POSITIONS) fprintf(stderr, "After writing, position of cursor was %i\n", tCursor);

        //--Pass back true so we don't double-rip the image.
        CompressionInfo::DeleteThis(tHorizontalInfo);
        CompressionInfo::DeleteThis(tVerticalInfo);
        return true;
    }

    //--Impossible case. Here to make the compiler shut up.
    return false;
}

///--[Worker Functions]
CompressionInfo *ImageLump::ScanHorizontal(ALLEGRO_BITMAP *pDataMap)
{
    ///--[Documentation]
    //--Scans the given bitmap and evaluates how large the data would be with compression. Returns a heap-allocated CompressionInfo
    //  which can be used to evaluate the sizing.
    //fprintf(stderr, "== Scan Horizontal ==\n");

    //--Allocate.
    CompressionInfo *nPack = (CompressionInfo *)malloc(sizeof(CompressionInfo));
    nPack->mExpectedFileSize = 0;
    nPack->mDataList = new StarlightLinkedList(true);

    //--This algorithm scans along each Y line.
    for(int y = 0; y < mDataHeight; y ++)
    {
        //--Variable reset.
        int tStartedX = 0;
        int tPixelCount = 0;
        bool tIsScanningOpaques = false;

        //--Package pointer. Is used multiple times.
        PixelInfoPack *tActivePack = NULL;

        //--Scan left to right.
        for(int x = 0; x < mDataWidth; x ++)
        {
            //--Get the color.
            ALLEGRO_COLOR tColor = al_get_pixel(pDataMap, x, y);

            //--Check the color's alpha value. If it's zero...
            if(tColor.a == 0.0f)
            {
                //--If we are scanning opaques, that ends the series.
                if(tIsScanningOpaques)
                {
                    //--We now know how many pixels to expect. Allocate.
                    tActivePack->mPixelStart = tStartedX;
                    tActivePack->mPixelsTotal = (x - tStartedX);
                    tActivePack->mPixels = (Color32 *)malloc(sizeof(Color32) * tActivePack->mPixelsTotal);
                    //fprintf(stderr, " Package at %i: %i\n", tActivePack->mPixelStart, tActivePack->mPixelsTotal);

                    //--Tracking.
                    tPixelCount = tPixelCount + tActivePack->mPixelsTotal;

                    //--Scan those pixels in.
                    for(int p = 0; p < (x - tStartedX); p ++)
                    {
                        ALLEGRO_COLOR tColor = al_get_pixel(pDataMap, p + tStartedX, y);
                        al_unmap_rgba(tColor, &tActivePack->mPixels[p].mRed, &tActivePack->mPixels[p].mGreen, &tActivePack->mPixels[p].mBlue, &tActivePack->mPixels[p].mAlpha);
                    }

                    //--Deactivate opaque mode.
                    tActivePack = NULL;
                    tIsScanningOpaques = false;
                }
                //--If not scanning opaques, then carry on.
                else
                {
                }
            }
            //--If the value is non-zero...
            else
            {
                //--If we are scanning opaques, then carry on.
                if(tIsScanningOpaques)
                {
                }
                //--If not scanning opaques, then start a new series.
                else
                {
                    //--Flags.
                    tStartedX = x;
                    tIsScanningOpaques = true;

                    //--Create and register a new pixel pack.
                    tActivePack = (PixelInfoPack *)malloc(sizeof(PixelInfoPack));
                    tActivePack->mPixelsTotal = 0;
                    tActivePack->mPixels = NULL;
                    nPack->mDataList->AddElementAsTail("X", tActivePack, PixelInfoPack::DeleteThis);
                }
            }
        }

        //--If we exit the loop while still in a series, then the package runs the remainder of the height.
        if(tIsScanningOpaques)
        {
            //--We now know how many pixels to expect. Allocate.
            tActivePack->mPixelStart = tStartedX;
            tActivePack->mPixelsTotal = (mDataWidth - tStartedX);
            tActivePack->mPixels = (Color32 *)malloc(sizeof(Color32) * tActivePack->mPixelsTotal);
            //fprintf(stderr, " Package at %i %i: %i\n", x, tActivePack->mPixelStart, tActivePack->mPixelsTotal);
            tPixelCount += tActivePack->mPixelsTotal;

            //--Scan those pixels in.
            for(int p = 0; p < (mDataWidth - tStartedX); p ++)
            {
                ALLEGRO_COLOR tColor = al_get_pixel(pDataMap, tStartedX + p, y);
                al_unmap_rgba(tColor, &tActivePack->mPixels[p].mRed, &tActivePack->mPixels[p].mGreen, &tActivePack->mPixels[p].mBlue, &tActivePack->mPixels[p].mAlpha);
            }

            //--Deactivate opaque mode.
            tActivePack = NULL;
            tIsScanningOpaques = false;
        }

        //--Put a pixel pack with 0 pixels in it. This indicates the end of a line.
        tActivePack = (PixelInfoPack *)malloc(sizeof(PixelInfoPack));
        tActivePack->mPixelsTotal = 0;
        tActivePack->mPixels = NULL;
        nPack->mDataList->AddElementAsTail("X", tActivePack, PixelInfoPack::DeleteThis);
    }

    //--Compute. The size is the number of pixels plus 32 bits for each header.
    PixelInfoPack *rInfoPack = (PixelInfoPack *)nPack->mDataList->PushIterator();
    while(rInfoPack)
    {
        //--If the pixel count is 0, that means this is a line-ender pack.
        if(rInfoPack->mPixelsTotal == 0)
        {
            nPack->mExpectedFileSize = nPack->mExpectedFileSize + (sizeof(int16_t) * 1);
        }
        //--Otherwise, this is a full pixel pack.
        else
        {
            //--Add for the header.
            nPack->mExpectedFileSize = nPack->mExpectedFileSize + (sizeof(int16_t) * 2);

            //--Add for the pixel count.
            nPack->mExpectedFileSize = nPack->mExpectedFileSize + (sizeof(uint8_t) * 4 * rInfoPack->mPixelsTotal);
        }

        //--Next pack.
        rInfoPack = (PixelInfoPack *)nPack->mDataList->AutoIterate();
    }

    //--Finish up.
    return nPack;
}
CompressionInfo *ImageLump::ScanVertical(ALLEGRO_BITMAP *pDataMap)
{
    ///--[Documentation]
    //--Scans the given bitmap and evaluates how large the data would be with compression. Returns this value.
    //fprintf(stderr, "== Scan Vertical ==\n");

    //--Allocate.
    CompressionInfo *nPack = (CompressionInfo *)malloc(sizeof(CompressionInfo));
    nPack->mExpectedFileSize = 0;
    nPack->mDataList = new StarlightLinkedList(true);

    //--This algorithm scans along each X line.
    for(int x = 0; x < mDataWidth; x ++)
    {
        //--Variable reset.
        int tStartedY = 0;
        int tPixelCount = 0;
        bool tIsScanningOpaques = false;

        //--Package pointer. Is used multiple times.
        PixelInfoPack *tActivePack = NULL;

        //--Scan top to bottom.
        for(int y = 0; y < mDataHeight; y ++)
        {
            //--Get the color.
            ALLEGRO_COLOR tColor = al_get_pixel(pDataMap, x, y);

            //--Check the color's alpha value. If it's zero...
            if(tColor.a == 0.0f)
            {
                //--If we are scanning opaques, that ends the series.
                if(tIsScanningOpaques)
                {
                    //--We now know how many pixels to expect. Allocate.
                    tActivePack->mPixelStart = tStartedY;
                    tActivePack->mPixelsTotal = (y - tStartedY);
                    tActivePack->mPixels = (Color32 *)malloc(sizeof(Color32) * tActivePack->mPixelsTotal);
                    //fprintf(stderr, " Package at %i %i: %i\n", x, tActivePack->mPixelStart, tActivePack->mPixelsTotal);
                    tPixelCount += tActivePack->mPixelsTotal;

                    //--Scan those pixels in.
                    for(int p = 0; p < (y - tStartedY); p ++)
                    {
                        ALLEGRO_COLOR tColor = al_get_pixel(pDataMap, x, tStartedY + p);
                        al_unmap_rgba(tColor, &tActivePack->mPixels[p].mRed, &tActivePack->mPixels[p].mGreen, &tActivePack->mPixels[p].mBlue, &tActivePack->mPixels[p].mAlpha);
                    }

                    //--Deactivate opaque mode.
                    tActivePack = NULL;
                    tIsScanningOpaques = false;
                }
                //--If not scanning opaques, then carry on.
                else
                {
                }
            }
            //--If the value is non-zero...
            else
            {
                //--If we are scanning opaques, then carry on.
                if(tIsScanningOpaques)
                {
                }
                //--If not scanning opaques, then start a new series.
                else
                {
                    //--Flags.
                    tStartedY = y;
                    tIsScanningOpaques = true;

                    //--Create and register a new pixel pack.
                    tActivePack = (PixelInfoPack *)malloc(sizeof(PixelInfoPack));
                    tActivePack->mPixelsTotal = 0;
                    tActivePack->mPixels = NULL;
                    nPack->mDataList->AddElementAsTail("X", tActivePack, PixelInfoPack::DeleteThis);
                }
            }
        }

        //--If we exit the loop while still in a series, then the package runs the remainder of the height.
        if(tIsScanningOpaques)
        {
            //--We now know how many pixels to expect. Allocate.
            tActivePack->mPixelStart = tStartedY;
            tActivePack->mPixelsTotal = (mDataHeight - tStartedY);
            tActivePack->mPixels = (Color32 *)malloc(sizeof(Color32) * tActivePack->mPixelsTotal);
            //fprintf(stderr, " Package at %i %i: %i\n", x, tActivePack->mPixelStart, tActivePack->mPixelsTotal);
            tPixelCount += tActivePack->mPixelsTotal;

            //--Scan those pixels in.
            for(int p = 0; p < (mDataHeight - tStartedY); p ++)
            {
                ALLEGRO_COLOR tColor = al_get_pixel(pDataMap, x, tStartedY + p);
                al_unmap_rgba(tColor, &tActivePack->mPixels[p].mRed, &tActivePack->mPixels[p].mGreen, &tActivePack->mPixels[p].mBlue, &tActivePack->mPixels[p].mAlpha);
            }

            //--Deactivate opaque mode.
            tActivePack = NULL;
            tIsScanningOpaques = false;
        }

        //--Put a pixel pack with 0 pixels in it. This indicates the end of a line.
        tActivePack = (PixelInfoPack *)malloc(sizeof(PixelInfoPack));
        tActivePack->mPixelsTotal = 0;
        tActivePack->mPixels = NULL;
        nPack->mDataList->AddElementAsTail("X", tActivePack, PixelInfoPack::DeleteThis);
    }

    //--Debug.
    //fprintf(stderr, "There are %i pixels in this image.\n", tPixelCount);
    //fprintf(stderr, "There are %i packages in this image.\n", tDataList->GetListSize());

    //--Compute. The size is the number of pixels plus 32 bits for each header.
    PixelInfoPack *rInfoPack = (PixelInfoPack *)nPack->mDataList->PushIterator();
    while(rInfoPack)
    {
        //--If the pixel count is 0, that means this is a line-ender pack.
        if(rInfoPack->mPixelsTotal == 0)
        {
            nPack->mExpectedFileSize = nPack->mExpectedFileSize + (sizeof(int16_t) * 1);
        }
        //--Otherwise, this is a full pixel pack.
        else
        {
            //--Add for the header.
            nPack->mExpectedFileSize = nPack->mExpectedFileSize + (sizeof(int16_t) * 2);

            //--Add for the pixel count.
            nPack->mExpectedFileSize = nPack->mExpectedFileSize + (sizeof(uint8_t) * 4 * rInfoPack->mPixelsTotal);
        }

        //--Next pack.
        rInfoPack = (PixelInfoPack *)nPack->mDataList->AutoIterate();
    }

    //--Finish up.
    return nPack;
}

