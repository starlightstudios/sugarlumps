//--Base
#include "ImageLump.h"

//--Classes
#include "SugarLumpFile.h"

//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"
#include "StringMacros.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "GLManager.h"

///========================================== System ==============================================
ImageLump::ImageLump()
{
    //--[RootLump]
    //--System
    mTypeCode = POINTER_TYPE_LUMP_IMAGE;

    //--[ImageLump]
    //--System
    mLocalCompression = ImageLump::cInvalidCompression;

    //--Storage
    mDataWidth = 0;
    mDataHeight = 0;
    mTrueWidth = 0;
    mTrueHeight = 0;
    mXOffset = 0;
    mYOffset = 0;
    mDataArraySize = 0;
    mDataArray = NULL;
    mRipBitmap = NULL;

    //--Hitboxes
    mTotalHitboxes = 0;
    mHitboxesList = new StarlightLinkedList(true);

    //--Public Variables
    mDiffPath = NULL;
}
ImageLump::~ImageLump()
{
    Clear();
}
void ImageLump::DeleteThis(void *pPtr)
{
    delete ((ImageLump *)pPtr);
}

///--[Constants]
const int ImageLump::cGLLookups[4] = {GL_COMPRESSED_RGB_S3TC_DXT1_EXT,  GL_COMPRESSED_RGBA_S3TC_DXT1_EXT,
                                      GL_COMPRESSED_RGBA_S3TC_DXT3_EXT, GL_COMPRESSED_RGBA_S3TC_DXT5_EXT};

///--[Statics]
uint8_t ImageLump::xCurrentCompression = ImageLump::cNoCompression;
bool ImageLump::xBlockTrimming = false;
char *ImageLump::xLastBitmapPath = NULL;
ALLEGRO_BITMAP *ImageLump::xLastBitmap = NULL;
char *ImageLump::xLastHitboxBitmapPath = NULL;
ALLEGRO_BITMAP *ImageLump::xLastHitboxBitmap = NULL;
bool ImageLump::xAllowSpecialAlphaTrimmer = false;
bool ImageLump::xAllowLineCompression = false;

//--Manual Rescaling
float ImageLump::xXScale = 1.0f;
float ImageLump::xYScale = 1.0f;
float ImageLump::xXScaleOffset = 0.0f;
float ImageLump::xYScaleOffset = 0.0f;

//--Debug Levels
uint8_t ImageLump::xDebugLevel = IL_DEBUG_NONE;

///===================================== Property Queries =========================================
uint8_t ImageLump::GetCompression()
{
    return mLocalCompression;
}

///======================================== Manipulators ==========================================
void ImageLump::SetCompression(int pFlag)
{
    mLocalCompression = pFlag;
}
void ImageLump::SetManualScales(float pXScale, float pYScale, float pXOffset, float pYOffset)
{
    xXScale = pXScale;
    xYScale = pYScale;
    xXScaleOffset = pXOffset;
    xYScaleOffset = pYOffset;
}

///======================================== Core Methods ==========================================
void ImageLump::Clear()
{
    //--Deallocate
    GLManager::TargetDisplay();
    al_destroy_bitmap(mRipBitmap);
    free(mDataArray);
    delete mHitboxesList;
    free(mDiffPath);

    //--System
    mLocalCompression = ImageLump::cInvalidCompression;

    //--Storage
    mDataWidth = 0;
    mDataHeight = 0;
    mTrueWidth = 0;
    mTrueHeight = 0;
    mXOffset = 0;
    mYOffset = 0;
    mDataArraySize = 0;
    mDataArray = NULL;
    mRipBitmap = NULL;

    //--Hitboxes
    mTotalHitboxes = 0;
    mHitboxesList = new StarlightLinkedList(true);

    //--Public Variables
    mDiffPath = NULL;
}
void ImageLump::LoadImage(const char *pPath, bool pIgnoreMasks)
{
    ///--[Documentation]
    //--Loads the image requested into static memory. We keep a static copy, since many rips will
    //  often occur from the same image twice, and deallocating between would be wasteful.
    if(!pPath) return;

    if(xLastBitmapPath && !strcasecmp(pPath, xLastBitmapPath) && xLastBitmap)
    {

    }
    //--New bitmap, scrap the old one.
    else
    {
        //--Scrap it
        if(xLastBitmap)
        {
            GLManager::TargetDisplay();
            al_destroy_bitmap(xLastBitmap);
            xLastBitmap = NULL;
        }

        //--Load the new one, copy the data.
        ResetString(xLastBitmapPath, pPath);
        xLastBitmap = al_load_bitmap(xLastBitmapPath);
        if(!xLastBitmap)
        {
            fprintf(stderr, "WARNING:  Couldn't load %s\n", pPath);
            return;
        }

        //--Remove the mask colors
        if(!pIgnoreMasks)
        {
            al_convert_mask_to_alpha(xLastBitmap, al_map_rgb( 64,  64,  64));
            al_convert_mask_to_alpha(xLastBitmap, al_map_rgb( 34, 177,  76));
            al_convert_mask_to_alpha(xLastBitmap, al_map_rgb(255,   0, 255));
        }
    }
}
void ImageLump::RootRip(const char *pPath, int pLft, int pTop, int pWid, int pHei, int pFlags)
{
    ///--[Documentation]
    //--Extracts the raw bitmap data from the provided file. What to actually *do* with said data
    //  is up to the compression routine.
    if(!pPath) return;
    Clear();
    DebugManager::PushPrint(false, "Ripping Image %s\n", pPath);

    //--Deallocate if there was a double call.
    if(mRipBitmap)
    {
        GLManager::TargetDisplay();
        al_destroy_bitmap(mRipBitmap);
        mRipBitmap = NULL;
        mDataArraySize = 0;
        free(mDataArray);
        mDataArray = NULL;
    }

    //--Load it.
    LoadImage(pPath, pFlags & SUGARCOLOR_IGNOREMASKS);
    if(!xLastBitmap) return;

    //--If negatives were passed for the Width or Height, auto-detect the total W/H of the image
    //  and rip the whole thing.
    if(pWid < 1)
    {
        pWid = al_get_bitmap_width(xLastBitmap);
    }
    if(pHei < 1)
    {
        pHei = al_get_bitmap_height(xLastBitmap);
    }

    //--Store how big the image was *supposed* to be.
    mTrueWidth = pWid;
    mTrueHeight = pHei;
    DebugManager::Print("True %i %i - ", mTrueWidth, mTrueHeight);

    //--Pad the edges to a multiple of 4 if using special compression.
    /*if(mLocalCompression >= ImageLump::cDXT1Compression && mLocalCompression <= cDXT5Compression)
    {
        if(pWid % 4 != 0)
        {
            pWid += 4 - (pWid % 4);
        }
        if(pHei % 4 != 0)
        {
            pHei += 4 - (pHei % 4);
        }
    }*/

    //--Create a storage bitmap, clear it off.
    al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP | ALLEGRO_MIN_LINEAR | ALLEGRO_NO_PREMULTIPLIED_ALPHA);
    mRipBitmap = al_create_bitmap(pWid, pHei);
    al_set_target_bitmap(mRipBitmap);
    al_clear_to_color(al_map_rgba(0, 0, 0, 0));

    //--Copy the region in question onto our rip bitmap.  We need to assemble flags first as Allegro
    //  may redefine these at some later date.
    int tFlags = 0;
    if(pFlags & ALLEGRO_FLIP_HORIZONTAL) tFlags |= 1;
    if(pFlags & ALLEGRO_FLIP_VERTICAL) tFlags |= 2;
    al_draw_bitmap_region(xLastBitmap, pLft, pTop, pWid, pHei, 0, 0, tFlags);
    DebugManager::Print("Left Top Coordinates %i %i - ", pLft, pTop);
    DebugManager::Print("Copied\n");

    //--If there's a diff case, handle that now.  If not, this routine returns immediately.
    HandleDiffCase(mDiffPath, pPath);

    //--If manual scaling is on, handle that now.  Do this before trimming, but after diff work.
    if(xXScale != 1.0f || xYScale != 1.0f || xXScaleOffset != 0.0f || xYScaleOffset != 0.0f)
    {
        //--Temporarily disable blending
        glEnable(GL_ALPHA_TEST);
        glAlphaFunc(GL_NOTEQUAL, 0.0f);
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO);

        ALLEGRO_BITMAP *tNewRipBitmap = al_create_bitmap(pWid, pHei);
        al_set_target_bitmap(tNewRipBitmap);
        al_clear_to_color(al_map_rgba(0, 0, 0, 0));
        al_draw_scaled_bitmap(mRipBitmap, 0, 0, pWid, pHei, xXScaleOffset, xYScaleOffset, pWid * xXScale, pHei * xYScale, tFlags);
        //al_draw_bitmap_region(mRipBitmap, pLft + xXScaleOffset, pTop + xYScaleOffset, pWid * xXScale, pHei * xYScale, 0, 0, tFlags);
        al_destroy_bitmap(mRipBitmap);
        mRipBitmap = tNewRipBitmap;

        //--Reset the blender.
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
    }

    //--Purge special alphas if needed.
    RemoveSpecialAlphas();

    //--Reset the static scalers.
    //xXScale = 1.0f;
    //xYScale = 1.0f;
    //xXScaleOffset = 0.0f;
    //xYScaleOffset = 0.0f;

    //--If the compression flag was not manually specified, use the automatically specified one
    //  in the static variable.
    if(mLocalCompression == ImageLump::cInvalidCompression)
    {
        mLocalCompression = ImageLump::xCurrentCompression;
    }
    DebugManager::Print("Compress Flag: %i\n", mLocalCompression);

    //--Trim off the edges.  This can be disabled with the xBlockTrimming flag.
    if(!ImageLump::xBlockTrimming)
    {
        TrimBitmap();
        DebugManager::Print("Pared %i %i\n", mDataWidth, mDataHeight);
    }
    //--Trimming was blocked, so store the offsets as 0.
    else
    {
        mDataWidth = mTrueWidth;
        mDataHeight = mTrueHeight;
        mXOffset = 0;
        mYOffset = 0;
    }
    DebugManager::Print("Trimmed\n");

    //--Pass the ripped section to the sub-calls.  They will know what to do with it, based on the
    //  compression flag.
    if(mLocalCompression == ImageLump::cNoCompression)
    {
        RipUncompressed(mRipBitmap);
    }
    else if(mLocalCompression == ImageLump::cPNGCompression)
    {
        RipCompressedPNG(mRipBitmap);
    }
    else if(mLocalCompression >= ImageLump::cDXT1Compression && mLocalCompression <= cDXT5Compression)
    {
        RipCompressedDXT(mRipBitmap, cGLLookups[mLocalCompression - ImageLump::cDXT1Compression]);
    }
    else if(mLocalCompression == cIsMonoColor)
    {
        //--This is not supposed to be accessed manually!
        RipUncompressed(mRipBitmap);
        //DebugManager::ForcePrint("%s: Please do not use code %i when compressing.  It will be automatic.\n", pPath, cIsMonoColor);
    }
    DebugManager::Print("Compressed\n");

    //--Check for errors.
    if(!mDataArray)
    {
        fprintf(stderr, "ImageLump:  Error, compression failed\n");
        mLocalCompression = ImageLump::cInvalidCompression;
    }
    DebugManager::Print("Passed Checks\n");

    //--Destroy the bitmap data itself.
    GLManager::TargetDisplay();
    al_destroy_bitmap(mRipBitmap);
    mRipBitmap = NULL;
    DebugManager::PopPrint("Done image rip\n");
}
void ImageLump::RootRipData(const char *pPath, int pLft, int pTop, int pWid, int pHei, int pFlags)
{
    ///--[Documentation]
    //--A specialized version of the RootRip. In this version, the image data is not ripped but everything else is.
    //  The program will record the true width and height, and the X/Y offset, but not the image pixel data.
    //--This is used for positioner images that don't care about an image's content. For example, when placing text
    //  on a graphic, we can use a black square to indicate where the text goes. There is no need to capture the square
    //  itself, just where it is on the image.
    if(!pPath) return;
    Clear();
    DebugManager::PushPrint(false, "Ripping Image %s\n", pPath);

    //--Deallocate if there was a double call.
    if(mRipBitmap)
    {
        GLManager::TargetDisplay();
        al_destroy_bitmap(mRipBitmap);
        mRipBitmap = NULL;
        mDataArraySize = 0;
        free(mDataArray);
        mDataArray = NULL;
    }

    //--Load it.
    LoadImage(pPath, pFlags & SUGARCOLOR_IGNOREMASKS);
    if(!xLastBitmap) return;

    //--If negatives were passed for the Width or Height, auto-detect the total W/H of the image
    //  and rip the whole thing.
    if(pWid < 1)
    {
        pWid = al_get_bitmap_width(xLastBitmap);
    }
    if(pHei < 1)
    {
        pHei = al_get_bitmap_height(xLastBitmap);
    }

    //--Store how big the image was *supposed* to be.
    mTrueWidth = pWid;
    mTrueHeight = pHei;
    DebugManager::Print("True %i %i - ", mTrueWidth, mTrueHeight);

    //--Create a storage bitmap, clear it off.
    al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP | ALLEGRO_MIN_LINEAR | ALLEGRO_NO_PREMULTIPLIED_ALPHA);
    mRipBitmap = al_create_bitmap(pWid, pHei);
    al_set_target_bitmap(mRipBitmap);
    al_clear_to_color(al_map_rgba(0, 0, 0, 0));

    //--Copy the region in question onto our rip bitmap. We need to assemble flags first as Allegro
    //  may redefine these at some later date.
    int tFlags = 0;
    if(pFlags & ALLEGRO_FLIP_HORIZONTAL) tFlags |= 1;
    if(pFlags & ALLEGRO_FLIP_VERTICAL)   tFlags |= 2;
    al_draw_bitmap_region(xLastBitmap, pLft, pTop, pWid, pHei, 0, 0, tFlags);
    DebugManager::Print("Left Top Coordinates %i %i - ", pLft, pTop);
    DebugManager::Print("Copied\n");

    //--If there's a diff case, handle that now. If not, this routine returns immediately.
    HandleDiffCase(mDiffPath, pPath);

    //--If manual scaling is on, handle that now. Do this before trimming, but after diff work.
    if(xXScale != 1.0f || xYScale != 1.0f || xXScaleOffset != 0.0f || xYScaleOffset != 0.0f)
    {
        //--Temporarily disable blending
        glEnable(GL_ALPHA_TEST);
        glAlphaFunc(GL_NOTEQUAL, 0.0f);
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO);

        ALLEGRO_BITMAP *tNewRipBitmap = al_create_bitmap(pWid, pHei);
        al_set_target_bitmap(tNewRipBitmap);
        al_clear_to_color(al_map_rgba(0, 0, 0, 0));
        al_draw_scaled_bitmap(mRipBitmap, 0, 0, pWid, pHei, xXScaleOffset, xYScaleOffset, pWid * xXScale, pHei * xYScale, tFlags);
        //al_draw_bitmap_region(mRipBitmap, pLft + xXScaleOffset, pTop + xYScaleOffset, pWid * xXScale, pHei * xYScale, 0, 0, tFlags);
        al_destroy_bitmap(mRipBitmap);
        mRipBitmap = tNewRipBitmap;

        //--Reset the blender.
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
    }

    //--Purge special alphas if needed.
    RemoveSpecialAlphas();

    //--Reset the static scalers.
    xXScale = 1.0f;
    xYScale = 1.0f;
    xXScaleOffset = 0.0f;
    xYScaleOffset = 0.0f;

    //--If the compression flag was not manually specified, use the automatically specified one
    //  in the static variable.
    if(mLocalCompression == ImageLump::cInvalidCompression)
    {
        mLocalCompression = ImageLump::xCurrentCompression;
    }
    DebugManager::Print("Compress Flag: %i\n", mLocalCompression);

    //--Trim off the edges.  This can be disabled with the xBlockTrimming flag.
    if(!ImageLump::xBlockTrimming)
    {
        TrimBitmap();
        DebugManager::Print("Pared %i %i\n", mDataWidth, mDataHeight);
    }
    //--Trimming was blocked, so store the offsets as 0.
    else
    {
        mDataWidth = mTrueWidth;
        mDataHeight = mTrueHeight;
        mXOffset = 0;
        mYOffset = 0;
    }
    DebugManager::Print("Trimmed\n");

    //--Now store only a single pixel of visual data. Declare the data width and height to be 1x1
    //  regardless of what they should be.
    mDataWidth = 1;
    mDataHeight = 1;
    mDataArraySize = (sizeof(uint8_t) * (mDataWidth * mDataHeight * 4));
    mDataArray = (uint8_t *)malloc(mDataArraySize);
    mDataArray[0] = 255;
    mDataArray[1] = 255;
    mDataArray[2] = 255;
    mDataArray[3] = 255;

    //--Destroy the bitmap data itself.
    GLManager::TargetDisplay();
    al_destroy_bitmap(mRipBitmap);
    mRipBitmap = NULL;
    DebugManager::PopPrint("Done image rip\n");
}
void ImageLump::AddHitbox(const char *pPath, int pLft, int pTop, int pWid, int pHei, const char *pChannel)
{
    ///--[Documentation]
    //--Adds a hitbox to the currently active Image lump. Hitboxes can't be deleted once added.
    if(!pPath || !pChannel) return;

    //--Same bitmap, do nothing.
    if(xLastHitboxBitmapPath && !strcasecmp(pPath, xLastHitboxBitmapPath) && xLastHitboxBitmap)
    {

    }
    //--New bitmap, scrap the old one.
    else
    {
        //--Scrap it
        if(xLastHitboxBitmap)
        {
            GLManager::TargetDisplay();
            al_destroy_bitmap(xLastHitboxBitmap);
            xLastHitboxBitmap = NULL;
        }

        //--Load the new one, copy the data.
        ResetString(xLastHitboxBitmapPath, pPath);
        xLastHitboxBitmap = al_load_bitmap(xLastHitboxBitmapPath);
        if(!xLastHitboxBitmap)
        {
            fprintf(stderr, "WARNING:  Couldn't load %s\n", pPath);
            return;
        }

        //--Remove the mask colors
        al_convert_mask_to_alpha(xLastHitboxBitmap, al_map_rgb( 64,  64,  64));
        al_convert_mask_to_alpha(xLastHitboxBitmap, al_map_rgb( 34, 177,  76));
        al_convert_mask_to_alpha(xLastHitboxBitmap, al_map_rgb(255,   0, 255));
    }
    if(!xLastHitboxBitmap) return;

    //--Resolve the mask.
    uint8_t tMask = 0x00;
    if(!strcmp(pChannel, "R")) tMask = 0x01;
    if(!strcmp(pChannel, "G")) tMask = 0x02;
    if(!strcmp(pChannel, "B")) tMask = 0x04;

    //--Setup
    int tLft = pWid+1;
    int tTop = pHei+1;
    int tRgt = -1;
    int tBot = -1;

    //--Now down to business. Checking across the provided area, look for pixels in the specific
    //  channel. Getting pixels matching that mask, find the extent.
    uint8_t tColorData;
    ALLEGRO_COLOR tReadColor;
    for(int x = 0; x < pWid; x ++)
    {
        for(int y = 0; y < pHei; y ++)
        {
            //--Resolve whether or not this is a usable pixel.
            tReadColor = al_get_pixel(xLastHitboxBitmap, x + pLft, y + pTop);
            tColorData = 0;
            if(tReadColor.a != 0)
            {
                if(tReadColor.r != 0) tColorData |= 0x01;
                if(tReadColor.g != 0) tColorData |= 0x02;
                if(tReadColor.b != 0) tColorData |= 0x04;
            }
            tColorData &= tMask;
            if(!tColorData) continue;

            //--If it's usable, check it against the existing rectangle.
            if(tLft > x) tLft = x;
            if(tRgt < x) tRgt = x;
            if(tTop > y) tTop = y;
            if(tBot < y) tBot = y;
        }
    }

    //--Save the data into the image lump, if it was non-neutral.
    if(tLft == pWid+1 || tTop == pHei+1 || tRgt == -1 || tBot == -1) return;
    HitboxPiece *nPiece = (HitboxPiece *)malloc(sizeof(HitboxPiece));
    nPiece->mLft = tLft;
    nPiece->mTop = tTop;
    nPiece->mRgt = tRgt;
    nPiece->mBot = tBot;
    mHitboxesList->AddElement("X", nPiece, &FreeThis);
    //fprintf(stderr, "%s Located hitbox at %i %i to %i %i - %i %i\n", pPath, pLft, pTop, tLft, tTop, tRgt, tBot);
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void ImageLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    ///--[Documentation]
    //--Writes all the data of the ImageLump. This Lump contains a header, as it must indicate the
    //  compression type of the data it stores.
    if(!pOutfile) return;

    ///--[Header]
    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "IMAGE00000";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    ///--[Compression and Position Data]
    //--Make sure the compression type is valid.  If it isn't, write a FAIL compression code, and
    //  no data.
    if(mLocalCompression >= cSupportedCompressions || mLocalCompression == ImageLump::cInvalidCompression)
    {
        uint8_t tVal = ImageLump::cInvalidCompression;
        fwrite(&tVal, sizeof(uint8_t), 1, pOutfile);
        sCounter += (sizeof(uint8_t) * 1);
        return;
    }

    //--Write the compression type
    fwrite(&mLocalCompression, sizeof(uint8_t), 1, pOutfile);
    sCounter += (sizeof(uint8_t) * 1);

    //--Write the offset data.
    fwrite(&mTrueWidth,  sizeof(int32_t), 1, pOutfile);
    fwrite(&mTrueHeight, sizeof(int32_t), 1, pOutfile);
    fwrite(&mXOffset,    sizeof(int32_t), 1, pOutfile);
    fwrite(&mYOffset,    sizeof(int32_t), 1, pOutfile);
    fwrite(&mDataWidth,  sizeof(int32_t), 1, pOutfile);
    fwrite(&mDataHeight, sizeof(int32_t), 1, pOutfile);
    sCounter += (sizeof(int32_t) * 6);

    ///--[Pixel Data]
    //--Write the length of the data
    fwrite(&mDataArraySize, sizeof(uint32_t), 1, pOutfile);
    sCounter += (sizeof(uint32_t) * 1);

    //--Write the data itself.
    fwrite(mDataArray, sizeof(uint8_t), mDataArraySize, pOutfile);
    sCounter += (sizeof(uint8_t) * mDataArraySize);

    ///--[Hitboxes]
    //--Write the number of hitboxes.  Can be zero.
    uint8_t tHitboxesTotal = mHitboxesList->GetListSize();
    fwrite(&tHitboxesTotal, sizeof(uint8_t), 1, pOutfile);
    sCounter += (sizeof(uint8_t) * 1);

    //--Write all the hitboxes.
    HitboxPiece *rPiece = (HitboxPiece *)mHitboxesList->PushIterator();
    while(rPiece)
    {
        fwrite(rPiece, sizeof(HitboxPiece), 1, pOutfile);
        sCounter += (sizeof(HitboxPiece) * 1);
        rPiece = (HitboxPiece *)mHitboxesList->AutoIterate();
    }
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void ImageLump::HookToLuaState(lua_State *pLuaState)
{
    /* ImageLump_SetDebugLevel(iFlag)
       Sets what debug information is printed to stderr. */
    lua_register(pLuaState, "ImageLump_SetDebugLevel", &Hook_ImageLump_SetDebugLevel);

    /* ImageLump_SetCompression(iFlag)
       Sets the compression to be used from now on.  Lumps already created will not have their
       compression type changed.  If this is NOT called, then all ImageLumps will be of type Bitmap
       which is very large but also incredibly easy to read. */
    lua_register(pLuaState, "ImageLump_SetCompression", &Hook_ImageLump_SetCompression);

    /* ImageLump_SetScales(fXScale, fYScale, fXOffset, fYOffset)
       Sets the scales that will take place on the next rip operation.  They are reset to defaults
       after each rip, and need to be set again if two images use the same scaling. */
    lua_register(pLuaState, "ImageLump_SetScales", &Hook_ImageLump_SetScales);

    /* ImageLump_Rip(sName[], sImage[], iLft, iTop, iWid, iHei, iFlipFlag)
       Creates a new ImageLump with the given name, by taking the pixel data from the specified
       image.  The location is a subregion, pass -1 for the width and height to use the full w/h
       of the image.  iFlipFlag can be 0, 1 (HFlip), 2 (VFlip), or 3 (HVFlip). */
    lua_register(pLuaState, "ImageLump_Rip", &Hook_ImageLump_Rip);

    /* ImageLump_RipData(sName, sImage, iLft, iTop, iWid, iHei, iFlipFlag)
       Creates a new ImageLump with the given name but does not rip any image data. Only the position, offsets, and
       sizes are stored. The image data is always given a width/height of 1x1.*/
    lua_register(pLuaState, "ImageLump_RipData", &Hook_ImageLump_Rip);

    /* ImageLump_RipDiff(sName[], sImageBase[], sImageNew[])
       Creates a new ImageLump with the given name, by taking pixel data from sImageNew and removing
       all pixels which are identical to sImageBase.  This typically removes a lot of data from
       animations where a character is stationary with a few exceptions.
       For obvious reasons, positioners are not provided.  Both images must be the same sizes and
       dimensions.  */
    lua_register(pLuaState, "ImageLump_RipDiff", &Hook_ImageLump_RipDiff);

    /* ImageLump_AddHitbox(sImage[], iLft, iTop, iWid, iHei, sChannel[])
       To the currently active ImageLump, adds a hitbox by scanning the requested image.  The coords
       will ideally match the Rip's coords.  sChannel should be one of "R", "G", or "B". */
    lua_register(pLuaState, "ImageLump_AddHitbox", &Hook_ImageLump_AddHitbox);

    /* ImageLump_SetSpecialAlphaFlag(bFlag)
       Some programs may have special flags set in the alpha channel of images that are being given
       to them.  In these cases, turn this flag on.  It costs extra processing time as each alpha
       special case must be manually checked and removed.
       Turning DXT compression off definitely helps, as the JPEG style may cause blending errors!
       Note:  Turn this flag to false when using the RipDiff function, as the RipDiff will automaticall
       correct special alphas. */
    lua_register(pLuaState, "ImageLump_SetSpecialAlphaFlag", &Hook_ImageLump_SetSpecialAlphaFlag);

    /* ImageLump_SetBlockTrimmingFlag(bFlag)
       If this flag is true, the image will not trim off transparent space to save hard-drive space. */
    lua_register(pLuaState, "ImageLump_SetBlockTrimmingFlag", &Hook_ImageLump_SetBlockTrimmingFlag);

    /* ImageLump_SetAllowLineCompression(bFlag)
       Allows the program to attempt line compression. This takes longer but can save space on bitmaps
       with long sequences of transparent pixels. If the compression would not save space, it will
       not be used. */
    lua_register(pLuaState, "ImageLump_SetAllowLineCompression", &Hook_ImageLump_SetAllowLineCompression);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_ImageLump_SetDebugLevel(lua_State *L)
{
    //ImageLump_SetDebugLevel(iFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("ImageLump_SetDebugLevel");

    ImageLump::xDebugLevel = lua_tointeger(L, 1);

    return 0;
}
int Hook_ImageLump_SetCompression(lua_State *L)
{
    //ImageLump_SetCompression(iFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("ImageLump_SetCompression");

    ImageLump::xCurrentCompression = lua_tointeger(L, 1);

    return 0;
}
int Hook_ImageLump_SetScales(lua_State *L)
{
    //ImageLump_SetScales(fXScale, fYScale, fXOffset, fYOffset)
    int pArgs = lua_gettop(L);
    if(pArgs != 4) return LuaArgError("ImageLump_SetCompression");

    ImageLump::SetManualScales(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));

    return 0;
}
int Hook_ImageLump_Rip(lua_State *L)
{
    //ImageLump_Rip(sName[], sImage[], iLft, iTop, iWid, iHei, iFlipFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 7) return LuaArgError("ImageLump_Rip");

    //--Create the Lump, set it as the ActiveObject;
    ImageLump *nLump = new ImageLump();
    DataLibrary::Fetch()->PushActiveEntity(nLump);

    //--Register it to the currently active SLF.
    SugarLumpFile *rFile = SugarLumpFile::Fetch();
    if(!rFile) return fprintf(stderr, "ImageLump_Rip:  Failed, no SLF open\n");
    rFile->RegisterLump(lua_tostring(L, 1), nLump, &ImageLump::DeleteThis);

    //--Call the routine
    nLump->RootRip(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7));

    return 0;
}
int Hook_ImageLump_RipData(lua_State *L)
{
    //ImageLump_RipData(sName, sImage, iLft, iTop, iWid, iHei, iFlipFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 7) return LuaArgError("ImageLump_RipData");

    //--Create the Lump, set it as the ActiveObject;
    ImageLump *nLump = new ImageLump();
    DataLibrary::Fetch()->PushActiveEntity(nLump);

    //--Register it to the currently active SLF.
    SugarLumpFile *rFile = SugarLumpFile::Fetch();
    if(!rFile) return fprintf(stderr, "ImageLump_Rip:  Failed, no SLF open\n");
    rFile->RegisterLump(lua_tostring(L, 1), nLump, &ImageLump::DeleteThis);

    //--Call the routine
    nLump->RootRipData(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7));

    return 0;
}
int Hook_ImageLump_RipDiff(lua_State *L)
{
    //ImageLump_RipDiff(sName[], sImageBase[], sImageNew[], iLft, iTop, iWid, iHei, iFlipFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 8) return LuaArgError("ImageLump_RipDiff");

    //--Create the Lump, set it as the ActiveObject;
    ImageLump *nLump = new ImageLump();
    DataLibrary::Fetch()->PushActiveEntity(nLump);

    //--Register it to the currently active SLF.
    SugarLumpFile *rFile = SugarLumpFile::Fetch();
    if(!rFile) return fprintf(stderr, "ImageLump_RipDiff:  Failed, no SLF open\n");
    rFile->RegisterLump(lua_tostring(L, 1), nLump, &ImageLump::DeleteThis);

    //--Call the routine
    ResetString(nLump->mDiffPath, lua_tostring(L, 2));
    nLump->RootRip(lua_tostring(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7), lua_tointeger(L, 8));

    return 0;
}
int Hook_ImageLump_AddHitbox(lua_State *L)
{
    //ImageLump_AddHitbox(sImage[], iLft, iTop, iWid, iHei, sChannel[])
    int pArgs = lua_gettop(L);
    if(pArgs != 6) return LuaArgError("ImageLump_AddHitbox");

    //--Get the Active ImageLump.
    ImageLump *rLump = (ImageLump *)DataLibrary::Fetch()->rActiveObject;
    if(!rLump) return LuaTypeError("ImageLump_AddHitbox");

    //--Run!
    rLump->AddHitbox(lua_tostring(L, 1), lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tostring(L, 6));
    return 0;
}
int Hook_ImageLump_SetSpecialAlphaFlag(lua_State *L)
{
    //ImageLump_SetSpecialAlphaFlag(bFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("ImageLump_SetSpecialAlphaFlag");

    //--Set the flag.
    ImageLump::xAllowSpecialAlphaTrimmer = lua_toboolean(L, 1);
    return 0;
}
int Hook_ImageLump_SetBlockTrimmingFlag(lua_State *L)
{
    //ImageLump_SetBlockTrimmingFlag(bFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("ImageLump_SetBlockTrimmingFlag");

    //--Set the flag.
    ImageLump::xBlockTrimming = lua_toboolean(L, 1);
    return 0;
}
int Hook_ImageLump_SetAllowLineCompression(lua_State *L)
{
    //ImageLump_SetAllowLineCompression(bFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("ImageLump_SetAllowLineCompression");

    //--Set the flag.
    ImageLump::xAllowLineCompression = lua_toboolean(L, 1);
    return 0;

}
