///========================================= ImageLump ============================================
//--A lump representing an Image in memory.  The ImageLump may be uncompressed or may have one of
//  several compression types in place, which are based on an internal flag.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLump.h"

///===================================== Local Structures =========================================
//--Used to represent a hitbox. Hitboxes can be compressed along with image data to make editing
//  them much easier.
typedef struct
{
    uint16_t mLft;
    uint16_t mTop;
    uint16_t mRgt;
    uint16_t mBot;
}HitboxPiece;

//--Info used for by-line compression algorithms.
#include "StarlightLinkedList.h"
typedef struct CompressionInfo
{
    //--Members
    uint32_t mExpectedFileSize;
    StarlightLinkedList *mDataList;

    //--Functions
    static void DeleteThis(void *pPtr)
    {
        CompressionInfo *rPtr = (CompressionInfo *)pPtr;
        delete rPtr->mDataList;
        free(rPtr);
    }
}CompressionInfo;

///===================================== Local Definitions ========================================
//--Masking Flags
#define SUGARCOLOR_IGNOREMASKS 0x10

//--Debug Flags
#define IL_DEBUG_NONE 0x00
#define IL_DEBUG_SIZES 0x01
#define IL_DEBUG_POSITIONS 0x02

///========================================== Classes =============================================
class ImageLump : public RootLump
{
    ///--[Constants]
    public:
    static const uint8_t cInvalidCompression = 0;
    static const uint8_t cNoCompression = 1;
    static const uint8_t cPNGCompression = 2;
    static const uint8_t cDXT1Compression = 3;
    static const uint8_t cDXT1CompressionWithAlpha = 4;
    static const uint8_t cDXT3Compression = 5;
    static const uint8_t cDXT5Compression = 6;
    static const uint8_t cIsMonoColor = 7;
    static const uint8_t cHorizontalLineCompression = 8;
    static const uint8_t cVeritcalLineCompression = 9;
    static const uint8_t cSupportedCompressions = 10;
    static const int cGLLookups[4];

    ///--[Variables]
    private:
    //--System
    uint8_t mLocalCompression;

    //--Storage
    int mDataWidth;
    int mDataHeight;
    int mTrueWidth;
    int mTrueHeight;
    int mXOffset;
    int mYOffset;
    uint32_t mDataArraySize;
    uint8_t *mDataArray;
    ALLEGRO_BITMAP *mRipBitmap;

    //--Hitboxes
    int mTotalHitboxes;
    StarlightLinkedList *mHitboxesList;

    //--Manual Rescaling
    static float xXScale;
    static float xYScale;
    static float xXScaleOffset;
    static float xYScaleOffset;

    protected:

    public:
    //--System
    ImageLump();
    virtual ~ImageLump();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    static uint8_t xCurrentCompression;
    static bool xBlockTrimming;
    static bool xAllowSpecialAlphaTrimmer;
    static bool xAllowLineCompression;
    static uint8_t xDebugLevel;
    char *mDiffPath;

    //--Bitmap ripping static
    static char *xLastBitmapPath;
    static ALLEGRO_BITMAP *xLastBitmap;
    static char *xLastHitboxBitmapPath;
    static ALLEGRO_BITMAP *xLastHitboxBitmap;

    //--Property Queries
    uint8_t GetCompression();

    //--Manipulators
    void SetCompression(int pFlag);
    static void SetManualScales(float pXScale, float pYScale, float pXOffset, float pYOffset);

    //--Core Methods
    void Clear();
    static void LoadImage(const char *pPath, bool pIgnoreMasks);
    void RootRip(const char *pPath, int pLft, int pTop, int pWid, int pHei, int pFlags);
    void RootRipData(const char *pPath, int pLft, int pTop, int pWid, int pHei, int pFlags);
    void HandleDiffCase(const char *pPath, const char *pLoadPath);
    void RemoveSpecialAlphas();
    void AddHitbox(const char *pPath, int pLft, int pTop, int pWid, int pHei, const char *pChannel);

    //--Compression Sub-types
    void RipUncompressed(ALLEGRO_BITMAP *pDataMap);
    void RipCompressedPNG(ALLEGRO_BITMAP *pDataMap);
    void RipCompressedDXT(ALLEGRO_BITMAP *pDataMap, int pCompressionFormat);
    bool RipCompressedByLine(ALLEGRO_BITMAP *pDataMap);
    CompressionInfo *ScanHorizontal(ALLEGRO_BITMAP *pDataMap);
    CompressionInfo *ScanVertical(ALLEGRO_BITMAP *pDataMap);

    //--Trimming
    void TrimBitmap();

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_ImageLump_SetDebugLevel(lua_State *L);
int Hook_ImageLump_SetCompression(lua_State *L);
int Hook_ImageLump_SetScales(lua_State *L);
int Hook_ImageLump_Rip(lua_State *L);
int Hook_ImageLump_RipData(lua_State *L);
int Hook_ImageLump_RipDiff(lua_State *L);
int Hook_ImageLump_AddHitbox(lua_State *L);
int Hook_ImageLump_SetSpecialAlphaFlag(lua_State *L);
int Hook_ImageLump_SetBlockTrimmingFlag(lua_State *L);
int Hook_ImageLump_SetAllowLineCompression(lua_State *L);
