///======================================= SugarLumpFile ==========================================
//--Represents a lump file in memory, which is to be dumped to the hard disk when the lua file
//  tells it to. The program does not support two concurrent SugarLumpFiles, all operations are
//  done on the global file.
//--Note: "Chunk" and "Lump" are interchangably used here.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define SLF_DEBUG_NONE 0x00
#define SLF_DEBUG_STATUS 0x01
#define SLF_DEBUG_LUMPINFO 0x02
#define SLF_DEBUG_AUTOQUERY 0x04

///========================================== Classes =============================================
class SugarLumpFile
{
    private:
    //--System
    static const int cFileVersion = 100;
    char *mHeader;

    //--Lumps
    StarlightLinkedList *mLumpList; //RootLump *, master

    //--Auto-Load Lumps
    StarlightLinkedList *mAutoLoadLumps; //AutoLoadLump *, master

    protected:

    public:
    //--System
    SugarLumpFile();
    ~SugarLumpFile();

    //--Public Variables
    char *mWritePath;
    static uint8_t xDebugLevel;
    static StarlightLinkedList *xAutoLoadQuery; //char *, master

    //--Property Queries
    AutoLoadLump *GetLump(const char *pName);

    //--Manipulators
    void CreateAutoLoadLump(const char *pLumpName);
    void RegisterAutoLoadName(const char *pAutoLoadLump, const char *pBitmapName, const char *pDLPath);
    void RegisterLump(const char *pName, RootLump *pLump, DeletionFunctionPtr pDelPtr);

    //--Core Methods
    //--Update
    //--File I/O
    void Write(const char *pPath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static SugarLumpFile *Fetch();
    static void QueryAutoLoadFromFile(const char *pFilePath, const char *pAutoLoadName);
    static int GetQueryCount();
    static const char *GetQueryName(int pSlot);
    static const char *GetQueryPath(int pSlot);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SLF_SetDebugLevel(lua_State *L);
int Hook_SLF_Open(lua_State *L);
int Hook_SLF_RegisterAutoLoadLump(lua_State *L);
int Hook_SLF_Close(lua_State *L);
int Hook_SLF_QueryAutoLoadInFile(lua_State *L);
int Hook_SLF_GetQueryCount(lua_State *L);
int Hook_SLF_GetQueryEntry(lua_State *L);
