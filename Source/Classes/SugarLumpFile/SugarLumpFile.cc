//--Base
#include "SugarLumpFile.h"

//--Classes
#include "AutoLoadLump.h"
#include "RootLump.h"

//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"
#include "StringMacros.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///========================================== System ==============================================
SugarLumpFile::SugarLumpFile()
{
    //--[SugarLumpFile]
    //--System
    mHeader = (char *)malloc(sizeof(char) * (strlen("SugarLumpFile100") + 1));
    strcpy(mHeader, "SugarLumpFile100");
    mHeader[13] = ((SugarLumpFile::cFileVersion / 100) % 10) + '0';
    mHeader[14] = ((SugarLumpFile::cFileVersion /  10) % 10) + '0';
    mHeader[15] = ((SugarLumpFile::cFileVersion /   1) % 10) + '0';

    //--Chunks
    mLumpList = new StarlightLinkedList(true);

    //--Special: AutoLoadLump.
    mAutoLoadLumps = new StarlightLinkedList(true);

    //--Public Variables
    mWritePath = NULL;
}
SugarLumpFile::~SugarLumpFile()
{
    delete mLumpList;
    free(mWritePath);
    delete mAutoLoadLumps;
}

///--[Public Statics]
uint8_t SugarLumpFile::xDebugLevel = SLF_DEBUG_NONE;
StarlightLinkedList *SugarLumpFile::xAutoLoadQuery = new StarlightLinkedList(true);

///===================================== Property Queries =========================================
AutoLoadLump *SugarLumpFile::GetLump(const char *pName)
{
    if(!pName) return NULL;
    return (AutoLoadLump *)mAutoLoadLumps->GetElementByName(pName);
}

///======================================== Manipulators ==========================================
void SugarLumpFile::CreateAutoLoadLump(const char *pLumpName)
{
    //--Duplicate check:
    void *rCheckPtr = mAutoLoadLumps->GetElementByName(pLumpName);
    if(rCheckPtr) return;

    //--Create.
    AutoLoadLump *nNewLump = new AutoLoadLump();
    mAutoLoadLumps->AddElement(pLumpName, nNewLump, &RootLump::DeleteThis);
}
void SugarLumpFile::RegisterAutoLoadName(const char *pAutoLoadLump, const char *pBitmapName, const char *pDLPath)
{
    //--Locate the auto-load lump.
    AutoLoadLump *rAutoLoadLump = (AutoLoadLump *)mAutoLoadLumps->GetElementByName(pAutoLoadLump);
    if(!rAutoLoadLump)
    {
        DebugManager::ForcePrint("Warning: AutoLoadLump %s not found. Failing.\n", pAutoLoadLump);
        return;
    }

    //--Register.
    rAutoLoadLump->RegisterLookup(pBitmapName, pDLPath);
}
void SugarLumpFile::RegisterLump(const char *pName, RootLump *pLump, DeletionFunctionPtr pDelPtr)
{
    ///--[Documentation]
    //--Registers the Lump to the file. Lumps do not contain internal names, their name is whatever
    //  they are on the SLF's list.
    //--Don't use this for auto-load lumps. Those are handled with another function.
    if(!pName || !pLump) return;
    mLumpList->AddElement(pName, pLump, pDelPtr);
}

///======================================== Core Methods ==========================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
void SugarLumpFile::Write(const char *pPath)
{
    ///--[Documentation]
    //--Write the SugarLumpFile to the provided path. Most of the hard logic is done by the Lumps
    //  themselves, in overridden functions.
    //--Note: Overwrite is ON!
    if(!pPath) return;

    ///--[Autoload Handler]
    //--Special: If the AutoLoadLump has information, and it's not on the list yet, put it there.
    AutoLoadLump *rAutoLump = (AutoLoadLump *)mAutoLoadLumps->PushIterator();
    while(rAutoLump)
    {
        //--Cross-register.
        RegisterLump(mAutoLoadLumps->GetIteratorName(), rAutoLump, &RootLump::DeleteThis);

        //--Next.
        rAutoLump = (AutoLoadLump *)mAutoLoadLumps->AutoIterate();
    }

    //--Clean up the autolump handlers.
    mAutoLoadLumps->SetDeallocation(false);
    mAutoLoadLumps->ClearList();

    ///--[Open, Headers]
    //--Open and check for write protection
    FILE *fOutfile = fopen(pPath, "wb");
    if(!fOutfile)
    {
        fprintf(stderr, "SugarLumpFile - Error opening %s for writing\n", pPath);
        return;
    }

    //--Debug.
    if(SugarLumpFile::xDebugLevel & SLF_DEBUG_STATUS)
    {
        fprintf(stderr, "SugarLumpFile - Writing Lumps.\n");
        fprintf(stderr, " Path: %s\n", pPath);
        fprintf(stderr, " Lumps: %i\n", mLumpList->GetListSize());
    }

    //--Write the header to the file.
    uint64_t tFileCursor = 0;
    fwrite(mHeader, sizeof(char), 16, fOutfile);
    tFileCursor += (sizeof(char) * 16);

    //--Write total number of Lumps.
    uint32_t tTotalChunks = mLumpList->GetListSize();
    fwrite(&tTotalChunks, sizeof(uint32_t), 1, fOutfile);
    tFileCursor += (sizeof(uint32_t) * 1);

    ///--[Lump Writing]
    //--Sort the Lumps by name.
    mLumpList->SortList(false);

    //--Write the name of each Lump, and leave space for its 64-bit address in the file.
    uint64_t tDummy = 0;
    RootLump *rLump = (RootLump *)mLumpList->PushIterator();
    DebugManager::PushPrint(SugarLumpFile::xDebugLevel & SLF_DEBUG_LUMPINFO, "Lump names:\n", pPath);
    while(rLump)
    {
        //--Get the name
        const char *rName = mLumpList->GetIteratorName();
        uint8_t tLen = strlen(rName);

        //--Write it!
        fwrite(&tLen, sizeof(uint8_t), 1, fOutfile);
        fwrite(rName, sizeof(char), tLen, fOutfile);
        tFileCursor += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);
        DebugManager::Print("%s \n", rName);

        //--Write a blank 64-bit integer, and tell the Lump where it is.
        rLump->SetAddressOfAddress(tFileCursor);
        fwrite(&tDummy, sizeof(uint64_t), 1, fOutfile);
        tFileCursor += (sizeof(uint64_t) * 1);

        //--Next!
        rLump = (RootLump *)mLumpList->AutoIterate();
    }
    DebugManager::PopPrint("End of name list.\n");

    //--Write the Lumps to the file.
    rLump = (RootLump *)mLumpList->PushIterator();
    while(rLump)
    {
        //--Lump stores the address of its... address... internally.
        rLump->WriteTo(tFileCursor, fOutfile);

        //--Next!
        rLump = (RootLump *)mLumpList->AutoIterate();
    }

    //--Debug.
    if(SugarLumpFile::xDebugLevel & SLF_DEBUG_STATUS)
    {
        fprintf(stderr, " Size: %i\n", (int)tFileCursor);
    }

    //--Go back and write the addresses in the header.
    rLump = (RootLump *)mLumpList->PushIterator();
    while(rLump)
    {
        //--Lump stores the address of its... address... internally.
        rLump->WriteAddress(fOutfile);

        //--Next!
        rLump = (RootLump *)mLumpList->AutoIterate();
    }

    ///--[Finish Up]
    //--Close and cleanup.
    fclose(fOutfile);

    //--Debug.
    if(SugarLumpFile::xDebugLevel & SLF_DEBUG_STATUS)
    {
        fprintf(stderr, "Finished writing.\n");
    }

}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
SugarLumpFile *SugarLumpFile::Fetch()
{
    //--Warning:  Can pass back NULL.  You were warned.
    return Global::Shared()->gActiveLumpFile;
}
void SugarLumpFile::QueryAutoLoadFromFile(const char *pFilePath, const char *pAutoLoadName)
{
    ///--[Documentation]
    //--Given a file path, opens the file as a standard SLF file, and locates the AutoLoad lump with
    //  the given name. Then, takes that lump's data and stores it for late querying. This query explicitly
    //  survives opening and closing SLF files.
    //--This is used to clone autoload data from already created SLF files, allowing "meta" autoloads
    //  to efficiently load from many files, even if those files were not created during this compression
    //  cycle (such as if no changes were made to those files, but the autoloader needs them).
    //--Regardless of the file/lump existing, always clears the existing query information.
    xAutoLoadQuery->ClearList();
    if(!pFilePath || !pAutoLoadName) return;

    //--Debug.
    if(SugarLumpFile::xDebugLevel & SLF_DEBUG_AUTOQUERY)
    {
        fprintf(stderr, "Querying autoload from file: %s %s\n", pFilePath, pAutoLoadName);
    }

    //--Setup.
    char tBuffer[1024];

    ///--[File Handling]
    FILE *fInfile = fopen(pFilePath, "rb");
    if(!fInfile) return;

    //--Check header.
    fread(tBuffer, sizeof(char), 16, fInfile);
    tBuffer[16] = '\0';
    if(strcasecmp(tBuffer, "SugarLumpFile100"))
    {
        fprintf(stderr, "Error, invalid header: SugarLumpFile100 vs. %s\n", tBuffer);
    }

    ///--[Directory]
    //--Read how many entries there are.
    uint32_t tTotalLumps = 0;
    fread(&tTotalLumps, sizeof(uint32_t), 1, fInfile);
    if(tTotalLumps < 1) { fclose(fInfile); return; }

    //--Allocate space.
    char **tNames        = (char **)   malloc(sizeof(char *)   * tTotalLumps);
    uint64_t *tPositions = (uint64_t *)malloc(sizeof(uint64_t) * tTotalLumps);

    //--Read all names and locations.
    for(int i = 0; i < (int)tTotalLumps; i ++)
    {
        //--Retrieve the name length.
        uint8_t tLen = 0;
        fread(&tLen, sizeof(uint8_t), 1, fInfile);

        //--Allocate.
        tNames[i] = (char *)malloc(sizeof(char) * (tLen + 1));

        //--Read.
        fread(tNames[i], sizeof(char), tLen, fInfile);
        tNames[i][tLen] = '\0';

        //--Get address.
        fread(&tPositions[i], sizeof(uint64_t), 1, fInfile);
    }

    ///--[Debug]
    if(SugarLumpFile::xDebugLevel & SLF_DEBUG_AUTOQUERY)
    {
        fprintf(stderr, "Parsed file: %s\n", pFilePath);
        fprintf(stderr, " Entries: %i\n", tTotalLumps);
        for(int i = 0; i < (int)tTotalLumps; i ++)
        {
            fprintf(stderr, " %03i: %s %lu\n", i, tNames[i], tPositions[i]);
        }
    }

    ///--[Locate Lump]
    int tLumpInQuestion = -1;
    for(int i = 0; i < (int)tTotalLumps; i ++)
    {
        if(!strcasecmp(tNames[i], pAutoLoadName))
        {
            tLumpInQuestion = i;
            break;
        }
    }

    //--Not found:
    if(tLumpInQuestion == -1)
    {
        fprintf(stderr, "Error: No lump %s found. Failing.\n", pAutoLoadName);
        for(int i = 0; i < (int)tTotalLumps; i ++) free(tNames[i]);
        free(tNames);
        free(tPositions);
        fclose(fInfile);
        return;
    }

    //--Debug.
    if(SugarLumpFile::xDebugLevel & SLF_DEBUG_AUTOQUERY)
    {
        fprintf(stderr, "Located matching lump in slot %i, address %lu.\n", tLumpInQuestion, tPositions[tLumpInQuestion]);
    }

    ///--[Parse Autoloader]
    //--Go to the lump in question.
    fseek(fInfile, tPositions[tLumpInQuestion], SEEK_SET);

    //--Check the lump header.
    fread(tBuffer, sizeof(char), 10, fInfile);
    tBuffer[10] = '\0';

    //--Error:
    if(strcasecmp(tBuffer, "AUTOLOAD00"))
    {
        fprintf(stderr, "Error: Lump is not an autoload lump. Failing.\n");
        for(int i = 0; i < (int)tTotalLumps; i ++) free(tNames[i]);
        free(tNames);
        free(tPositions);
        fclose(fInfile);
        return;
    }

    //--Size of the entire lump, sans header.
    uint32_t tLumpSize = 0;
    fread(&tLumpSize, sizeof(uint32_t), 1, fInfile);

    //--How many name/path pairs there are.
    uint32_t tPairsTotal = 0;
    fread(&tPairsTotal, sizeof(uint32_t), 1, fInfile);

    //--Read the pairs and store them in the query.
    for(int p = 0; p < (int)tPairsTotal; p ++)
    {
        //--Length of name, two bytes.
        uint16_t tNameLen = 0;
        fread(&tNameLen, sizeof(uint16_t), 1, fInfile);

        //--Read name.
        char *tName = (char *)malloc(sizeof(char) * (tNameLen+1));
        fread(tName, sizeof(char), tNameLen, fInfile);
        tName[tNameLen] = '\0';

        //--Length of path, two bytes.
        uint16_t tPathLen = 0;
        fread(&tPathLen, sizeof(uint16_t), 1, fInfile);

        //--Read path.
        char *tPath = (char *)malloc(sizeof(char) * (tPathLen+1));
        fread(tPath, sizeof(char), tPathLen, fInfile);
        tPath[tPathLen] = '\0';

        //--Store.
        xAutoLoadQuery->AddElementAsTail(tName, tPath, &FreeThis);

        //--Clean.
        free(tName);
    }

    ///--[Finish Up]
    //--Deallocate.
    for(int i = 0; i < (int)tTotalLumps; i ++) free(tNames[i]);
    free(tNames);
    free(tPositions);

    //--Close file.
    fclose(fInfile);

    //--Debug.
    if(SugarLumpFile::xDebugLevel & SLF_DEBUG_AUTOQUERY)
    {
        fprintf(stderr, "Completed parsing autoloader lump from external file.\n");
    }
}
int SugarLumpFile::GetQueryCount()
{
    return xAutoLoadQuery->GetListSize();
}
const char *SugarLumpFile::GetQueryName(int pSlot)
{
    const char *rName = xAutoLoadQuery->GetNameOfElementBySlot(pSlot);
    if(!rName) return "Null";
    return rName;
}
const char *SugarLumpFile::GetQueryPath(int pSlot)
{
    const char *rPath = (char *)xAutoLoadQuery->GetElementBySlot(pSlot);
    if(!rPath) return "Null";
    return rPath;
}

///======================================== Lua Hooking ===========================================
void SugarLumpFile::HookToLuaState(lua_State *pLuaState)
{
    /* SLF_SetDebugLevel(iLevel)
       Specifies what debug information should be output to stderr. */
    lua_register(pLuaState, "SLF_SetDebugLevel", &Hook_SLF_SetDebugLevel);

    /* SLF_Open(sFileName)
       Opens an SLF file at the specified location, if it exists. Doesn't create directories by
       itself. If an SLF file is already open, this call is ignored. */
    lua_register(pLuaState, "SLF_Open", &Hook_SLF_Open);

    /* SLF_RegisterAutoLoadLump(sLoaderName)
       Creates a new auto-loader lump with the given name. */
    lua_register(pLuaState, "SLF_RegisterAutoLoadLump", &Hook_SLF_RegisterAutoLoadLump);

    /* SLF_Close()
       If an SLF file is open right now, it is closed and written to the provided location. The SLF
       file will also be deallocated. If one is not open, nothing happens. */
    lua_register(pLuaState, "SLF_Close", &Hook_SLF_Close);

    /* SLF_QueryAutoLoadInFile(sFilePath, sAutoLoadLumpName)
       Scans the given file and named lump, storing them in a static query which can be used for
       cloning autoload entries. */
    lua_register(pLuaState, "SLF_QueryAutoLoadInFile", &Hook_SLF_QueryAutoLoadInFile);

    /* SLF_GetQueryCount() (1 Integer)
       Returns how many entries were in the last queried autoload lump. Can legally return 0. */
    lua_register(pLuaState, "SLF_GetQueryCount", &Hook_SLF_GetQueryCount);

    /* SLF_GetQueryEntry(iSlot) (2 Strings)
       Returns the name and path of the given entry in the autoload lump. Returns "Null" on error. */
    lua_register(pLuaState, "SLF_GetQueryEntry", &Hook_SLF_GetQueryEntry);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_SLF_SetDebugLevel(lua_State *L)
{
    //SLF_SetDebugLevel(iLevel)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SLF_SetDebugLevel");

    SugarLumpFile::xDebugLevel = lua_tointeger(L, 1);

    return 0;
}
int Hook_SLF_Open(lua_State *L)
{
    //SLF_Open(sFileName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SLF_Open");

    //--Is there a file open? If so, fail.
    SugarLumpFile *rCheckFile = SugarLumpFile::Fetch();
    if(rCheckFile) return 0;

    //--Create and place.
    SugarLumpFile *nFile = new SugarLumpFile();

    Global::Shared()->gActiveLumpFile = nFile;
    nFile->mWritePath = CopyString(lua_tostring(L, 1));

    return 0;
}
int Hook_SLF_RegisterAutoLoadLump(lua_State *L)
{
    //SLF_RegisterAutoLoadLump(sLumpName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SLF_RegisterAutoLoadLump");

    //--Is there a file open? If not, fail.
    SugarLumpFile *rCheckFile = SugarLumpFile::Fetch();
    if(!rCheckFile) return 0;

    //--Create.
    rCheckFile->CreateAutoLoadLump(lua_tostring(L, 1));
    return 0;
}
int Hook_SLF_Close(lua_State *L)
{
    //SLF_Close()

    //--Is there a file open?  If not, fail.
    SugarLumpFile *rCheckFile = SugarLumpFile::Fetch();
    if(!rCheckFile) return fprintf(stderr, "SLF_Close:  Failed, no SLF file was open!\n");

    //--Write the file to disk.
    rCheckFile->Write(rCheckFile->mWritePath);

    //--Clear
    delete rCheckFile;
    Global::Shared()->gActiveLumpFile = NULL;

    return 0;
}
int Hook_SLF_QueryAutoLoadInFile(lua_State *L)
{
    //SLF_QueryAutoLoadInFile(sFilePath, sAutoLoadLumpName) (Static)
    int pArgs = lua_gettop(L);
    if(pArgs < 2) return LuaArgError("SLF_QueryAutoLoadInFile");

    SugarLumpFile::QueryAutoLoadFromFile(lua_tostring(L, 1), lua_tostring(L, 2));
    return 0;
}
int Hook_SLF_GetQueryCount(lua_State *L)
{
    //SLF_GetQueryCount() (1 Integer)
    lua_pushinteger(L, SugarLumpFile::GetQueryCount());
    return 1;
}
int Hook_SLF_GetQueryEntry(lua_State *L)
{
    //SLF_GetQueryEntry(iSlot) (2 Strings)
    int pArgs = lua_gettop(L);
    if(pArgs < 1) { LuaArgError("SLF_SetDebugLevel"); lua_pushstring(L, "Null"); lua_pushstring(L, "Null"); return 2; }

    lua_pushstring(L, SugarLumpFile::GetQueryName(lua_tointeger(L, 1)));
    lua_pushstring(L, SugarLumpFile::GetQueryPath(lua_tointeger(L, 1)));
    return 2;
}
