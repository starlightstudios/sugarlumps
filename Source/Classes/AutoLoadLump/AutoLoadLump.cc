//--Base
#include "AutoLoadLump.h"

//--Classes
#include "SugarLumpFile.h"

//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
AutoLoadLump::AutoLoadLump()
{
    //--[RootLump]
    //--System
    mTypeCode = POINTER_TYPE_LUMP_AUTOLOADER;

    //--[AutoLoadLump]
    //--System
    mLookupList = new StarlightLinkedList(true);

    //--Data
    mStoredEntryCount = 0;
    mArraySize = 0;
    mArray = NULL;
}
AutoLoadLump::~AutoLoadLump()
{
    delete mLookupList;
    free(mArray);
}
void AutoLoadLump::DeleteLoadPair(void *pPtr)
{
    LoadPair *rPair = (LoadPair *)pPtr;
    free(rPair->mBitmapName);
    free(rPair->mDLPath);
    free(rPair);
}

///--[Public Variables]
uint8_t AutoLoadLump::xDebugLevel = ALL_DEBUG_NONE;

///===================================== Property Queries =========================================
bool AutoLoadLump::HasInformation()
{
    return (mLookupList->GetListSize() > 0 || mArraySize > 0);
}

///======================================== Manipulators ==========================================
void AutoLoadLump::RegisterLookup(const char *pBitmapName, const char *pDLPath)
{
    ///--[Documentation]
    //--Registers a name/path pair to the lump.
    if(!pBitmapName || !pDLPath) return;

    //--Allocate.
    LoadPair *nPackage = (LoadPair *)malloc(sizeof(LoadPair));
    memset(nPackage, 0, sizeof(LoadPair));

    //--Set.
    ResetString(nPackage->mBitmapName, pBitmapName);
    ResetString(nPackage->mDLPath, pDLPath);

    //--Register.
    mLookupList->AddElement("X", nPackage, &AutoLoadLump::DeleteLoadPair);
}

///======================================== Core Methods ==========================================
void AutoLoadLump::CompileInformation()
{
    ///--[Documentation]
    //--Takes all the information out of the lookup list and places it in a single array for file
    //  writing purposes. Should only be called once.
    if(AutoLoadLump::xDebugLevel & ALL_DEBUG_COMPILE_STATUS)
    {
        fprintf(stderr, " AutoLoadLump - Compiling.\n");
        fprintf(stderr, "  Entries: %i\n", mLookupList->GetListSize());
    }

    //--First, wipe the existing data if there is any.
    mArraySize = 0;
    free(mArray);
    mArray = NULL;

    //--Store the number of entries for later.
    mStoredEntryCount = mLookupList->GetListSize();

    //--Next, assemble sizing information. Each string is preceeded by its size as a 16-bit integer.
    LoadPair *rPair = (LoadPair *)mLookupList->PushIterator();
    while(rPair)
    {
        //--Add two 16-bit sizes, one for each string.
        mArraySize += (sizeof(int16_t)) * 2;

        //--Add one for each letter, excluding the trailing NULL.
        mArraySize += strlen(rPair->mBitmapName);
        mArraySize += strlen(rPair->mDLPath);

        //--Next.
        rPair = (LoadPair *)mLookupList->AutoIterate();
    }

    //--Now that we have the expected size, allocate it.
    mArray = (uint8_t *)malloc(sizeof(uint8_t) * mArraySize);
    if(AutoLoadLump::xDebugLevel & ALL_DEBUG_COMPILE_STATUS)
    {
        fprintf(stderr, "  Size: %i\n", mArraySize);
    }

    //--Now go back and place all the strings into the array.
    int i = 0;
    rPair = (LoadPair *)mLookupList->PushIterator();
    while(rPair)
    {
        //--Put the Bitmap's name in with its preceding 16-bit length.
        int16_t tLen = (int16_t)strlen(rPair->mBitmapName);
        memcpy(&mArray[i], &tLen, sizeof(int16_t));
        i += sizeof(int16_t);

        //--Now print the Bitmap's name.
        memcpy(&mArray[i], rPair->mBitmapName, sizeof(char) * tLen);
        i += sizeof(char) * tLen;

        //--Do it all again for the DL Path.
        tLen = (int16_t)strlen(rPair->mDLPath);
        memcpy(&mArray[i], &tLen, sizeof(int16_t));
        i += sizeof(int16_t);
        memcpy(&mArray[i], rPair->mDLPath, sizeof(char) * tLen);
        i += sizeof(char) * tLen;

        //--Next.
        rPair = (LoadPair *)mLookupList->AutoIterate();
    }

    //--All the data has been dumped. Clear the lookup list.
    if(AutoLoadLump::xDebugLevel & ALL_DEBUG_COMPILE_STATUS)
    {
        fprintf(stderr, " Finished.\n");
    }
    mLookupList->ClearList();
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void AutoLoadLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    ///--[Documentation]
    //--Writes the data of the lump to the given file.
    if(!pOutfile) return;

    ///--[Compile]
    //--Compile the lump.
    CompileInformation();

    ///--[Write]
    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "AUTOLOAD00";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    //--Write the size of the array in bytes.
    fwrite(&mArraySize, sizeof(uint32_t), 1, pOutfile);
    sCounter += (sizeof(uint32_t) * 1);

    //--Write how many name/path pairs are in the lump.
    fwrite(&mStoredEntryCount, sizeof(int32_t), 1, pOutfile);
    sCounter += (sizeof(int32_t) * 1);

    //--Write the array. If the size was zero, does nothing.
    if(!mArray) return;
    fwrite(mArray, sizeof(uint8_t), mArraySize, pOutfile);
    sCounter += (sizeof(uint8_t) * mArraySize);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarlightLinkedList *AutoLoadLump::GetList()
{
    return mLookupList;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void AutoLoadLump::HookToLuaState(lua_State *pLuaState)
{
    /* AutoLoader_SetDebugLevel(iLevel) (Static)
       Sets amount of information this lump type writes to stderr. */
    lua_register(pLuaState, "AutoLoader_SetDebugLevel", &Hook_AutoLoader_SetDebugLevel);

    /* AutoLoader_Register(sAutoloaderName, sBitmapName, sDLPath)
       Register a new auto-loading instruction. Presently, this only includes SugarBitmaps to be
       loaded to the DataLibrary. Each SLF file has its own auto-loader created automatically when
       the file is created. */
    lua_register(pLuaState, "AutoLoader_Register", &Hook_AutoLoader_Register);

    /* Autoloader_QuerySize(sAutoloaderName)
       Returns how many entries are in the given autoloader lump. */
    lua_register(pLuaState, "Autoloader_QuerySize", &Hook_Autoloader_QuerySize);

    /* Autoloader_QueryEntry(sAutoloaderName, iSlot)
       Returns a pair of name/path for the given slot in the given autoloader lump. */
    lua_register(pLuaState, "Autoloader_QueryEntry", &Hook_Autoloader_QueryEntry);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AutoLoader_SetDebugLevel(lua_State *L)
{
    //AutoLoader_SetDebugLevel(iLevel) (Static)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AutoLoader_SetDebugLevel");

    AutoLoadLump::xDebugLevel = lua_tointeger(L, 1);

    return 0;
}
int Hook_AutoLoader_Register(lua_State *L)
{
    //AutoLoader_Register(sAutoloaderName, sBitmapName, sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("AutoLoader_Register");

    //--Register and return.
    SugarLumpFile *rActiveFile = SugarLumpFile::Fetch();
    if(rActiveFile)
    {
        rActiveFile->RegisterAutoLoadName(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));
    }
    return 0;
}
int Hook_Autoloader_QuerySize(lua_State *L)
{
    //Autoloader_QuerySize(sAutoloaderName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        lua_pushinteger(L, 0);
        LuaArgError("Autoloader_QuerySize");
        return 1;
    }

    //--Check file.
    SugarLumpFile *rActiveFile = SugarLumpFile::Fetch();
    if(!rActiveFile) { lua_pushinteger(L, 0); return 1; }

    //--Check lump.
    AutoLoadLump *rCheckLump = rActiveFile->GetLump(lua_tostring(L, 1));
    if(!rCheckLump) { lua_pushinteger(L, 0); return 1; }

    //--Return size.
    lua_pushinteger(L, rCheckLump->GetList()->GetListSize());
    return 1;
}
int Hook_Autoloader_QueryEntry(lua_State *L)
{
    //Autoloader_QueryEntry(sAutoloaderName, iSlot)
    int tArgs = lua_gettop(L);
    if(tArgs < 2)
    {
        lua_pushstring(L, "Null");
        lua_pushstring(L, "Null");
        LuaArgError("Autoloader_QueryEntry");
        return 2;
    }

    //--Check file.
    SugarLumpFile *rActiveFile = SugarLumpFile::Fetch();
    if(!rActiveFile) { lua_pushstring(L, "Null"); lua_pushstring(L, "Null"); return 2; }

    //--Check lump.
    AutoLoadLump *rCheckLump = rActiveFile->GetLump(lua_tostring(L, 1));
    if(!rCheckLump) { lua_pushstring(L, "Null"); lua_pushstring(L, "Null"); return 2; }

    //--Get entry.
    LoadPair *rPair = (LoadPair *)rCheckLump->GetList()->GetElementBySlot(lua_tointeger(L, 2));
    if(!rPair) { lua_pushstring(L, "Null"); lua_pushstring(L, "Null"); return 2; }

    //--Return.
    lua_pushstring(L, rPair->mBitmapName);
    lua_pushstring(L, rPair->mDLPath);
    return 2;
}
