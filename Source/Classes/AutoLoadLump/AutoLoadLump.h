///======================================= AutoLoadLump ===========================================
//--A lump containing lookup information for SugarBitmap type objects. Doesn't contain any actual
//  bitmap data. Instead, the lump contains the name of a bitmap in the file, and a location that
//  it should be loaded to in the DataLibrary.
//--Exactly how the end program uses the information is up to it. The default behavior cross-loads
//  the bitmap to that DL location when the lump is used.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLump.h"

///===================================== Local Structures =========================================
typedef struct
{
    char *mBitmapName;
    char *mDLPath;
}LoadPair;

///===================================== Local Definitions ========================================
//--Debug Levels
#define ALL_DEBUG_NONE 0x00
#define ALL_DEBUG_COMPILE_STATUS 0x01

///========================================== Classes =============================================
class AutoLoadLump : public RootLump
{
    private:
    //--System
    StarlightLinkedList *mLookupList; //LoadPair *, master

    //--Data
    int32_t mStoredEntryCount;
    uint32_t mArraySize;
    uint8_t *mArray;

    protected:

    public:
    //--System
    AutoLoadLump();
    virtual ~AutoLoadLump();
    static void DeleteLoadPair(void *pPtr);

    //--Public Variables
    static uint8_t xDebugLevel;

    //--Property Queries
    bool HasInformation();

    //--Manipulators
    void RegisterLookup(const char *pBitmapName, const char *pDLPath);

    //--Core Methods
    void CompileInformation();

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    StarlightLinkedList *GetList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AutoLoader_SetDebugLevel(lua_State *L);
int Hook_AutoLoader_Register(lua_State *L);
int Hook_Autoloader_QuerySize(lua_State *L);
int Hook_Autoloader_QueryEntry(lua_State *L);
