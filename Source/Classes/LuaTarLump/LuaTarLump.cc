//--Base
#include "LuaTarLump.h"

//--Classes
#include "SugarLumpFile.h"

//--CoreClasses
#include "StarlightFileSystem.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
LuaTarLump::LuaTarLump()
{
    //--System
    //--Storage
    mDataSize = 0;
    mData = NULL;
}
LuaTarLump::~LuaTarLump()
{
    free(mData);
}

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void LuaTarLump::AcceptData(uint32_t pSize, void *pData)
{
    ///--[Documentation]
    //--Given a block of data, resizes the lump around that data. Pass NULL to clear the lump.
    mDataSize = 0;
    free(mData);
    mData = NULL;
    if(pSize == 0 || !pData) return;

    //--Allocate and set.
    mDataSize = pSize;
    mData = malloc(mDataSize);
    memcpy(mData, pData, mDataSize);
}

///======================================== Core Methods ==========================================
void LuaTarLump::BuildTarball(const char *pScriptRoot)
{
    ///--[Documentation]
    //--Worker function. Builds a series of LuaTarLumps by recursing through the provided directory.
    if(!pScriptRoot) return;

    //--An active SLF file is needed! We don't use it here, we just check it to save time parsing.
    SugarLumpFile *rSLF = SugarLumpFile::Fetch();
    if(!rSLF) return;

    ///--[Filesystem Setup]
    //--Order a Filesystem to parse the provided directory. For now, print it.
    StarlightFileSystem *tFileSystem = new StarlightFileSystem();
    tFileSystem->mIgnoreFolders = false;
    tFileSystem->mIgnoreZerothEntry = true;
    tFileSystem->ScanDirectory(pScriptRoot);

    ///--[Iterate]
    //--For each entry, check if it's a lua file. If it is, create a lump of it and store that lump.
    int tTotalEntries = tFileSystem->GetTotalEntries();
    for(int i = 0; i < tTotalEntries; i ++)
    {
        //--Fetch
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo) continue;

        //--Regular file? Is it a .lua?
        if(rFileInfo->mIsFile)
        {
            //--It's a .lua!
            int tLen = strlen(rFileInfo->mPath);
            if(tLen > 5 && rFileInfo->mPath[tLen - 4] == '.' && rFileInfo->mPath[tLen - 3] == 'l' &&
                           rFileInfo->mPath[tLen - 2] == 'u' && rFileInfo->mPath[tLen - 1] == 'a')
            {
                LuaTarLump::CreateAround(tFileSystem->GetLastParsedDirectory(), rFileInfo->mPath);
            }
        }
        //--Directory. Auto-recurse is on, so we should ignore these.
        else
        {
        }
    }
}
void LuaTarLump::CreateAround(const char *pScriptRoot, const char *pPath)
{
    ///--[ ====== Documentation ===== ]
    //--Worker routine. Given the base directory (ex:  C:/Danika/) and a path to a lua file
    //  (ex: c:/Danika/Slobber.lua) will create a LuaTarLump of the name of the file in the tree
    //  local to the root.
    if(!pScriptRoot || !pPath) return;

    //--We need an SLF file to reg it to!
    SugarLumpFile *rSLF = SugarLumpFile::Fetch();
    if(!rSLF) return;

    ///--[ ======= Path Setup ======= ]
    //--So, first we need to search for the spot in the path that matches the script root. Copy these
    //  into temporary buffers for safety.
    char tRootName[512];
    strcpy(tRootName, pScriptRoot);
    char tPathName[512];
    strcpy(tPathName, pPath);

    //--For every character that matches between both scripts, add one to this counter.
    int tCommonChars = 0;
    for(uint32_t i = 0; i < strlen(tRootName); i ++)
    {
        if(tRootName[i] == tPathName[i])
        {
            tCommonChars ++;
        }
        else
        {
            i = strlen(tRootName);
        }
    }

    //--Now simply string-shift off the characters. We have the truncated path!
    char tFinalPath[512];
    for(uint32_t i = tCommonChars; i < strlen(tPathName)+1; i ++)
    {
        tFinalPath[i-tCommonChars] = tPathName[i];
    }

    //--Debug
    //fprintf(stderr, "Truncated to: %s\n", tFinalPath);

    ///--[ ==== Create Lua Lump ===== ]
    //--Setup
    bool tUseRawText = true;
    void *nDataBlock = NULL;
    int tSizeOfData = 0;

    ///--[Compile Mode]
    //--Next, we need to compile this file.
    if(!tUseRawText)
    {
        LuaManager *rLuaManager = LuaManager::Fetch();
        rLuaManager->LoadLuaFile(tPathName);
        nDataBlock = rLuaManager->DumpLoadedFile();
        tSizeOfData = rLuaManager->xLastDumpedSize;
    }
    ///--[Raw Text Mode]
    //--Because the idiots at Lua made it EXTREMELY difficult to strip debug data from the lua
    //  files, it can often be smaller and faster to actually just include the raw text!
    else
    {
        //--Open.
        FILE *fInfile = fopen(tPathName, "rb");

        //--Check size.
        fseek(fInfile, 0, SEEK_END);
        tSizeOfData = ftell(fInfile);
        fseek(fInfile, 0, SEEK_SET);

        //--Copy all data into waiting buffer.
        nDataBlock = malloc(tSizeOfData);
        fread(nDataBlock, tSizeOfData, 1, fInfile);

        //--Clean.
        fclose(fInfile);
    }

    ///--[Process and Store Lump]
    //--File is compiled, create a new lump with the name being the path, and give it the data.
    LuaTarLump *nLump = new LuaTarLump();
    nLump->AcceptData(tSizeOfData, nDataBlock);
    fprintf(stderr, " Size: %i\n", tSizeOfData);
    //fprintf(stderr, " Passed data\n");

    //--Register the lump.
    rSLF->RegisterLump(tFinalPath, nLump, &LuaTarLump::DeleteThis);
    //fprintf(stderr, " Regged lump\n");

    ///--[Finish Up]
    //--Other cleanup.
    if(tUseRawText)
    {
        free(nDataBlock);
    }
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void LuaTarLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Writes all the data of the LuaTarLump.  Header specifies the version.
    if(!pOutfile) return;

    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "LUATAR0000";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    //--Write the length of the data block.
    fwrite(&mDataSize, sizeof(uint32_t), 1, pOutfile);
    sCounter += (sizeof(uint32_t) * 1);

    //--Write the data block itself.
    fwrite(mData, sizeof(uint8_t), mDataSize, pOutfile);
    sCounter += (sizeof(uint8_t) * mDataSize);

}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void LuaTarLump::HookToLuaState(lua_State *pLuaState)
{
    /* LuaTar_BuildTarball(sBasePath[])
       Builds a lua tar archive by recursing within the requested folder.  The path can be relative
       or absolute (it's best done relative) and will be truncated from the root. */
    lua_register(pLuaState, "LuaTar_BuildTarball", &Hook_LuaTar_BuildTarball);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LuaTar_BuildTarball(lua_State *L)
{
    //LuaTar_BuildTarball(sBasePath[])
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("LuaTar_BuildTarball");

    LuaTarLump::BuildTarball(lua_tostring(L, 1));

    return 0;
}
