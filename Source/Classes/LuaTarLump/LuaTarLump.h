///======================================== LuaTarLump ============================================
//--A Lua Tar Lump is a single lua file stored in a .slf file, typically as compressed lua bytecode.
//  The lua tar lump does not necessarily know anything about itself, but the slf file will store
//  the lump as its pathname by default. Therefore, a LuaManager will know the relative directory
//  for a given lump at all times.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLump.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class LuaTarLump : public RootLump
{
    private:
    //--System
    //--Storage
    uint32_t mDataSize;
    void *mData;

    protected:

    public:
    //--System
    LuaTarLump();
    virtual ~LuaTarLump();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void AcceptData(uint32_t pSize, void *pData);

    //--Core Methods
    static void BuildTarball(const char *pScriptRoot);
    static void CreateAround(const char *pScriptRoot, const char *pPath);

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LuaTar_BuildTarball(lua_State *L);

