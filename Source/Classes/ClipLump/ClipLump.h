///========================================== ClipLump ============================================
//--Represents a block of collisions data. Presently, the only supported type is 1-bit per pixel.
//  This is left expandable by a 1-byte type flag.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLump.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define CLIPTYPE_1BIT 0

#include "Definitions.h"
#include "Structures.h"

///========================================== Classes =============================================
class ClipLump : public RootLump
{
    private:
    //--System
    uint8_t mType;

    //--Sizes
    uint16_t mXSize;
    uint16_t mYSize;

    //--Data
    uint32_t mArraySize;
    uint8_t *mArray;

    protected:

    public:
    //--System
    ClipLump();
    virtual ~ClipLump();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetSizes(uint16_t pXSize, uint16_t pYSize);

    //--Core Methods
    void RipData(ALLEGRO_BITMAP *pBitmap, int pLft, int pTop, int pWid, int pHei);

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

