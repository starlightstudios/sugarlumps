//--Base
#include "ClipLump.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
ClipLump::ClipLump()
{
    //--[RootLump]
    //--System
    mTypeCode = POINTER_TYPE_LUMP_CLIP;

    //--[ClipLump]
    //--System
    mType = CLIPTYPE_1BIT;

    //--Sizes
    mXSize = 0;
    mYSize = 0;

    //--Data
    mArraySize = 0;
    mArray = NULL;
}
ClipLump::~ClipLump()
{
    free(mArray);
}

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void ClipLump::SetSizes(uint16_t pXSize, uint16_t pYSize)
{
    mXSize = pXSize;
    mYSize = pYSize;
}

///======================================== Core Methods ==========================================
void RipByte(uint8_t &sByte, ALLEGRO_COLOR pColors[8])
{
    //--Inlined for speed.
    sByte = 0x00;
    if(pColors[0].b != 0 && pColors[0].a != 0) sByte |= 0x01;
    if(pColors[1].b != 0 && pColors[1].a != 0) sByte |= 0x02;
    if(pColors[2].b != 0 && pColors[2].a != 0) sByte |= 0x04;
    if(pColors[3].b != 0 && pColors[3].a != 0) sByte |= 0x08;
    if(pColors[4].b != 0 && pColors[4].a != 0) sByte |= 0x10;
    if(pColors[5].b != 0 && pColors[5].a != 0) sByte |= 0x20;
    if(pColors[6].b != 0 && pColors[6].a != 0) sByte |= 0x40;
    if(pColors[7].b != 0 && pColors[7].a != 0) sByte |= 0x80;
}
void ClipLump::RipData(ALLEGRO_BITMAP *pBitmap, int pLft, int pTop, int pWid, int pHei)
{
    //--Taking in a bitmap which has the clips as blue-pixels and unclipped zones as non-blue,
    //  places all the collisions data into the required array space.
    //--Note that we PAD the array's edges.  If the pWid is not divisible by 8, then the spare bits
    //  are still stored, but the unpacker should not use them - they are undefined.
    if(!pBitmap || pWid < 1 || pHei < 1) return;

    //--Clear old array.
    mArraySize = 0;
    free(mArray);
    mArray = NULL;

    //--Set values
    mXSize = pWid;
    mYSize = pHei;

    //--Estimate size.  Pad the edges.
    int tWid = (pWid / 8);
    if(pWid % 8 != 0) tWid ++;
    mArraySize = tWid * pHei;
    mArray = (uint8_t *)malloc(sizeof(uint8_t) * mArraySize);

    //--Scan over the bitmap.  Use the subroutine to actually do the hard work.
    int tIndex = 0;
    ALLEGRO_COLOR tPassArray[8];
    for(int y = pTop; y < pTop + pHei; y ++)
    {
        for(int x = pLft; x < pLft + (tWid * 8); x += 0)
        {
            //--Rip 8 pixels of data.
            for(int i = 0; i < 8; i ++)
            {
                tPassArray[i] = al_get_pixel(pBitmap, x, y);
                x ++;
            }

            //--Process that.
            RipByte(mArray[tIndex], tPassArray);
            tIndex ++;
        }
    }
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void ClipLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Writes all the data of the ClipLump.  Header specifies the version.
    if(!pOutfile) return;

    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "CLIP000000";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    //--Write the data's type.
    fwrite(&mType, sizeof(uint8_t), 1, pOutfile);
    sCounter += (sizeof(uint8_t) * 1);

    //--Write X and Y size.
    fwrite(&mXSize, sizeof(uint16_t), 1, pOutfile);
    fwrite(&mYSize, sizeof(uint16_t), 1, pOutfile);
    sCounter += (sizeof(uint16_t) * 2);

    //--Write the data array.
    fwrite(&mArraySize, sizeof(uint32_t), 1, pOutfile);
    sCounter += (sizeof(uint32_t) * 1);

    if(!mArray) return;
    fwrite(mArray, sizeof(uint8_t), mArraySize, pOutfile);
    sCounter += (sizeof(uint8_t) * mArraySize);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
