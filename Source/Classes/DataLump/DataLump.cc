//--Base
#include "DataLump.h"

//--Classes
#include "SugarLumpFile.h"

//--CoreClasses
#include "StarlightLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
DataLump::DataLump()
{
    //--[RootLump]
    //--System
    mTypeCode = POINTER_TYPE_LUMP_DATA;

    //--[DataLump]
    //--System
    //--Storage
    mDataListing = new StarlightLinkedList(true);
}
DataLump::~DataLump()
{
    delete mDataListing;
}
void DataLump::DeleteDataLumpEntry(void *pPtr)
{
    DataLumpEntry *rEntry = (DataLumpEntry *)pPtr;
    free(rEntry->mDataPtr);
    free(rEntry);
}

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void DataLump::AddInt8(int8_t pValue)
{
    int8_t *pPtr = (int8_t *)AddEntry(sizeof(int8_t));
    *pPtr = pValue;
}
void DataLump::AddUInt8(uint8_t pValue)
{
    uint8_t *pPtr = (uint8_t *)AddEntry(sizeof(uint8_t));
    *pPtr = pValue;
}
void DataLump::AddInt16(int16_t pValue)
{
    int16_t *pPtr = (int16_t *)AddEntry(sizeof(int16_t));
    *pPtr = pValue;
}
void DataLump::AddUInt16(uint16_t pValue)
{
    uint16_t *pPtr = (uint16_t *)AddEntry(sizeof(uint16_t));
    *pPtr = pValue;
}
void DataLump::AddInt32(int32_t pValue)
{
    int32_t *pPtr = (int32_t *)AddEntry(sizeof(int32_t));
    *pPtr = pValue;
}
void DataLump::AddUInt32(uint32_t pValue)
{
    uint32_t *pPtr = (uint32_t *)AddEntry(sizeof(uint32_t));
    *pPtr = pValue;
}
void DataLump::AddInt64(int64_t pValue)
{
    int64_t *pPtr = (int64_t *)AddEntry(sizeof(int64_t));
    *pPtr = pValue;
}
void DataLump::AddUInt64(uint64_t pValue)
{
    uint64_t *pPtr = (uint64_t *)AddEntry(sizeof(uint64_t));
    *pPtr = pValue;
}
void DataLump::AddStringWithPrefix(const char *pString)
{
    //--Adds a string of a fixed size, along with a 16-bit prefix indicating how long the string is.
    //  The string will *not* be null-terminated.
    if(!pString) return;

    //--Get length.
    int tLength = (int)strlen(pString);
    void *rDataPtr = AddEntry(sizeof(int16_t) + (sizeof(char) * tLength));

    //--Write the length as a 16-bit integer.
    int16_t *rDataAsInt16 = (int16_t *)rDataPtr;
    *rDataAsInt16 = tLength;

    //--Write the rest of the string.
    char *rDataAsChar = (char *)rDataPtr;
    for(int i = 0; i < tLength; i ++)
    {
        rDataAsChar[2+i] = pString[i];
    }
}

///======================================== Core Methods ==========================================
void *DataLump::AddEntry(size_t pSize)
{
    //--Subroutine, adds a new entry of the given size to the end of the list and returns its data pointer.
    //  You can then set the pointer however you want.
    //--pSize always defaults to at least 1 even if you screw up.
    if(pSize < 1) pSize = 1;

    //--Allocate.
    DataLumpEntry *nNewEntry = (DataLumpEntry *)malloc(sizeof(DataLumpEntry));
    nNewEntry->mDataPtr = malloc(pSize);
    nNewEntry->mDataSize = pSize;
    mDataListing->AddElement("X", nNewEntry, DataLump::DeleteDataLumpEntry);

    //--Pass back the pointer.
    return nNewEntry->mDataPtr;
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void DataLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Writes the lump at the given position, including the header.

    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "DATA000000";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    //--For each entry...
    DataLumpEntry *rEntry = (DataLumpEntry *)mDataListing->PushIterator();
    while(rEntry)
    {
        //--Write.
        fwrite(rEntry->mDataPtr, rEntry->mDataSize, 1, pOutfile);
        sCounter += rEntry->mDataSize;

        //--Next entry.
        rEntry = (DataLumpEntry *)mDataListing->AutoIterate();
    }
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void DataLump::HookToLuaState(lua_State *pLuaState)
{
    /* DataLump_Begin(sLumpName)
       Begins writing a new DataLump with the specified name. Remember to pop it when you're done. */
    lua_register(pLuaState, "DataLump_Begin", &Hook_DataLump_Begin);

    /* DataLump_Append("Int8", iValue)
       DataLump_Append("UInt8", iValue)
       DataLump_Append("Int16", iValue)
       DataLump_Append("UInt16", iValue)
       DataLump_Append("Int32", iValue)
       DataLump_Append("UInt32", iValue)
       DataLump_Append("String", sString)
       Appends the specified byte/bytes onto the DataLump's end. */
    lua_register(pLuaState, "DataLump_Append", &Hook_DataLump_Append);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_DataLump_Begin(lua_State *L)
{
    //DataLump_Begin(sLumpName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DataLump_Begin");

    //--Get the active SLF file. Fail if none is available.
    SugarLumpFile *rFile = SugarLumpFile::Fetch();
    if(!rFile) return fprintf(stderr, "DataLump_Begin:  Failed, no SLF open\n");

    //--Create and set.
    DataLump *nNewLump = new DataLump();
    rFile->RegisterLump(lua_tostring(L, 1), nNewLump, &RootLump::DeleteThis);
    rDataLibrary->rActiveObject = nNewLump;
    return 0;
}
int Hook_DataLump_Append(lua_State *L)
{
    //DataLump_Append("Int8", iValue)
    //DataLump_Append("UInt8", iValue)
    //DataLump_Append("Int16", iValue)
    //DataLump_Append("UInt16", iValue)
    //DataLump_Append("Int32", iValue)
    //DataLump_Append("UInt32", iValue)
    //DataLump_Append("String", sString)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DataLump_Append");

    //--Verify the active object.
    DataLump *rDataLump = (DataLump *)DataLibrary::Fetch()->rActiveObject;
    if(!rDataLump || rDataLump->GetType() != POINTER_TYPE_LUMP_DATA) return LuaTypeError("DataLump_Append");

    //--Type check.
    const char *rSwitchType = (const char *)lua_tostring(L, 1);

    //--Integer 8-bit.
    if(!strcasecmp(rSwitchType, "Int8") && tArgs == 2)
    {
        rDataLump->AddInt8(lua_tointeger(L, 2));
    }
    //--Unsigned Integer 8-bit.
    else if(!strcasecmp(rSwitchType, "UInt8") && tArgs == 2)
    {
        rDataLump->AddUInt8(lua_tointeger(L, 2));
    }
    //--Integer 16-bit.
    else if(!strcasecmp(rSwitchType, "Int16") && tArgs == 2)
    {
        rDataLump->AddInt16(lua_tointeger(L, 2));
    }
    //--Unsigned Integer 16-bit.
    else if(!strcasecmp(rSwitchType, "UInt16") && tArgs == 2)
    {
        rDataLump->AddUInt16(lua_tointeger(L, 2));
    }
    //--Integer 32-bit.
    else if(!strcasecmp(rSwitchType, "Int32") && tArgs == 2)
    {
        rDataLump->AddInt32(lua_tointeger(L, 2));
    }
    //--Unsigned Integer 32-bit.
    else if(!strcasecmp(rSwitchType, "UInt32") && tArgs == 2)
    {
        rDataLump->AddUInt32(lua_tointeger(L, 2));
    }
    //--String.
    else if(!strcasecmp(rSwitchType, "String") && tArgs == 2)
    {
        rDataLump->AddStringWithPrefix(lua_tostring(L, 2));
    }
    //--Unhandled type.
    else
    {
        LuaPropertyError("DataLump_Append", rSwitchType, tArgs);
    }

    return 0;
}
