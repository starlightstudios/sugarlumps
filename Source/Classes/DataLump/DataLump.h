///========================================== DataLump ============================================
//--A lump of generic data. Unlike other lumps, this one must be handled manually in some way. It can
//  also be edited directly from the Lua file.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLump.h"

#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
typedef struct
{
    size_t mDataSize;
    void *mDataPtr;
}DataLumpEntry;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class DataLump : public RootLump
{
    private:
    //--System
    //--Storage
    StarlightLinkedList *mDataListing;

    protected:

    public:
    //--System
    DataLump();
    virtual ~DataLump();
    static void DeleteDataLumpEntry(void *pPtr);

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void AddInt8(int8_t pValue);
    void AddUInt8(uint8_t pValue);
    void AddInt16(int16_t pValue);
    void AddUInt16(uint16_t pValue);
    void AddInt32(int32_t pValue);
    void AddUInt32(uint32_t pValue);
    void AddInt64(int64_t pValue);
    void AddUInt64(uint64_t pValue);
    void AddStringWithPrefix(const char *pString);

    //--Core Methods
    void *AddEntry(size_t pSize);

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_DataLump_Begin(lua_State *L);
int Hook_DataLump_Append(lua_State *L);

