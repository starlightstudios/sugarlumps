--[AutoExec]
--Lua file which is executed if no other files were specified on the command line.

--Common functions
LM_ExecuteScript("Data/CommonFuncs.lua")

--Map Compression
LM_ExecuteScript("Data/ImageCompression/Exec.lua")
--LM_ExecuteScript("Data/MapCompression/MapCompress.lua")

--Script Compression
--SLF_Open("Output/Scripts.slf")
--LuaTar_BuildTarball("Data/Scripts/")
--SLF_Close()

--Print debug information
io.write("Used AutoExec.lua!\n")